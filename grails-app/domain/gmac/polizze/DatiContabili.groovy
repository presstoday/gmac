package gmac.polizze

class DatiContabili {
    BigDecimal pcl, imposte, provvigioniDealer, imponibile, totale, pdu, provvigioniSocietaCommerciale, provvigioniMach1
    String codiceProdotto, marca


    static constraints = {
        pcl nullable: true, min: 0.0
        imposte nullable: true, min: 0.0
        provvigioniDealer nullable: true, min: 0.0
        imponibile nullable: true, min: 0.0
        pdu nullable: true, min: 0.0
        provvigioniSocietaCommerciale nullable: true, min: 0.0
        provvigioniMach1 nullable: true, min: 0.0
        totale nullable: true, min: 0.0
        codiceProdotto nullable: true
        marca nullable: true
    }
}
