package gmac.polizze

import gmac.utenti.Dealer
import groovy.time.TimeCategory as TC
import org.grails.databinding.BindingFormat

enum TipoPolizza {
    EXTENSION, DEMO_OPEL
}


class Polizza {
    Date dateCreated = new Date()
    String noPolizza
    @BindingFormat("capitalize") String nome
    @BindingFormat("capitalize") String cognome
    TipoPolizza tipoPolizza = TipoPolizza.EXTENSION
    Date dataDecorrenza
    Date dataScadenza
    Date dataTagliando
    Date dataEsclusione
    Integer km
    boolean annullata =false
    boolean esclusa =false
    boolean nuova =false
    static belongsTo = [dealer: Dealer]
    Tracciato tracciato
    TracciatoCompagnia tracciatoCompagnia
    Telai telaio
    String telaioDemo
    String targa
    String modello
    String marca

    static constraints = {
        dataDecorrenza nullable: false
        dataScadenza nullable: false
        dataTagliando nullable: true, validator: { dataTagliando, polizza ->
            if(polizza.tipoPolizza == TipoPolizza.EXTENSION && !dataTagliando) return "polizze.dataTagliando.nullable"
        }
        dataEsclusione nullable: true
        noPolizza nullable: true
        tracciato nullable: true
        tracciatoCompagnia nullable: true
        dealer nullable: false
        km nullable: true, max: 120000
        tipoPolizza nullable: false
        telaio nullable: true, validator: { telaio, polizza ->
            if(polizza.tipoPolizza == TipoPolizza.EXTENSION && !telaio) return "polizze.telaio.nullable"
        }
        telaioDemo nullable: true, unique:true, validator: { telaioDemo, polizza ->
            if(polizza.tipoPolizza == TipoPolizza.DEMO_OPEL && !telaioDemo) return "polizze.telaioDemo.nullable"
        }
        nome nullable: true, validator: { nome, polizza ->
            if(polizza.tipoPolizza == TipoPolizza.EXTENSION && !nome) return "polizze.nome.nullable"
        }
        cognome nullable: true, validator: { cognome, polizza ->
            if(polizza.tipoPolizza == TipoPolizza.EXTENSION && !cognome) return "polizze.cognome.nullable"
        }
        targa nullable: true, unique: true, validator: { targa, polizza ->
            if(polizza.tipoPolizza == TipoPolizza.DEMO_OPEL && !targa) return "polizze.targa.nullable"
        }
        modello nullable: true, validator: { modello, polizza ->
            if(polizza.tipoPolizza == TipoPolizza.DEMO_OPEL && !modello) return "polizze.modello.nullable"
        }
        marca nullable: true, validator: { marca, polizza ->
            if(polizza.tipoPolizza == TipoPolizza.DEMO_OPEL && !marca) return "polizze.marca.nullable"
        }
    }
    static mapping = {

    }
    def afterInsert(){
        if(tipoPolizza == TipoPolizza.DEMO_OPEL){
            noPolizza = "DO" + id.toString().padLeft(8, "0")

        }else{
            noPolizza = "EW" + id.toString().padLeft(8, "0")
        }

    }
}
