package gmac.polizze

import gmac.utenti.Dealer

enum TipoDocumento {
    LIBRETTO, CERTIFICATO
}

class Documenti {
    TipoDocumento tipo=TipoDocumento.LIBRETTO
    Date dateCreated, lastUpdated
    String fileName
    String targa
    static belongsTo = [dealer: Dealer]
    boolean inviatoMail =false
    byte[] fileContent
    static constraints = {
        fileName: unique: true
        fileContent nullable: false, size: 1..(1024 * 1024 * 5)
        tipo nullable: false
        targa nullable: false
    }

    static mapping = {
        table "documenti_polizza"
    }
}
