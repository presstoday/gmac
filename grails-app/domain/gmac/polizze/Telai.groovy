package gmac.polizze

import org.grails.databinding.BindingFormat

class Telai {
    String modello
    String trasmissione
    String cilindrata
    String versione
    @BindingFormat("uppercase")String targa
    String telaio
    Date dataImmatricolazione

    static constraints = {
        telaio blank: false, unique: true
        targa blank: true, nullable: true
        modello nullable: true
        trasmissione nullable: true
        cilindrata nullable: true
        versione nullable: true
        dataImmatricolazione nullable: false

        dataImmatricolazione validator: { data ->
            /* if(data > dataMassima) return ["telai.dataImmatricolazione.data.max", data, dataMassima]
            def today = new Date(), minimo = use(TC) { today - 4.years }, massimo = use(TC) { today - 8.years }
            if(data > minimo) return "telai.dataImmatricolazione.min.notmet"
            if(data < massimo) return "telai.dataImmatricolazione.max.exceeded"*/
        }
    }
}
