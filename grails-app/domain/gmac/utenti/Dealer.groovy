package gmac.utenti

import gmac.polizze.Documenti
import gmac.polizze.Polizza
import org.grails.databinding.BindingFormat

class Dealer extends Utente  {

    @BindingFormat("capitalize")String ragioneSociale
    @BindingFormat("uppercase") String piva
    @BindingFormat("capitalize") String indirizzo
    @BindingFormat("capitalize") String localita
    @BindingFormat("capitalize") String provincia
    String cap,telefono

    //String codice
    boolean attivo = false, accettazione=false

    static constraints = {
        ragioneSociale blank: false
        piva blank: false
        provincia nullable: true
        cap nullable: true
        telefono nullable: true

    }
    static hasMany = [polizze: Polizza, documenti: Documenti]

    static mapping = {
        table "dealers"
    }

    String toString() { return "${username}" }
    Set<Ruolo> getDefaultRoles() { return [Ruolo.DEALER] }
}
