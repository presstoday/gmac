
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="navigazione"/>
    <title>DEMO OPEN</title>
    <script language="javascript">
        if(history.length > 0) history.forward()
    </script>
</head>
<% String env = grails.util.Environment.current.name
%>
<body>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="text-center">Creazione Polizza</h3>
            </div>
            <div class="panel-body">
                <g:if test="${flash.error}"><div class="labelErrore marginesotto">${flash.error.toString().replaceAll("\\<.*?>","")} </div></g:if>
                %{--<g:if test="${flash.message}"><div class="labelErrore marginesotto">${flash.message.toString().replaceAll("\\<.*?>","")} </div></g:if>--}%
                %{--<g:hasErrors bean="${polizza}">
                    <g:renderErrors bean="${polizza}"/>
                </g:hasErrors>--}%
                <f:form id="aggiungiDemoform" method="post" class="row form-horizontal" enctype="multipart/form-data">
                    <div class="datiPoli">
                    </div>
                    <div class="col-md-12">
                        <input type="hidden" name="aggiungi" id="aggiungi" value="false"/>
                        <input type="hidden" name="annullaIns" id="annullaIns" value="false"/>
                        <div class="col-md-5">
                            <label class="testoParagraph control-label">DEALER</label>
                            <input type="text" align="center" name="dealerNome" class="col-md-7 form-control targaMaiuscola" id="dealerNome" value="${utente.ragioneSociale}" readonly="true" >
                        </div>
                        <div class="col-md-5">
                            <label class="testoParagraph control-label">&nbsp;&nbsp;&nbsp;</label>
                            <input type="text" align="center" name="dealerMail" class="form-control" id="dealerMail" value="${utente.email}" readonly="true" >
                            <input type="hidden" align="center" name="dealer" class="form-control" id="dealer" value="${utente.id}" readonly="true" >
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <label class="testoParagraph control-label marginesopra">TARGA</label>
                            <input type="text" align="center" name="targa" class="form-control targaMaiuscola" id="targa" required="true" value="">
                        </div>
                        <div class="col-md-4">
                            <label class="testoParagraph control-label marginesopra">TELAIO</label>
                            <input class="form-control targaMaiuscola" type="text" name="telaioDemo"  maxlength="17" id="telaioDemo" required="true" value="" />

                        </div>
                        <div class=" col-md-2">
                            <label class="testoParagraph control-label">DATA IMMATRICOLAZIONE</label>
                            <input type="text" name="dataImmatri" class="form-control" data-provide="datepicker" data-date-language="it"
                                   data-date-format="dd-mm-yyyy" data-date-today-btn="linked" data-date-autoclose="true"
                                   data-date-today-highlight="true" data-date-toggle-active="true"  data-date-end-date="0d"
                                   data-date-disable-touch-keyboard="true" data-date-enable-on-readonly="false" id="dataImmatri" required="true" value=""  >
                        </div>
                        <div class="col-md-3">
                            <label class="testoParagraph control-label marginesopra">VALORE ASSICURATO</label>
                            <input type="number" align="center" name="valoreAssicurato" class="form-control" id="valoreAssicurato" required="true"value="">
                        </div>
                        <div class="col-md-3">
                            <label class="testoParagraph control-label marginesopra">MARCA</label>
                            <select class="form-control" name="marca" id="marca">
                                <g:each var="marchio" in="${marche}">
                                    <option>${marchio}</option>
                                </g:each>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label class="testoParagraph control-label marginesopra">MODELLO</label>
                            <input type="text" align="center" name="modello" class="form-control targaMaiuscola" id="modello" required="true" value="" >
                        </div>
                        <div class="col-md-2">
                            <label class="testoParagraph control-label marginesopra">AUTO NUOVA</label>
                            <select class="form-control" name="nuova" id="nuova">
                                <option value="1" selected >SI</option>
                                <option value="0" >NO</option>
                            </select>
                        </div>
                        <div id ="dataTaglia" class=" col-md-2">
                            <label class="testoParagraph control-label marginesopra">DATA INVIO RICHIESTA</label>
                            <input type="text" align="center" name="dataInvio" class="form-control" id="dataInvio" value="" readonly="true" >
                        </div>
                    </div>
                    <div class="col-md-12 marginesopra">
                        <div id ="dataDecorr" class=" col-md-2">
                            <label class="testoParagraph control-label ">DATA DECORRENZA</label>
                            <input type="text" align="center" name="dataDecorrenza" class="form-control" id="dataDecorrenza" value=""  readonly="true" >
                        </div>
                        <div class="col-md-2">
                            <label class="testoParagraph control-label ">PREMIO</label>
                            <input type="text" align="center" name="premio" class="form-control" id="premio" value="${"100"}"   required="true" readonly="true" >
                        </div>
                        <div class="col-md-6 ">

                            <label class="testoParagraph control-label">ALLEGA LIBRETTO*</label>
                            <input id="input-fileLibretto" type="file"  class="file moduloAdesione" multiple data-preview-file-type="text" name="fileLibretto" data-upload-async="false" data-language="it" data-max-file-count="1" data-show-preview="false" data-show-upload="false" required="true">
                        </div>
                    </div>
                    <div class="col-md-12 marginesopraplus">
                        <br>
                        <div class="col-md-2">
                           %{--<g:link page="1" controller="menu" action="demoOpen" class="btn btn-yellow pull-left"><b>ANNULLA</b></g:link>--}%
                            <a href="" %{--onclick="return annullaInserimento(${utente.id});"--}% class="btn btn-yellow pull-right annullaIns" name="${utente.id}" id="annulla"  title="annulla"><b>ANNULLA</b></a>
                            %{--<button type="submit" id="annulla"  name="annulla" class="btn btn-yellow pull-left"><b>ANNULLA</b></button>--}%
                        </div>
                        <div class="col-md-2 col-md-push-6">
                            %{--<a href=""--}%%{--onclick="return chiamataWS();"--}%%{-- class="btn btn-yellow pull-right" name="aggCoper" id="aggCoper"  title="aggiungi copertura"><b>AGGIUNGI COPERTURA</b></a>--}%
                            <button type="submit" id="aggiungiPol" name="aggiungiPol" class="btn btn-yellow pull-right"><i class="fa fa-plus-square"></i><b> AGGIUNGI COPERTURA</b></button>
                        </div>
                        <div class="col-md-2 col-md-push-6">
                            <button type="submit" id="salva"  name="salva" class="btn btn-yellow pull-right"><b>SALVA E CHIUDI</b></button>
                        </div>
                    </div>
                </f:form>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    $( document ).ready(function() {
       // $(".alert-danger").remove();
        var today=new Date();
        var tomorrow=new Date();
        var giorno=today.getDay();
        if(giorno==5){
            tomorrow.setDate(today.getDate()+3);

        }else if(giorno==6){
            tomorrow.setDate(today.getDate()+2);
        }else{
            tomorrow.setDate(today.getDate()+1);
        }
        var output=formatDate(tomorrow);
        $("#dataDecorrenza").val(output);
        var invioR=formatDate(today);
        $("#dataInvio").val(invioR);
        /***pop-up con le esclusioni polizza***/
        /*$("#modalEsclusioniPolizza").modal({
            backdrop: 'static',
            keyboard: false
        });*/
        <g:if test="${flash.message}">
        swal({
            title: "Polizza salvata!",
            type: "success",
            timer: "2000",
            showConfirmButton: false
        });

        </g:if>
        <g:if test="${flash.success}">
        swal({
            title: "${flash.success}",
            type: "success",
            timer: "2000",
            showConfirmButton: false
        });

        </g:if>
        <g:if test="${flash.error}">
        swal({
            title: "Errore nel salvataggio della polizza",
            type: "error",
            timer: "2000",
            showConfirmButton: false
        });
        </g:if>
    });
    var txttargaCliente="";
    var txttelaioCliente="";
    var txtmodelloCliente="";
    var txtdataImmCliente="";
    var txtdataDecoCliente="";
    var txtdataInvioCliente="";
    var txtvaloreCliente="";
    var txtimmCliente="";
    var txtpremio="";
    function verificaTarga() {
        var valore= $.trim($("#targa").val());
        var regtarga=/^[a-zA-Z]{2}[0-9]{3}[a-zA-Z]{2}$/;
        if(valore!='' && regtarga.test(valore)){
            return true;
        }
        else {
            if(!regtarga.test(valore) && valore!=''){txttargaCliente="il formato della targa è sbagliato";}
            else {txttargaCliente="compilare il campo targa";}
            return false;
        }
    }
    function verificaTelaio() {
        var valore= $.trim($("#telaioDemo").val());
        if (valore!=''){
            return true;
        }
        else {
            txttelaioCliente="compilare il telaio";
            return false;
        }
    }
    function verificaModello() {
        var valore= $.trim($("#modello").val());
        if (valore!=''){ return true; }
        else {
            txtmodelloCliente="compilare il modello";
            return false;
        }
    }
    function verificaValore() {
        var valore= $.trim($("#valoreAssicurato").val());

        if (valore!='' && valore >0){
            return true;
        }
        else {
            txtvaloreCliente="il valore assicurato deve essere superiore a 0";
            return false;
        }
    }
    function verificaPremio() {
        var valore= $.trim($("#premio").val());
        if (valore!=''){ return true; }
        else {
            txtpremio="il premio non ha un valore";
            return false;
        }
    }
    function verificaDataImmatricolazione(){
        var data1 = $("#dataImmatri").val();
        //var dateformat1= new Date(parseInt(data1[2], 10), parseInt(data1[1], 10)-1, parseInt(data1[0], 10));
        if (data1 !=''){ return true;  }
        else{
            txtdataImmCliente="compilare la data Immatricolazione";
            return false;
        }
    }
    function verificaDataDecorrenza(){
        var data1 = $("#dataDecorrenza").val();
        //var dateformat1= new Date(parseInt(data1[2], 10), parseInt(data1[1], 10)-1, parseInt(data1[0], 10));
        if (data1 !=''){ return true;  }
        else{
            txtdataImmCliente="la data decorrenza non è stata caricata, contattare Mach1";
            return false;
        }
    }
    function verificadataInvio(){
        var data1 = $("#dataInvio").val();
        //var dateformat1= new Date(parseInt(data1[2], 10), parseInt(data1[1], 10)-1, parseInt(data1[0], 10));
        if (data1 !=''){ return true;  }
        else{
            txtdataInvioCliente="compilare la data d'Invio della richiesta";
            return false;
        }
    }

    /***funzione formatazzione data ***/
    function formatDate(date) {
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();
        return  (day < 10 ? '0' : '') + day + '-' +(month < 10 ? '0' : '') + month + '-' + year;
    }

    /** verifico che la targa è stata compilata*/
    $("#targa").on("change", function(event){
        if( (verificaTarga() && verificaTelaio()) && verificaDataImmatricolazione()  && verificaModello() && verificaValore() ){
            $( ".labelErrore" ).remove();
        }else{
            if(!verificaTarga()){

                $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append(txttargaCliente);
            }else{
                $( ".labelErrore" ).remove();
            }

        }

    });
    $("#telaioDemo").on("change", function(event) {
        if( (verificaTarga() && verificaTelaio()) && verificaDataImmatricolazione()  && verificaModello() && verificaValore() ){
            $( ".labelErrore" ).remove();
        }else{
             /*$( ".labelErrore" ).remove();*/
             $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'></span></div>" );
             if(!verificaTelaio()) $( ".testoAlert").append(txttelaioCliente).append("<br>");


        }
    });
    $("#valoreAssicurato").on("change", function(event) {
        if( (verificaTarga() && verificaTelaio()) && verificaDataImmatricolazione()  && verificaModello() && verificaValore() ){
            $( ".labelErrore" ).remove();
        }else{
            /*$( ".labelErrore" ).remove();*/
            $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'></span></div>" );
            if(!verificaValore()) $( ".testoAlert").append(txtvaloreCliente).append("<br>");


        }
    });

    $("#dataInvio").on("changeDate", function(event) {
        if( (verificaTarga() && verificaTelaio()) && verificaDataImmatricolazione()  && verificaModello() && verificaValore() ){
            $( ".labelErrore" ).remove();
        }else{
           /* $( ".labelErrore" ).remove();*/
            $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'></span></div>" );
            if(!verificadataInvio()) $( ".testoAlert").append(txtdataInvioCliente).append("<br>");

        }
    });
    $("#dataImmatri").on("changeDate", function(event) {
        if( (verificaTarga() && verificaTelaio()) && verificaDataImmatricolazione()  && verificaModello() && verificaValore() ){
            $( ".labelErrore" ).remove();
        }else{
            /*$( ".labelErrore" ).remove();*/
            $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'></span></div>" );
            if(!verificaDataImmatricolazione()) $( ".testoAlert").append(txtdataImmCliente).append("<br>");

        }
    });

    $('#salva').click(function(e) {
        e.preventDefault();
        input = document.getElementById('input-fileLibretto');
        var aggiungi=$("#aggiungi").val(false);
        var annullaPoli=$("#annullaIns").val(false);
        if(verificaTarga() && verificaTelaio() && verificaDataImmatricolazione() && verificaValore() && verificaModello() && verificadataInvio() && verificaPremio() && verificaDataDecorrenza()){
            $( ".labelErrore" ).remove();
            $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'></span></div>" );
            if (!input) {
                alert("selezionare un file da caricare");
            }
            else if (!input.files) {
                alert("il file che è stato allegato non è supportato da questo browser");
            }
            else if (!input.files[0]) {
                $( ".testoAlert").append("allegare il libretto").append("<br>");
            }else{
                $("#aggiungiDemoform").submit();
            }
        }else{
            $( ".labelErrore" ).remove();
            $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'></span></div>" );
            if(!verificaTarga()) $( ".testoAlert").append(txttargaCliente).append("<br>");
            if(!verificaTelaio()) $( ".testoAlert").append(txttelaioCliente).append("<br>");
            if(!verificaDataImmatricolazione()) $( ".testoAlert").append(txtdataImmCliente).append("<br>");
            if(!verificaValore()) $( ".testoAlert").append(txtvaloreCliente).append("<br>");
            if(!verificaModello()) $( ".testoAlert").append(txtmodelloCliente).append("<br>");
            if(!verificadataInvio()) $( ".testoAlert").append(txtdataInvioCliente).append("<br>");
            if(!verificaPremio()) $( ".testoAlert").append(txtpremio).append("<br>");
            if(!verificaDataDecorrenza()) $( ".testoAlert").append(txtdataDecoCliente).append("<br>");
            if (!input) {
                alert("Um, couldn't find the fileinput element.");
            }
            else if (!input.files) {
                alert("This browser doesn't seem to support the `files` property of file inputs.");
            }
            else if (!input.files[0]) {
                $( ".testoAlert").append("allegare il libretto").append("<br>");
            }
            swal({
                title: "controllare che la targa, il telaio, la data Immatricolazione, il valore, il modello, la data d'invio, il premio, e la data decorrenza siano compilati!",
                type: "error",
                timer: "2000",
                showConfirmButton: false
            });
        }
        //do other stuff when a click happens
    });

    $('#aggiungiPol').click(function(e) {
        e.preventDefault();
        input = document.getElementById('input-fileLibretto');
        var aggiungi=$("#aggiungi").val(true);
        var annullaPoli=$("#annullaIns").val(false);
        if(verificaTarga() && verificaTelaio() && verificaDataImmatricolazione() && verificaValore() && verificaModello() && verificadataInvio() && verificaPremio() && verificaDataDecorrenza()){
            $( ".labelErrore" ).remove();
            $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'></span></div>" );
            if (!input) {
                alert("Um, couldn't find the fileinput element.");
            }
            else if (!input.files) {
                alert("This browser doesn't seem to support the `files` property of file inputs.");
            }
            else if (!input.files[0]) {
                $( ".testoAlert").append("allegare il libretto").append("<br>");
            }else{
                $("#aggiungiDemoform").submit();
            }
        }else{
            $( ".labelErrore" ).remove();
            $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'></span></div>" );
            if(!verificaTarga()) $( ".testoAlert").append(txttargaCliente).append("<br>");
            if(!verificaTelaio()) $( ".testoAlert").append(txttelaioCliente).append("<br>");
            if(!verificaDataImmatricolazione()) $( ".testoAlert").append(txtdataImmCliente).append("<br>");
            if(!verificaValore()) $( ".testoAlert").append(txtvaloreCliente).append("<br>");
            if(!verificaModello()) $( ".testoAlert").append(txtmodelloCliente).append("<br>");
            if(!verificadataInvio()) $( ".testoAlert").append(txtdataInvioCliente).append("<br>");
            if(!verificaPremio()) $( ".testoAlert").append(txtpremio).append("<br>");
            if(!verificaDataDecorrenza()) $( ".testoAlert").append(txtdataDecoCliente).append("<br>");
            if (!input) {
                alert("Um, couldn't find the fileinput element.");
            }
            else if (!input.files) {
                alert("This browser doesn't seem to support the `files` property of file inputs.");
            }
            else if (!input.files[0]) {
                $( ".testoAlert").append("allegare il libretto").append("<br>");
            }
            swal({
                title: "controllare che la targa, il telaio, la data Immatricolazione, il valore, il modello, la data d'invio, il premio, e la data decorrenza siano compilati!",
                type: "error",
                timer: "2000",
                showConfirmButton: false
            });
        }
        //do other stuff when a click happens
    });
    $('.annullaIns').click(function(e) {
        e.preventDefault();
        $( ".labelErrore" ).remove();
        var data1 = $.trim($(this).val());
        var nome=this.name;
        annullaInserimento(nome);
    });
    function annullaInserimento(nome){
        $.post("${createLink(controller: "polizze", action: "annullaInserimento")}",{  dealer:nome,  _csrf: "${request._csrf.token}"}, function(response) {
            var risposta=response.risposta;
            if(risposta==false){
                var errore=response.errore;
                $( ".labelErrore" ).remove();
                $( ".inner" ).after( "<div class=\"labelErrore marginesotto\"><p>"+response.errore+"</p></div>" );
            }else{
               var  environment = "${env}";
                if(environment!="development"){
                    var url = "/OPEL/menu/demoOpen";
                }else{
                    var url = "/menu/demoOpen";
                }
                window.location.href = url;
                //alert(window.location.href);
            }
        }, "json");
    }
</script>
</body>
</html>