
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="navigazione"/>
    <title>GARANZIA MOTORE TRASMISSIONE CAMBIO</title>
    <script language="javascript">
        if(history.length > 0) history.forward()
    </script>
</head>

<body>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="text-center">Creazione Polizza</h3>
            </div>
            <div class="panel-body">
                <g:renderFlash/>
                <g:hasErrors bean="${polizza}">
                    <g:renderErrors bean="${polizza}"/>
                </g:hasErrors>
                <f:form id="creaExtensionform" method="post" class="row form-horizontal">
                    <div class="col-md-12">
                        <div class="col-md-9">
                            <label class="testoParagraph control-label">Telaio</label>
                            <div id="telai">
                                <input type="text" align="center" name="telaiotrovato.telaio"  class="form-control typeahead" id="telaio-input" value="${telaiotrovato?.telaio ?: ""}" required="true" placeholder="inserire il numero di telaio" data-toggle="tooltip" data-placement="top" title="Se non trova il telaio nell'elenco chiamare MACH1 : 02 00621286">  Selezionare il telaio tra quelli disponibili
                                <input type="hidden" name="telaio.id" id="id-telaio" value="${telaiotrovato?.id ?: ""}" >
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <label class="testoParagraph control-label">Dealer</label>
                            <input type="text" align="center" name="dealerNome" class="form-control" id="dealerNome" value="${utente.ragioneSociale}" readonly="true" >
                            <%--<p class="form-control-static">${utente.ragioneSociale}</p>--%>
                            <input type="hidden" align="center" name="dealer.id" class="form-control" id="dealer.id" value="${utente.id}" readonly="true" >
                        </div>
                        <div class="col-md-4">
                            <label class="testoParagraph control-label">Tipo polizza</label>
                            <input type="text" align="center" name="tipoPolizza" class="form-control" id="tipoPolizza" value="GARANZIA MOTORE TRASMISSIONE CAMBIO" readonly="true" >
                        </div>
                        <div class="col-md-2">
                            <label class="testoParagraph control-label">Durata</label>
                            <input type="text" align="center" name="durata" class="form-control" id="durata" value="12 mesi" readonly="true" >
                        </div>
                        <div class="col-md-2">
                            <label class="testoParagraph control-label">Marca</label>
                            <input type="text" align="center" name="marca" class="form-control" id="marchio" value="OPEL" readonly="true">
                        </div>
                    </div>
                        <div class="col-md-12">
                            <div class= "col-md-4" >
                                <label class="testoParagraph control-label">Nome proprietario</label>
                                <input type="text" name="nome" class="form-control" id="nome"  value="" required="true">
                            </div>
                            <div class= "col-md-4" >
                                <label class="testoParagraph control-label">Cognome proprietario</label>
                                <input type="text" name="cognome" class="form-control" id="cognome" value="" required="true">
                            </div>
                            <div class= "col-md-2" >
                                <label class="testoParagraph control-label">KM</label>
                                <input type="number" name="km" class="form-control" id="km"  data-toggle="tooltip" title="I km non possono essere superiori a 120.000" value="" required="true">
                            </div>
                            <div class="col-md-2">
                                <label class="testoParagraph control-label">Targa</label>
                                <input type="text" align="center" name="targa" class="form-control targaMaiuscola" id="targa" value="" readonly="true">
                            </div>
                        </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <label class="testoParagraph control-label">Cilindrata</label>
                            <input type="text" align="center" name="cilindrata" class="form-control" id="cilindrata" value="" readonly="true" >
                        </div>
                        <div class="col-md-2">
                            <label class="testoParagraph control-label">Modello</label>
                            <input type="text" align="center" name="modello" class="form-control" id="modello" readonly="true" value="" >
                        </div>

                        <div class= "col-md-4" >
                            <label class="testoParagraph control-label">Trasmissione</label>
                            <input type="text" name="trasmissione" class="form-control" id="trasmissione"  value="" readonly="true" >
                        </div>

                        <div class="col-md-4">
                            <label class="testoParagraph control-label">Versione</label>
                            <input type="text" align="center" name="versione" class="form-control" id="versione" value="" readonly="true">
                        </div>
                    </div>
                        <div class="col-md-12">
                            <div id ="dataImmat" class=" col-md-2">
                                <label class="testoParagraph control-label">Data immatricolazione</label>
                                <input type="text" data-toggle="tooltip" title="La data no puo' essere inferiore a 4 anni e superiore a 8 anni" name="dataImmatri" class="form-control" data-provide="datepicker" data-date-language="it"
                                       data-date-format="dd-mm-yyyy" data-date-today-btn="linked" data-date-autoclose="true"
                                       data-date-today-highlight="true" data-date-toggle-active="true"
                                       data-date-disable-touch-keyboard="true" data-date-enable-on-readonly="false" id="dataImmatri" value="" readonly="true" >
                            </div>
                            <div id ="dataTaglia" class=" col-md-2">
                                <label class="testoParagraph control-label">Data tagliando</label>
                                <input type="text"  name="dataTagliando" class="form-control" data-provide="datepicker" data-date-language="it"
                                       data-date-format="dd-mm-yyyy" data-date-today-btn="false" data-date-autoclose="true" data-date-end-date="0d" data-date-start-date="-1d"
                                       data-date-today-highlight="false" data-date-toggle-active="true"
                                       data-date-disable-touch-keyboard="true" data-date-enable-on-readonly="false" id="dataTagliando" value="" required="true" >
                            </div>
                            <div id ="dataDecorr" class=" col-md-2">
                                <label class="testoParagraph control-label">Data decorrenza</label>
                                <input type="text" align="center" name="dataDecorrenza" class="form-control" id="dataDecorrenza" value="" readonly="true" >
                            </div>
                            <div id ="dataScade" class=" col-md-2">
                                <label class="testoParagraph control-label">Scadenza copertura</label>
                                <input type="text" align="center" name="dataScadenza" class="form-control" id="dataScadenza"  readonly="true" >
                            </div>
                        </div>
                    <div class="col-md-12">
                        <br>
                        <div class="col-md-2">
                           <g:link page="1" controller="menu" action="extensionWarranty" class="btn btn-yellow pull-left">Annulla</g:link>
                        </div>
                        <div class="col-md-2 col-md-push-8">
                            <button type="submit" id="salva" disabled="true" class="btn btn-yellow pull-right">Salva</button>
                        </div>
                        %{--<button type="submit" name="submit" id="submit" class="btn btn-primary"  onclick="verifica();">Salva</button>--}%
                        %{--<g:submitButton controller="polizze" class="btn btn-warning" value="Salva" action="creaPolizzaExtension" id="salva" name="salva" >Salva</g:submitButton>--}%
                    </div>
                </f:form>
            </div>
        </div>
    </div>
    <div class="modal" id="modalEsclusioniPolizza">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background: #f7d900">
                    <h3 class="article-title text-center"><fa:icon name="warning"/> Esclusioni copertura veicoli</h3>
                </div>
                <div class="modal-body">
                    <div class="ibox">
                        <div class="ibox-content">
                            <p class="testoParagraph">Sono esclusi <strong><u>i seguenti Autoveicoli:</u></strong></p>
                            <ul>
                                <li class="testoParagraph">con oltre 8 (otto) anni di vetustà dalla data di prima immatricolazione;</li>
                                <li class="testoParagraph">con oltre 120.000 km alla data del tagliando;</li>
                                <li class="testoParagraph">autoveicoli che al momento del guasto abbiano una percorrenza superiore di 50.000 Km rispetto ai chilometri registrati alla data di effettuazione del tagliando di manutenzione;</li>
                                <li class="testoParagraph">con un peso a vuoto (tara), più il trasporto/carico - di oltre 3.500 kg o adattati al trasporto di più di 8 persone, incluso il conducente;</li>
                                <li class="testoParagraph">alimentati da motore rotativo, elettrico, ibrido, idrogeno, o con impianto GPL o gas metano non originale della casa costruttrice;</li>
                                <li class="testoParagraph">modificati in difformità agli standard prescritti dalla casa costruttrice;</li>
                                <li class="testoParagraph">autoveicoli sui quali sono state installate attrezzature sperimentali, anche se fornite dalla casa costruttrice;</li>
                                <li class="testoParagraph">autoveicoli con motori elaborati, anche nel caso in cui le modifiche siano state apportate da elaboratori ufficiali della casa costruttrice;</li>
                                <li class="testoParagraph">autoveicoli che siano sottoposti ad un uso improprio o diverso da quello privato;</li>
                                <li class="testoParagraph">adibiti ad uso professionale come: taxi, noleggio con conducente, scuola guida e scuola di pilotaggio, veicoli a noleggio a breve e lungo termine, ambulanze o soccorso pubblico o privato, servizi di polizia pubblica o privata, autoveicoli adibiti al soccorso stradale;</li>
                                <li class="testoParagraph">autobus, autocarri, autotreni, autosnodati, autocaravan, trattori stradali, rimorchi, autoveicoli per trasporti e/o uso speciale, ciclomotori, motoveicoli, macchine agricole, motoveicoli e autoveicoli d'epoca e di interesse storico e collezionistico (art. 60 Cod. della Strada);</li>
                                <li class="testoParagraph">autoveicoli utilizzati per il servizio di trasporto di cose per conto terzi, e/o spedizioni postali;</li>
                                <li class="testoParagraph">autoveicoli utilizzati per competizioni sportive di qualsiasi genere, o che siano stati oggetto di modifiche/trasformazioni tecniche;</li>
                                <li class="testoParagraph">autoveicoli già in copertura con la garanzia del venditore o del costruttore;</li>
                                <li class="testoParagraph">autoveicoli con un valore commerciale superiore a € 150.000,00.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button"  id="bottonmodalEsclusioni" class="btn btn-yellow" onclick="return lanciamodalDataTagliando();" data-dismiss="modal">Confermo</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="modal" id="modalKmallaVendita">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header" style="background: #f7d900">
                    <h3 class="article-title text-center"><fa:icon name="times-circle"/>Attenzione</h3>
                </div>
                <div class="modal-body">
                        <p class="testoParagraph">i Km non possono essere superiori a 120000</p>
                </div>
                <div class="modal-footer">
                    <button type="button" id="bottonkm" class="btn btn-yellow" data-dismiss="modal">Ho capito</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="modal" id="modalDataTagliando">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header" style="background: #f7d900">
                    <h3 class="article-title text-center"><fa:icon name="times-circle"/>Attenzione</h3>
                </div>
                <div class="modal-body">
                    <p class="testoParagraph">L'attivazione della polizza può essere effettuata entro le 24 ore successive alla data del tagliando</p>
                </div>
                <div class="modal-footer">
                    <button type="button" id="bottonDataTagliando" class="btn btn-yellow" data-dismiss="modal">Ho capito</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <div class="modal" id="modalDataImmatr48">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header" style="background: #f7d900">
                    <h3 class="article-title text-center"><fa:icon name="times-circle"/>Attenzione</h3>
                </div>
                <div class="modal-body">
                    <p class="testoParagraph">la data di immatricolazione è inferiore a 4 anni</p>
                </div>
                <div class="modal-footer">
                    <button type="button" id="bottonDataImmatr48" class="btn btn-yellow" data-dismiss="modal">Ho capito</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <div class="modal" id="modalDataImmatr96">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header" style="background: #f7d900">
                    <h3 class="article-title text-center"><fa:icon name="times-circle"/>Attenzione</h3>
                </div>
                <div class="modal-body">
                    <p class="testoParagraph">la data di immatricolazione è superiore a 8 anni</p>
                </div>
                <div class="modal-footer">
                    <button type="button" id="bottonDataImmatr96" class="btn btn-yellow" data-dismiss="modal">Ho capito</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <div class="modal" id="modalTelaio">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header" style="background: #f7d900">
                    <h3 class="article-title text-center"><fa:icon name="times-circle"/>Attenzione</h3>
                </div>
                <div class="modal-body">
                    <p class="testoParagraph">esiste una polizza creata negli ultimi 6 mesi per questo telaio</p>
                </div>
                <div class="modal-footer">
                   %{-- <g:link controller="menu" action="extensionWarranty"><div class="btn btn-yellow col-md-offset-2  col-md-6"><b>Ok</b></div></g:link>--}%
                    <button type="button" id="bottonTelaio" onclick="" class="btn btn-yellow" data-dismiss="modal">Ho capito</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <div class="modal" id="modalTarga">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header" style="background: #f7d900">
                    <h3 class="article-title text-center"><fa:icon name="times-circle"/>Attenzione</h3>
                </div>
                <div class="modal-body">
                    <p class="testoParagraph">Inserire la targa.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" id="bottonTarga" class="btn btn-yellow" data-dismiss="modal">Ho capito</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
</div>
<script type="text/javascript">
        $( document ).ready(function() {
            /***pop-up con le esclusioni polizza***/
            $("#modalEsclusioniPolizza").modal({
                backdrop: 'static',
                keyboard: false
            });
        });
        /***funzione formatazzione data ***/
    function formatDate(date) {
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();
        return  (day < 10 ? '0' : '') + day + '-' +(month < 10 ? '0' : '') + month + '-' + year;
    }
        /***verifico che i km non siano superiori a 120000 ***/
    $("#km").on("change", function(event) {
        var valore= $("#km").val();
        if(valore!=''){
            if ( valore>120000){
                $("#modalKmallaVendita").modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $("#salva").prop('disabled', true);
            }else if (valore<120000 && verificaDataTagliando()==true && verificaTarga()==true && verificaNome()==true && verificaCognome()==true) {
                $("#salva").prop('disabled', false);
            }
        }else{
            $("#salva").prop('disabled', true);
        }
    });
    /** verifico che la targa è stata compilata*/
    $("#targa").on("change", function(event){
        if(verificaTarga()==true){
            $("#nome").prop('disabled', false);
            $("#cognome").prop('disabled', false);
            $("#km").prop('disabled', false);
            $("#dataTagliando").prop('disabled', false);
        }
    });
    $("#nome").on("change", function(event){
        if(verificaNome()==true && verificaDataTagliando()==true && verificaTarga()==true && verificaCognome()==true){
            $("#salva").prop('disabled', false);
        }else{
            $("#salva").prop('disabled', true);
        }
    });
    $("#cognome").on("change", function(event){
        if(verificaNome()==true && verificaDataTagliando()==true && verificaTarga()==true && verificaCognome()==true){
            $("#salva").prop('disabled', false);
        }else{
            $("#salva").prop('disabled', true);
        }
    });
    /***verifico che la data tagliando non sia superiore a 24 ore prima o dopo e inoltre setto la data decorrenza e scadenza***/
    $("#dataTagliando").on("changeDate", function(event) {
        var data1 = $("#dataTagliando").val().split('-');
        var dateformat1= new Date(parseInt(data1[2], 10), parseInt(data1[1], 10)-1, parseInt(data1[0], 10));
        dateformat1.setHours(new Date().getHours());
        dateformat1.setMinutes(new Date().getMinutes());
        dateformat1.setSeconds(new Date().getSeconds());
        var dateformat2= new Date(parseInt(data1[2], 10), parseInt(data1[1], 10)-1, parseInt(data1[0], 10)+30);
        dateformat2.setHours(new Date().getHours());
        dateformat2.setMinutes(new Date().getMinutes());
        dateformat2.setSeconds(new Date().getSeconds());
        if(data1 != ''){
            if(verificaKM()==true && verificaTarga()==true && verificaCognome()==true && verificaNome()==true){
                $("#salva").prop('disabled', false);
            }
            else{
                $("#salva").prop('disabled', true);
            }
            $('#dataDecorrenza').val(isNaN(dateformat2.getFullYear()) ? '' : formatDate(dateformat2));

            var data2 = $("#dataDecorrenza").val().split('-');
            var dateformat3 = new Date(parseInt(data2[2], 10), parseInt(data2[1], 10)-1, parseInt(data2[0], 10));

            dateformat3.setFullYear(dateformat3.getFullYear() + 1);
            dateformat3.setHours(new Date().getHours());
            dateformat3.setMinutes(new Date().getMinutes());
            dateformat3.setSeconds(new Date().getSeconds());
            $('#dataScadenza').val(isNaN(dateformat3) ? '' : formatDate(dateformat3));
        }else{
            $('#dataDecorrenza').val('');
            $('#dataScadenza').val('');
            $("#salva").prop('disabled', true);
        }

    });

    /***funzione che verifica lo stato del campo KM***/
    function verificaKM() {
        var valore= $("#km").val();
        if (valore>0 && valore>120000){ return false; }
        else if (valore>0 && valore<120000 && valore !=''){ return true; }
    }
    function verificaTarga() {
        var valore= $("#targa").val();
        var regtarga=/^[a-zA-Z]{2}[0-9]{3,4}[a-zA-Z]{2}$/;
        if(valore!='' && regtarga.test(valore)){ return true; }
        else  return false;
    }
    /***funzione che verifica lo stato del campo data tagliando***/
    function verificaDataTagliando(){
        var data1 = $("#dataTagliando").val();
        //var dateformat1= new Date(parseInt(data1[2], 10), parseInt(data1[1], 10)-1, parseInt(data1[0], 10));
        if (data1 !=''){ return true;  }
        else{return false;  }
    }
    function verificaNome(){
       // var data1=$("#nome").val();
        if($.trim($("#nome").val()) != '' ){return true;}
        else {return false}
    }
    function verificaCognome(){
        if($.trim($("#cognome").val()) != '' ){return true;}
        else {return false}
    }
    /***funzione che lancia modal con nota sulla data tagliando***/
    function lanciamodalDataTagliando(){
        $("#modalDataTagliando").modal({
            backdrop: 'static',
            keyboard: false
        });
    }
    /***typeahead per la ricerca telaio***/
    $('#telai .typeahead').typeahead({
                hint: true,
                highlight: true,
                minLength: 6,
                classNames: {
                    menu: 'popover',
                    dataset: 'popover-content'
                }
            }, {
                name: "elencotelai",
                display: "telaio",
                limit: 20,
                source: new Bloodhound({
                    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    remote: {
                        url: '${createLink(action: "getTelai", params: [elencoTelaio: "notelaio"])}',
                        wildcard: 'notelaio'
                    }
                }),
                templates:{
                    header: Handlebars.compile( '<p class="testoTypeahead"><strong>Telai disponibili:</strong></p>'),
                    suggestion: Handlebars.compile('<div>{{#if telaio}} {{telaio}} {{else}} <p class="testoTypeahead"><strong><i>il telaio inserito non si trova nell\'elenco di telai disponibili</i></strong></p>{{/if}}</div>')
                }
            }
    ).on("typeahead:select", function(event, selected) {
        if(selected.id >0){
            if(selected.telaio){
                $.post("${createLink(controller: "polizze", action: 'getPolizza')}",{ noTelaio: selected.telaio,  _csrf: "${request._csrf.token}"}, function(response) {
                    var risposta=response.risposta;
                    if(risposta==false){
                        /***questo verra implementato quando avremmo i telai aggiornati*/
                        $("#modalTelaio").modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        $("#nome").prop('readonly', true);
                        $("#cognome").prop('readonly', true);
                        $("#km").prop('readonly', true);
                        $("#dataTagliando").prop('readonly', true);
                        $("#salva").prop('disabled', true);
                    }else{
                        $("#nome").prop('readonly', false);
                        $("#cognome").prop('readonly', false);
                        $("#km").prop('readonly', false);
                        $("#dataTagliando").prop('readonly', false);
                    }
                }, "json");
            }
        }
        $("#id-telaio").val(selected.id);
        $("#modello").val(selected.modello);
        $("#cilindrata").val(selected.cilindrata);
        $("#trasmissione").val(selected.trasmissione);
        $("#versione").val(selected.versione);
        $("#nome").val("");
        $("#cognome").val("");
        $("#km").val("");
        $("#dataTagliando").val("");
        var dataImmat=selected.dataImmatricolazione.split('-');
        var dataImmatricolazione= new Date(parseInt(dataImmat[0], 10), parseInt(dataImmat[1], 10)-1, parseInt(dataImmat[2], 10)+1);
        $("#dataImmatri").val(formatDate(dataImmatricolazione));
        $.post("${createLink(action: 'datiTelai')}",{ id: selected.id, _csrf: "${request._csrf.token}"}, function(response) {
            /**verifico il formato della targa*/
            if(response.id>0){
                /***verifico se la data immatricolazione e inferiore a 4 anni e superiore a 8 anni**/
                if(response.checkDataImmatr48){
                    $("#modalDataImmatr48").modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                    $("#nome").prop('disabled', true);
                    $("#cognome").prop('disabled', true);
                    $("#km").prop('disabled', true);
                    $("#dataTagliando").prop('disabled', true);
                    if(!response.checkTarga){
                        $("#targa").val("");
                    }else{
                        $("#targa").val(response.targa);
                    }
                    $("#salva").prop('disabled', true);

                }
                else if(response.checkDataImmatr96){
                    $("#modalDataImmatr96").modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                    $("#nome").prop('disabled', true);
                    $("#cognome").prop('disabled', true);
                    $("#km").prop('disabled', true);
                    $("#dataTagliando").prop('disabled', true);
                    if(!response.checkTarga){
                        $("#targa").val("");
                    }else{
                        $("#targa").val(response.targa);
                    }
                    $("#salva").prop('disabled', true);
                }else{
                    if(!response.checkTarga){
                        $("#modalTarga").modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        $("#targa").prop('readonly', false);
                        $("#targa").val("");
                        $("#targa").css("backgroundColor","#f7d900");
                        $("#targa").css("color","#000");
                        $("#targa").css("font-weight","bold");
                        $("#nome").prop('disabled', true);
                        $("#cognome").prop('disabled', true);
                        $("#km").prop('disabled', true);
                        $("#dataTagliando").prop('disabled', true);
                        $("#salva").prop('readonly', true);
                    }else{
                        $("#targa").val(response.targa);
                        $("#targa").prop('readonly', true);
                        $("#targa").css("backgroundColor","#adafae");
                        $("#targa").css("font-weight","normal");
                        $("#targa").css("color","#fff");
                        $("#nome").prop('disabled', false);
                        $("#cognome").prop('disabled', false);
                        $("#km").prop('disabled', false);
                        $("#dataTagliando").prop('disabled', false);
                        $("#salva").prop('readonly', true);
                    }

                }
            }

        });
    }).on("blur", function(event) {
        var input = $(this);
        if(input.val() == "") {
            input.typeahead("val", "");
            $("#id-telaio").val(null);
            $.post("${createLink(action: 'datiTelai')}",{ id: null, _csrf: "${request._csrf.token}"}, function(response) {});
        }
    });
</script>
</body>
</html>