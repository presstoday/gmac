
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="navigazione"/>
    <title>creazione polizza RCA Gratuita</title>
    <script language="javascript">
        if(history.length > 0) history.forward()
    </script>
</head>

<body>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="text-center">Creazione Polizza</h3>
            </div>
            <div class="panel-body">
                <g:renderFlash/>
                <f:form id="creaRCAform" method="post" class="row form-horizontal">
                    <div class="col-md-12">
                        <div class="col-md-9">
                            <label class="testoParagraph control-label">Telaio</label>
                            <div id="telai">
                                <input type="text" align="center" name="telaiotrovato.telaio" class="form-control typeahead" id="telaio-input" value="${telaiotrovato?.telaio ?: ""}" required="true" placeholder="inserire il numero di telaio" data-toggle="tooltip" data-placement="top" title="Se non trova il telaio nell'elenco chiamare MACH1 : 02 30465068">
                                <input type="hidden" name="telaiotrovato.id" id="id-telaio" value="${telaiotrovato?.id ?: ""}" >
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <label class="testoParagraph control-label">Dealer</label>
                            <input type="text" align="center" name="dealer" class="form-control" id="dealer" value="${utente.ragioneSociale}" readonly="true" >
                        </div>
                        <div class="col-md-2">
                            <label class="testoParagraph control-label">Tipo polizza</label>
                            <input type="text" align="center" name="tipoPolizza" class="form-control" id="tipoPolizza" value="RCA Gratuita" readonly="true" >
                        </div>
                        <div class="col-md-2">
                            <label class="testoParagraph control-label">Targa</label>
                            <input type="text" align="center" name="targa" class="form-control" id="targa" value="" readonly="true">
                        </div>
                        <div class="col-md-2">
                            <label class="testoParagraph control-label">Marca</label>
                            <input type="text" align="center" name="marca" class="form-control" id="marchio" value="OPEL" readonly="true">
                        </div>
                        <div class="col-md-2">
                            <label class="testoParagraph control-label">Durata</label>
                            <input type="text" align="center" name="durata" class="form-control" id="durata" value="12 mesi" readonly="true" >
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class= "col-md-4" >
                            <label class="testoParagraph control-label">Nome propietario</label>
                            <input type="text" name="nomeproprietario" class="form-control" id="nomeproprietario"  value="" required="true">
                        </div>
                        <div class= "col-md-4" >
                            <label class="testoParagraph control-label">Cognome propietario</label>
                            <input type="text" name="cognomeproprietario" class="form-control" id="cognomeproprietario" value="" required="true">
                        </div>
                        <div class= "col-md-2" >
                            <label class="testoParagraph control-label">KM</label>
                            <input type="number" name="kmAllaVendita" class="form-control" id="kmAllaVendita"  data-toggle="tooltip" title="I km non possono essere superiori a 120.000" value="" required="true">
                        </div>
                        <div class="col-md-2">
                            <label class="testoParagraph control-label">Cilindrata</label>
                            <input type="text" align="center" name="cilindrata" class="form-control" id="cilindrata" value="" readonly="true" >
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <label class="testoParagraph control-label">Modello</label>
                            <input type="text" align="center" name="modello" class="form-control" id="modello" readonly="true" value="" >
                        </div>

                        <div class= "col-md-4" >
                            <label class="testoParagraph control-label">Trasmissione</label>
                            <input type="text" name="trasmissione" class="form-control" id="trasmissione"  value="" readonly="true" >
                        </div>

                        <div class="col-md-4">
                            <label class="testoParagraph control-label">Versione</label>
                            <input type="text" align="center" name="versione" class="form-control" id="versione" value="" readonly="true">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div id ="dataImmat" class=" col-md-2">
                            <label class="testoParagraph control-label">Data immatricolazione</label>
                            <input type="text" data-toggle="tooltip" title="La data no puo' essere inferiore a 4 anni e superiore a 8 anni" name="dataImmatri" class="form-control" data-provide="datepicker" data-date-language="it"
                                   data-date-format="dd-mm-yyyy" data-date-today-btn="linked" data-date-autoclose="true"
                                   data-date-today-highlight="true" data-date-toggle-active="true"
                                   data-date-disable-touch-keyboard="true" data-date-enable-on-readonly="false" id="dataImmatri" value="" readonly="true" >
                        </div>
                        <div id ="dataTaglia" class=" col-md-2">
                            <label class="testoParagraph control-label">Data tagliando</label>
                            <input type="text"  name="dataTagliando" class="form-control" data-provide="datepicker" data-date-language="it"
                                   data-date-format="dd-mm-yyyy" data-date-today-btn="false" data-date-autoclose="true" data-date-end-date="0d" data-date-start-date="-1d"
                                   data-date-today-highlight="false" data-date-toggle-active="true"
                                   data-date-disable-touch-keyboard="true" data-date-enable-on-readonly="false" id="dataTagliando" value="" required="true" >
                        </div>

                        <div id ="dataDecorr" class=" col-md-2">
                            <label class="testoParagraph control-label">Data decorrenza</label>
                            <input type="text" align="center" name="dataDecorrenza" class="form-control" id="dataDecorrenza" value="" readonly="true" >
                        </div>
                        <div id ="dataScade" class=" col-md-2">
                            <label class="testoParagraph control-label">Scadenza copertura</label>
                            <input type="text" align="center" name="dataScadenza" class="form-control" id="dataScadenza"  readonly="true" >
                        </div>
                    </div>


                    <div class="col-md-12">
                        <br>
                        <div class="col-md-2">
                            <g:link page="1" controller="menu" action="rcaGratuita" class="btn btn-yellow">Annulla</g:link>
                        </div>
                        <div class="col-md-2 col-md-push-9">
                            <button type="submit" id="salva" disabled="true" class="btn btn-yellow">Salva</button>
                        </div>
                        %{--<button type="submit" name="submit" id="submit" class="btn btn-primary"  onclick="verifica();">Salva</button>--}%
                        %{--<g:submitButton controller="polizze" class="btn btn-warning" value="Salva" action="creaPolizzaExtension" id="salva" name="salva" >Salva</g:submitButton>--}%

                    </div>
                </f:form>
            </div>
        </div>
    </div>
    <div class="modal" id="modalCondizioniPolizza">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background: #f7d900">
                    <h3 class="article-title text-center"><fa:icon name="info-circle"/> Consenso condizioni polizza</h3>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <a href="${createLink(action: "generaPDF")}" class="btn btn-yellow" target="_blank" title="Download"><span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span> Scarica</a>
                    <button type="button" onclick="return acettoValidate();" id="bottonmodal" class="btn btn-yellow" data-dismiss="modal">Accetto</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <div class="modal" id="modalEsclusioniPolizza">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background: #f7d900">
                    <h3 class="article-title text-center"><fa:icon name="warning"/> Esclusioni copertura veicoli</h3>
                </div>
                <div class="modal-body">
                    <div class="ibox">
                        <div class="ibox-content">
                            <p class="testoParagraph">Sono esclusi dalla copertura i veicoli che:</p>
                            <ul>
                                <li class="testoParagraph">siano ciclomotori e motoveicoli;</li>
                                <li class="testoParagraph">siano autoveicoli diversi dalle autovetture definite dal Codice della Strada, quali ad esempio autobus, autocarri trattori stradali, autotreni, autoarticolati, autosnodati, autocaravan e mezzi d'opera;</li>
                                <li class="testoParagraph">siano autoveicoli da utilizzarsi per servizio di trasporto di cose per conto terzi, autoveicoli per trasporti specifici e/o per uso speciale;</li>
                                <li class="testoParagraph">siano autoveicoli da utilizzarsi per servizio di piazza (taxi o ex taxi), di linea, di ambulanza, di scuola guida, di locazione e/o noleggio (con o senza conducente);</li>
                                <li class="testoParagraph">siano autoveicoli da utilizzarsi a fini sportivi o di competizione e/o che siano stati oggetto di modifiche/trasformazioni tecniche;</li>
                                <li class="testoParagraph">siano autoveicoli ad alimentazione elettrica, GPL, metano, idrogeno e ibrida;</li>
                                <li class="testoParagraph">siano veicoli senza obbligo di patente di guida;</li>
                                <li class="testoParagraph">siano autoveicoli diffusi in Italia in meno di 300 esemplari;</li>
                                <li class="testoParagraph">siano autoveicoli delle seguenti marche: <strong>ALPINE, ASTON-MARTIN, BENTLEY, BUGATTI, BUICK, CADILLAC, CORVETTE, DE TOMASO, DODGE, EXCALIBUR, FERRARI, HUMMER, LAMBORGHINI, LINCOLN, LOTUS, MAYBACH, MASERATI, MERCURY, MVS - VENTURI, PORSCHE, ROLLS-ROYCE, TVR;</strong></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button"  id="bottonmodalEsclusioni" class="btn btn-yellow" onclick="return lanciamodalDataTagliando();" data-dismiss="modal">Confermo</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="modal" id="modalKmallaVendita">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header" style="background: #f7d900">
                    <h3 class="article-title text-center"><fa:icon name="times-circle"/>Attenzione</h3>
                </div>
                <div class="modal-body">
                    <p class="testoParagraph">i Km non possono essere superiori a 120000</p>
                </div>
                <div class="modal-footer">
                    <button type="button" id="bottonkm" class="btn btn-yellow" data-dismiss="modal">Ho capito</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="modal" id="modalDataTagliando">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header" style="background: #f7d900">
                    <h3 class="article-title text-center"><fa:icon name="times-circle"/>Attenzione</h3>
                </div>
                <div class="modal-body">
                    <p class="testoParagraph">L'attivazione della polizza può essere effettuata entro le 24 ore successive alla data del tagliando</p>
                </div>
                <div class="modal-footer">
                    <button type="button" id="bottonDataTagliando" class="btn btn-yellow" data-dismiss="modal">Ho capito</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <div class="modal" id="modalDataImmatr48">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header" style="background: #f7d900">
                    <h3 class="article-title text-center"><fa:icon name="times-circle"/>Attenzione</h3>
                </div>
                <div class="modal-body">
                    <p class="testoParagraph">la data di immatricolazione non può essere inferiore a 4 anni</p>
                </div>
                <div class="modal-footer">
                    <button type="button" id="bottonDataImmatr48" class="btn btn-yellow" data-dismiss="modal">Ho capito</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <div class="modal" id="modalDataImmatr96">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header" style="background: #f7d900">
                    <h3 class="article-title text-center"><fa:icon name="times-circle"/>Attenzione</h3>
                </div>
                <div class="modal-body">
                    <p class="testoParagraph">la data di immatricolazione non può essere superiore a 8 anni</p>
                </div>
                <div class="modal-footer">
                    <button type="button" id="bottonDataImmatr96" class="btn btn-yellow" data-dismiss="modal">Ho capito</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <div class="modal" id="modalTelaio">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header" style="background: #f7d900">
                    <h3 class="article-title text-center"><fa:icon name="times-circle"/>Attenzione</h3>
                </div>
                <div class="modal-body">
                    <p class="testoParagraph">esiste una polizza associata a questo telaio</p>
                </div>
                <div class="modal-footer">
                    <button type="button" id="bottonTelaio" class="btn btn-yellow" data-dismiss="modal">Ho capito</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
</div>
<script type="text/javascript">
    $( document ).ready(function() {
        /***pop-up con le esclusioni polizza***/
        $("#modalEsclusioniPolizza").modal({
            backdrop: 'static',
            keyboard: false
        });

    });
    /***funzione formatazzione data ***/
    function formatDate(date) {
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();
        return  (day < 10 ? '0' : '') + day + '-' +(month < 10 ? '0' : '') + month + '-' + year;
    }
    /***verifico che i km non siano superiori a 120000 ***/
    $("#kmAllaVendita").on("change", function(event) {
        var valore= $("#kmAllaVendita").val();
        if (valore>120000){
            $("#modalKmallaVendita").modal({
                backdrop: 'static',
                keyboard: false
            });
            $("#salva").prop('disabled', true);
        }else{
            if(verificaDataTagliando()==true){
                $("#salva").prop('disabled', false);
            }
        }
    });
    /***verifico che la data tagliando non sia superiore a 24 ore prima o dopo e inoltre setto la data decorrenza e scadenza***/
    $("#dataTagliando").on("changeDate", function(event) {
        var data1 = $("#dataTagliando").val().split('-');
        var dateformat1= new Date(parseInt(data1[2], 10), parseInt(data1[1], 10)-1, parseInt(data1[0], 10));
        dateformat1.setHours(new Date().getHours());
        dateformat1.setMinutes(new Date().getMinutes());
        dateformat1.setSeconds(new Date().getSeconds());
        var dateformat2= new Date(parseInt(data1[2], 10), parseInt(data1[1], 10)-1, parseInt(data1[0], 10)+30);
        dateformat2.setHours(new Date().getHours());
        dateformat2.setMinutes(new Date().getMinutes());
        dateformat2.setSeconds(new Date().getSeconds());
        var diffOre=dayDiff(dateformat1);
        /*** questa modifica va messa nella rca gratuita**/
        /*
         var dataodierna=new Date().getTime();
         if(dataodierna>10){
         dateformat2= new Date(parseInt(data1[2], 10), parseInt(data1[1], 10)-1, parseInt(data1[0], 10)+31);
         alert(dateformat2);
         }*/
        if(verificaKM()==true){
            $("#salva").prop('disabled', false);
        }
        else{
            $("#salva").prop('disabled', true);
        }
        $('#dataDecorrenza').val(isNaN(dateformat2.getFullYear()) ? '' : formatDate(dateformat2));

        var data2 = $("#dataDecorrenza").val().split('-');
        var dateformat3 = new Date(parseInt(data2[2], 10), parseInt(data2[1], 10)-1, parseInt(data2[0], 10));

        dateformat3.setFullYear(dateformat3.getFullYear() + 1);
        dateformat3.setHours(new Date().getHours());
        dateformat3.setMinutes(new Date().getMinutes());
        dateformat3.setSeconds(new Date().getSeconds());
        $('#dataScadenza').val(isNaN(dateformat3) ? '' : formatDate(dateformat3));
        /*
         var data2 = $("#dataDecorrenza").val().split('/');
         var dateformat2 = new Date(parseInt(data2[2], 10), parseInt(data2[1], 10)-1, parseInt(data2[0], 10));
         var messi= monthDiff(dateformat1,dateformat2)
         if (messi >3){
         alert('la data non può essere superiore a 3 mesi')
         //$("#dataTagliando").setAttributeNode("data-toggle","tooltip");
         //$("#dataTagliando").attr("title","La data no puo\' essere superiore a 8 anni");
         // $("#dataTagliando").append("data-toggle='tooltip' title='La data no puo' essere superiore a 8 anni' ");
         }
         */
    });
    /***funzione per calcolo diferenza messi***/
    function monthDiff(d1, d2) {
        if (d1>d2){
            var temp=d2;
            d2=d1;
            d1= temp;
        }
        var diffmessi=d2.getMonth()-d1.getMonth();
        var diffanni=d2.getYear()-d1.getYear();
        return diffmessi+ (diffanni*12);

        /*var months;
         months = (d2.getFullYear() - d1.getFullYear()) * 12;
         months -= d1.getMonth() + 1;
         months += d2.getMonth();
         return months <= 0 ? 0 : months;*/
    }
    /***funzione per calcolo diferenza giorni***/
    function dayDiff(d1) {
        var daytime=new Date();
        var diff = daytime-d1;
        var days  = diff/1000/60/60/24;
        /* if (d1<d2){
         var temp=d2;
         d2=d1;
         d1= temp;
         }
         var diffmessi=d2.getMonth()-d1.getMonth();
         var diffanni=d2.getYear()-d1.getYear();
         return diffmessi+ (diffanni*12);*/
        return days;

    }
    /***funzione che verifica lo stato del campo KM***/
    function verificaKM() {
        var valore= $("#kmAllaVendita").val();
        if (valore>120000){ return false; }
        else if ( valore<120000 && valore !=''){ return true; }
    }
    /***funzione che verifica lo stato del campo data tagliando***/
    function verificaDataTagliando(){
        var data1 = $("#dataTagliando").val();
        //var dateformat1= new Date(parseInt(data1[2], 10), parseInt(data1[1], 10)-1, parseInt(data1[0], 10));
        //var diffGiorno=dayDiff(dateformat1);
        if (data1 !=''){ return true;    }
        else{return false;  }
    }
    /***funzione che lancia modal con nota sulla data tagliando***/
    function lanciamodalDataTagliando(){
        $("#modalDataTagliando").modal({
            backdrop: 'static',
            keyboard: false
        });
    }
    /***funzione che lancia modal sulla data immatricolazione minore 4 anni***/
    function lanciamodalDataImmatr48(){
        $("#modalDataImmatr48").modal({
            backdrop: 'static',
            keyboard: false
        });
    }
    /***funzione che lancia modal sulla data immatricolazione maggiore 8 anni***/
    function lanciamodalDataImmatr96(){
        $("#modalDataImmatr96").modal({
            backdrop: 'static',
            keyboard: false
        });
    }
    /***typeahead per la ricerca telaio***/
    $('#telai .typeahead').typeahead({
                hint: true,
                highlight: true,
                minLength: 4,
                classNames: {
                    menu: 'popover',
                    dataset: 'popover-content'
                }
            }, {
                name: "elencotelai",
                display: "telaio",
                limit: ${limite},
                source: new Bloodhound({
                    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    remote: {
                        url: '${createLink(action: "getTelai", params: [elencoTelaio: "notelaio"])}',
                        wildcard: 'notelaio'
                    }
                }),
                templates:{
                    header: Handlebars.compile( '<p class="testoParagraph">Telai disponibili</p>'),
                    suggestion: Handlebars.compile('<div>{{#if telaio}} {{telaio}} {{else}} non ci sono telai disponibili {{/if}}</div>')
                }
            }
    ).on("typeahead:select", function(event, selected) {
        if(selected.id >0){
            if(verificaKM()==true && verificaDataTagliando()==true){
                $("#salva").prop('disabled', false);
            }/***verifico se la data immatricolazione e inferiore a 4 anni e superiore a 8 anni**/
            if(selected.dataImmatricolazione){
                var dataImma=selected.dataImmatricolazione.split('-');
                var dateformat1= new Date(parseInt(dataImma[0], 10), parseInt(dataImma[1], 10)-1, parseInt(dataImma[2], 10));
                var dataCorrente= (new Date());
                var messiDiff=monthDiff(dateformat1,dataCorrente)
                if(messiDiff<48){
                    //lanciamodalDataImmatr48();
                }
                if(messiDiff>96){
                    //lanciamodalDataImmatr96();
                }
            }
            if(selected.telaio){
                $.post("${createLink(controller: "polizze", action: 'getPolizza')}",{ telaio: selected.telaio,  _csrf: "${request._csrf.token}"}, function(response) {
                    var risposta=response.risposta;
                    if(risposta==false){
                        /**questo verra implementato quando avremmo i telai aggiornati
                         $("#modalTelaio").modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                         $("#salva").prop('disabled', true);**/
                    }
                }, "json");
            }
        }
        $("#id-telaio").val(selected.id);
        $("#targa").val(selected.targa);
        $("#modello").val(selected.modello);
        $("#cilindrata").val(selected.cilindrata);
        $("#trasmissione").val(selected.trasmissione);
        $("#versione").val(selected.versione);
        var dataImmat=selected.dataImmatricolazione.split('-');
        var dataImmatricolazione= new Date(parseInt(dataImmat[0], 10), parseInt(dataImmat[1], 10)-1, parseInt(dataImmat[2], 10)+1);
        $("#dataImmatri").val(formatDate(dataImmatricolazione));
        $.post("${createLink(action: 'datiTelai')}",{ id: selected.id, _csrf: "${request._csrf.token}"}, function(response) {});
    }).on("blur", function(event) {
        var input = $(this);
        if(input.val() == "") {
            input.typeahead("val", "");
            $("#id-telaio").val(null);
            $.post("${createLink(action: 'datiTelai')}",{ id: null, _csrf: "${request._csrf.token}"}, function(response) {});
        }
    });
</script>

</body>
</html>