<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
</head>

<body>
<p align="CENTER">
    <strong>FASCICOLO INFORMATIVO</strong>
</p>
<p align="CENTER">
    <strong>ALLEGATO 1</strong>
</p>
<p align="CENTER">
    “<strong>GARANZIA MOTORE TRASMISSIONE CAMBIO</strong>
</p>
<p align="CENTER">
    (<strong>Contratto di assicurazione perdite pecuniarie</strong>)
</p>
<p align="CENTER">
    <strong>POLIZZA COLLETTIVA N. </strong>
    <strong>_______________</strong>
</p>
<p>
    <strong>Il presente Fascicolo Informativo contiene i seguenti documenti: </strong>
</p>
<ol>
    <li>
        <p>
            <strong>Nota Informativa;</strong>
        </p>
    </li>
</ol>
<ol start="2">
    <li>
        <p>
            <strong>Glossario; </strong>
        </p>
    </li>
</ol>
<ol start="3">
    <li>
        <p>
            <strong>Condizioni di Assicurazione </strong>
        </p>
    </li>
</ol>
<p align="JUSTIFY">
    <strong>AVVERTENZA: prima della sottoscrizione leggere attentamente la Nota Informativa”.</strong>
</p>
<p align="CENTER">
    <strong>NOTA INFORMATIVA</strong>
</p>
<p align="CENTER">
    <strong>DEL CONTRATTO DI ASSICURAZIONE</strong>
</p>
<p align="CENTER">
    <strong>POLIZZA N. </strong>
    <strong>______________</strong>
</p>
<p align="JUSTIFY">
    <em>
        La presente Nota Informativa è redatta secondo lo schema predisposto dall’IVASS, ma il suo contenuto non è soggetto alla preventiva approvazione
        dell’IVASS. Il contraente deve prendere visione delle condizioni di assicurazione prima della sottoscrizione della polizza.
    </em>
</p>
<p align="JUSTIFY">
    <strong>A. INFORMAZIONI SULL’IMPRESA DI ASSICURAZIONE</strong>
</p>
<p align="JUSTIFY">
    <u><strong>1. Informazioni Generali</strong></u>
</p>
<p align="JUSTIFY">
    Axa France Iard S.A. con sede legale presso 313 Terrasses de l'Arche 92727 – Nanterre Cedex (France), e iscritta al Registro delle imprese __________ n.
    ___________________ operante in Italia in libera prestazione di servizi è iscritta nell’Elenco II annesso all’Albo delle imprese di assicurazione e
    riassicurazione dell’IVASS con il numero I.00600. Axa France Iard S.A. è autorizzata in Francia con provvedimento _________________, e vigilata dalla
    Autorité de Contrôle Prudentiel et de Resolution- Secteur Assurances, 61 rue Taitbout 75009 Paris, Sito internet:    <u><a class="testoParagraph" href="http://www.societe.com/societe/axa-france-iard">www.societe.com/societe/axa-france-iard</a></u> Telefono:________________ Fax:
__________________ Indirizzo e-mail:_____________________
</p>
<p align="JUSTIFY">
    <u><strong>2. Informazioni sulla situazione patrimoniale dell’impresa</strong></u>
</p>
<p align="JUSTIFY">
    In base ai dati riferiti all'ultimo bilancio approvato al 31.12.2014, il patrimonio netto è di _________________ di cui il capitale sociale ammonta a
    _____________________. Le riserve patrimoniali ammontano a _________________. L'indice di solvibilità è pari al ____%. Si precisa che tale indice
    rappresenta il rapporto tra l'ammontare del margine di solvibilità disponibile e l'ammontare del margine di solvibilità richiesto dalla normativa vigente.
</p>
<p align="JUSTIFY">
    Si rinvia al sito internet della Compagnia di Assicurazione per la consultazione di eventuali aggiornamenti del presente Fascicolo Informativo non
    derivanti da innovazioni normative.
</p>
<p align="JUSTIFY">
    <strong>B. INFORMAZIONI SUL CONTRATTO</strong>
</p>
<p align="JUSTIFY">
    Il contratto non prevede tacito rinnovo.
</p>
<p align="JUSTIFY">
    <u><strong>3. Coperture assicurative offerte – Limitazioni ed Esclusioni</strong></u>
</p>
<p align="JUSTIFY">
    Oggetto della garanzia “Guasti per Autoveicoli Usati Opel” è una copertura assicurativa per gli Autoveicoli con marchio Opel.
</p>
<p align="JUSTIFY">
    La Compagnia di Assicurazione si impegna a rimborsare all’Assicurato, a titolo di indennizzo, i costi, IVA esclusa, sostenuti dall’Assicurato per le parti
    di ricambio e la manodopera necessari alla riparazione del Veicolo, qualora durante il periodo di validità della presente copertura assicurativa si
    verifichi un Guasto ai componenti del Veicolo oggetto di copertura.
</p>
<p align="JUSTIFY">
    Si rinvia agli art. 1.2 – Prestazioni oggetto della Garanzia e Massimali delle Condizioni di Assicurazione per gli aspetti di dettaglio.
</p>
<p align="JUSTIFY">
    <strong>AVVERTENZA</strong>
    : sono previsti casi di esclusioni e limitazioni che possono dare luogo al mancato pagamento dell’Indennizzo. In particolare, sono previste:
</p>
<p align="JUSTIFY">
    Si rinvia per gli aspetti di dettaglio agli art. 2 e 8 delle Condizioni di Assicurazione;
</p>
<p align="JUSTIFY">
    <strong>AVVERTENZA</strong>
    : sono previsti i seguenti massimali:
</p>
<p align="JUSTIFY">
    1) un importo massimo pagabile a titolo di Indennizzo pari a:
</p>
<p align="JUSTIFY">
    Euro 2.000,00 IVA esclusa per singolo Sinistro per anno;
</p>
<p align="JUSTIFY">
    2) in ogni caso l’importo massimo pagabile a titolo di Indennizzo in relazione a ciascun Sinistro non potrà mai essere superiore al Valore dell’Autoveicolo
    riportato su EuroTax Blu, al momento del Sinistro.
</p>
<p align="JUSTIFY">
    I massimali indicati trovano applicazione sia con riferimento a una singola riparazione sia con riferimento a più riparazioni tra loro aggregate e che
    siano effettuate nel periodo di durata della copertura assicurativa.
</p>
<p align="JUSTIFY">
    Si rinvia all’art. 1.2 – Prestazioni oggetto della Garanzia e Massimali delle Condizioni di Assicurazione per gli aspetti di dettaglio.
</p>
<p align="JUSTIFY">
    <strong>Esempio </strong>
</p>
<p align="JUSTIFY">
    1. Nel momento in cui si verifica il Sinistro e viene richiesta la riparazione, il Valore dell’Autoveicolo è di Euro 5.000,00 e il costo per le parti del
    ricambio e per la manodopera, IVA inclusa, è di Euro 2.000,00, la Compagnia di Assicurazione rimborsa totalmente i costi sostenuti dall’Assicurato, IVA
    esclusa.
</p>
<p align="JUSTIFY">
    2. Dopo sei mesi dalla riparazione di cui al punto 1, il Valore di Mercato del Veicolo è di Euro 4.000,00, Si verifica un altro Sinistro e viene richiesta
    un’altra riparazione il cui costo è di Euro 1.500,00. La Compagnia di Assicurazione non provvederà al rimborso in quanto il massimale previsto è stato già
    eroso dalla precedente riparazione.
</p>
<p align="JUSTIFY">
    <strong>AVVERTENZA</strong>
    : sono previsti dei casi di esclusione di operatività della copertura assicurativa.
</p>
<p align="JUSTIFY">
    Si rinvia agli articoli 2 e 8 delle Condizioni di Assicurazione
</p>
<p align="JUSTIFY">
    <u><strong>4. Dichiarazioni dell’Assicurato in ordine alle circostanze del rischio</strong></u>
</p>
<p align="JUSTIFY">
    AVVERTENZA: ai sensi degli artt. 1892 e 1893 c.c. eventuali dichiarazioni false o reticenze sulle circostanze che possono influire sulla valutazione del
    rischio da parte della Compagnia di Assicurazione, rese in sede di conclusione del contratto, possono comportare effetti sulla prestazione.
</p>
<p align="JUSTIFY">
    Si rinvia all’art. 1 Dichiarazioni concernenti il rischio della Sezione II delle Condizioni di Assicurazione per gli aspetti di dettaglio.
</p>
<p align="JUSTIFY">
    <u><strong>5. Aggravamento e diminuzione del rischio</strong></u>
</p>
<p align="JUSTIFY">
    L’Assicurato ha l’obbligo di dare immediato avviso alla Compagnia di Assicuratore dei mutamenti che aggravano il rischio in modo tale che, se il nuovo
    stato di cose fosse esistito e fosse stato conosciuto dalla Compagnia di Assicurazione al momento della conclusione del contratto, la Compagnia di
    Assicurazione non avrebbe consentito l’assicurazione o l’avrebbe consentita per un premio più elevato. Gli aggravamenti del rischio non comunicati
    dall’Assicurato alla Compagnia di Assicurazione possono comportare la perdita totale o parziale del diritto all’Indennizzo nonché la cessazione di
    efficacia della copertura assicurativa ai sensi dell’art. 1898 c.c.
</p>
<p align="JUSTIFY">
    <u><strong>6.</strong></u>
    <u> </u>
    <u><strong>Premio</strong></u>
</p>
<p align="JUSTIFY">
    Il premio assicurativo è dovuto in via anticipata e in un’unica soluzione, per il periodo di copertura assicurativa prescelto per ogni Autoveicolo di
    marchio Opel che ha effettuato il tagliando di manutenzione presso le concessionarie e/o officine autorizzate Opel per il quale l’Assicurato chieda
    l’attivazione della copertura attraverso il sistema informatico.
</p>
<p align="JUSTIFY">
    Le modalità di pagamento del premio previste sono il bonifico bancario.
</p>
<p align="JUSTIFY">
    Si rinvia all’articolo 7 Premio delle Condizioni di Assicurazione
</p>
<p align="JUSTIFY">
    <u><strong>7. Rivalse</strong></u>
</p>
<p align="JUSTIFY">
    Il presente contratto non prevede il diritto di rivalsa.
</p>
<p align="JUSTIFY">
    <u><strong>8. Diritto di Recesso</strong></u>
</p>
<p align="JUSTIFY">
    Il Contraente potrà recedere da ogni singola attivazione entro 30 giorni dalla data di decorrenza della copertura mediante comunicazione alla Compagnia. In
    tal caso il premio gli sarà interamente rifuso.
</p>
<p align="JUSTIFY">
    Qualora il recesso avvenga oltre il termine di 30 giorni dalla decorrenza dell’Assicurazione, il Contraente non avrà diritto alla restituzione del premio.
</p>
<p align="JUSTIFY">
    <u><strong>9. Prescrizione e decadenza dei diritti derivanti dal contratto</strong></u>
</p>
<p align="JUSTIFY">
    I diritti derivanti dal contratto di assicurazione si prescrivono in due anni dalla data in cui si è verificato il fatto su cui il diritto si fonda.
</p>
<p align="JUSTIFY">
    <u><strong>10. Legge applicabile</strong></u>
</p>
<p align="JUSTIFY">
    Il contratto di assicurazione è disciplinato dalla legge italiana.
</p>
<p align="JUSTIFY">
    <u><strong>11. Regime fiscale</strong></u>
</p>
<p align="JUSTIFY">
    E’ a carico del Contraente ed è pari al 21,25% del premio imponibile.
</p>
<p align="JUSTIFY">
    <strong>C. INFORMAZIONI SULLE PROCEDURE DI LIQUIDAZIONE E SUI RECLAMI</strong>
</p>
<p align="JUSTIFY">
    <u><strong>12. Sinistri – Liquidazione dell’indennizzo</strong></u>
</p>
<p align="JUSTIFY">
    <strong>AVVERTENZA</strong>
    : Sono previste specifiche modalità e termini per la denuncia del Sinistro. Si segnala altresì che i costi per l’invio della documentazione richiesta ai
    fini della liquidazione dell’Indennizzo sono a carico dell’Assicurato.
</p>
<p align="JUSTIFY">
    Si rinvia al l’art. 4 Obblighi dell’assicurato in caso di sinistro delle Condizioni di Assicurazione per gli aspetti di dettaglio.
</p>
<p align="JUSTIFY">
    <strong>13 Reclami</strong>
</p>
<p align="JUSTIFY">
    Qualunque reclamo inerente al presente contratto di assicurazione e alla gestione di un Sinistro deve essere presentato per iscritto alla Compagnia di
    Assicurazione al seguente indirizzo:
</p>
<p align="CENTER">
    <strong>Axa France Iard S.A. </strong>
</p>
<p align="CENTER">
    <strong>sede in 313 Terrasses de l’Arche</strong>
</p>
<p align="CENTER">
    <strong>92727 Nanterre (Francia)</strong>
</p>
<p align="CENTER">
    Tel. _____________
</p>
<p align="CENTER">
    Fax: _______________
</p>
<p align="CENTER">
    <u><a class="testoParagraph" href="mailto:__________________@axa.fr">__________________@axa.fr</a></u>
</p>
<p align="JUSTIFY">
    Qualora l'esponente non si ritenga soddisfatto dell'esito del reclamo o in caso di assenza di riscontro nel termine massimo di 45 (quarantacinque) giorni,
    potrà rivolgersi all'IVASS, Servizio Tutela degli Utenti, Via del Quirinale, 21, 00187 Roma. Il reclamo dovrà essere inviato all’IVASS a mezzo posta oppure
    trasmesso al numero di fax: 06.42133745 oppure 06.42133353 e dovrà contenere copia del reclamo già inoltrato alla Compagnia di Assicurazione e il relativo
    riscontro.
</p>
<p align="JUSTIFY">
    L'Assicurato può reperire dettagliate informazioni sulle modalità di presentazione dei reclami alla Compagnia di Assicurazione e all’IVASS sul sito
    internet www.ivass.it, sezione “Per il Consumatore – Come presentare un reclamo”, con possibilità di accedere anche ai facsimile di reclamo.
</p>
<p align="JUSTIFY">
    Si ricorda che permane la competenza esclusiva dell’Autorità Giudiziaria, oltre alla facoltà di ricorrere ai sistemi conciliativi esistenti.
</p>
<p align="JUSTIFY">
    In particolare, si informa che, ai sensi dell’art. 5 del D.Lgs. 4 marzo 2010, n. 28, così come modificato dall’art. 84, comma 1, del D.L. 21 giugno 2013,
    n. 69, in caso di controversia inerente a o derivante dal presente contratto, la parte che intenda promuovere una causa dovrà previamente esperire il
    procedimento di mediazione obbligatorio, deferendo la controversia a uno degli Organismi di mediazione abilitati che abbia sede nel luogo del giudice
    territorialmente competente per la controversia.
</p>
<p align="JUSTIFY">
    Per la soluzione delle liti transfrontaliere, è possibile presentare il reclamo direttamente al sistema estero competente, ossia quello del Paese in cui ha
    sede la Compagnia di Assicurazione che ha concluso il contratto (rintracciabile accedendo al sito: htpp://www.ec.europa.eu/fin-net). Per la Francia di
    tratta del Le Médiateur de la Fédération Française des Sociétés d'Assurances (FFSA), BP 290, 75425 PARIS CEDEX 09, o all’IVASS che provvede direttamente
    all’inoltro a detto sistema, dandone notizia al reclamante.
</p>
<p align="JUSTIFY">
    E’ possibile presentare un reclamo direttamente all’autorità di vigilanza del Paese di origine competente, scrivendo a Le Médiateur de la Fédération
    Française des Sociétés d'Assurances (FFSA), BP 290, 75425 PARIS CEDEX 09.
</p>
<p align="JUSTIFY">
    Axa France Iard S.A. è direttamente responsabile della veridicità e della completezza dei dati e delle notizie contenuti nella presente Nota Informativa.
</p>
<p align="JUSTIFY">
    Il Legale Rappresentante
</p>
<p align="JUSTIFY">
    ________________
</p>
<p align="CENTER">
    <strong>GLOSSARIO</strong>
</p>
<p align="JUSTIFY">
    <u><strong>Assicurato / Beneficiario</strong></u>
    <strong>:</strong>
    il concessionario e/o l’officina convenzionata alla rete Opel Italia individuata dal Contraente che ha erogato un tagliando di manutenzione al veicolo
    usato di marca Opel e contestualmente ha abbinato la garanzia guasti al veicolo oggetto del tagliando di manutenzione.
</p>
<p align="JUSTIFY">
    <u><strong>Autorizzazione:</strong></u>
    L’approvazione data dalla Compagnia, anche per il tramite della Centrale Operativa, all’Assicurato per riparare l’Autoveicolo in conformità con quanto
    previsto dalle condizioni di assicurazione.
</p>
<p align="JUSTIFY">
    <u><strong>Autoveicolo:</strong></u>
    il veicolo usato di marca Opel, immatricolato in Italia con numero di targa italiano, che abbia più di 4 (quattro) anni e meno di 8 (otto) dalla data di
    prima immatricolazione e non abbia superato i 120.000 Km al momento dell’effettuazione del tagliando di manutenzione, il cui peso complessivo utilizzato a
    pieno carico non ecceda i 35 q.li, che è configurato di trasporto persone e beni e che non ricade nelle esclusioni previste dall’art. 2 delle condizioni di
    assicurazione.
</p>
<p align="JUSTIFY">
    <u><strong>Compagnia di Assicurazione, Compagnia, Assicuratore</strong></u>
    <strong>:</strong>
    la compagnia AXA France Iard S.A., che assume l’obbligo di coprire i rischi assicurati.
</p>
<p align="JUSTIFY">
    <u><strong>Contraente</strong></u>
    <strong>:</strong>
    la General Motor Financial Italia S.p.a. (per brevità “GMFI”) che sottoscrive la presente Polizza di Assicurazione, in qualità di contraente di polizza
    collettiva insieme con la Compagnia di Assicurazione, assumendone i diritti e gli obblighi da lei previsti, nonché quelli derivanti dagli artt. 1882 ss.
    Codice Civile.
</p>
<p align="JUSTIFY">
    <u><strong>Data di Decorrenza:</strong></u>
    Le ore 00.00 del trentesimo giorno successivo all’effettuazione del tagliando di manutenzione presso il concessionario e/o officina autorizzata Opel.
</p>
<p align="JUSTIFY">
    <u><strong>Gestore dei Sinistri o Gestore o Centrale Operativa</strong></u>
    <strong>:</strong>
    Inter Partner Assistance S.A., con sede in Corso Como, 17 – 20154 – Milano tel. _____________, fax______________ e-mail ____________________.
</p>
<p align="JUSTIFY">
    <u><strong>Guasto</strong></u>
    <strong>: </strong>
    il guasto di uno o più componenti dell’Autoveicolo oggetto di copertura dovuto ad un difetto intrinseco della componente coperta e non riferibile a cause
    esterne e provocato da causa diversa dall'usura, dal deterioramento o dalla negligenza nell'utilizzo dell'autoveicolo, che ne determini la rottura ed il
    conseguente mancato funzionamento per cui necessita la riparazione o sostituzione. La rumorosità, le vibrazioni o i fruscii di uno o più componenti oggetto
    di copertura non sono considerati guasto meccanico.
</p>
<p align="JUSTIFY">
    <u><strong>Indennizzo</strong></u>
    <strong>:</strong>
    la somma dovuta dalla Compagnia d’Assicurazione all’Assicurato in caso di Sinistro.
</p>
<p align="JUSTIFY">
    <u><strong>Limite dell’Assicurazione</strong></u>
    <strong>:</strong>
    il limite massimo d’Indennizzo dovuto dalla Compagnia all’Assicurato per anno e per ciascun Sinistro.
</p>
<p align="JUSTIFY">
    <u><strong>Parti:</strong></u>
    GMFI e AXA France IARD che assumono gli obblighi ed i diritti discendenti dal presente Contratto, e quelli derivanti dagli artt. 1882 ss. Codice Civile.
</p>
<p align="JUSTIFY">
    <u><strong>Polizza di Assicurazione, Polizza, Contratto, Contratto di Assicurazione</strong></u>
    <strong>:</strong>
    il presente contratto, composto dalle Condizioni Generali e dalle Condizioni Speciali di Assicurazione.
</p>
<p align="JUSTIFY">
    <u><strong>Premio</strong></u>
    <strong>:</strong>
    l’importo, comprensivo delle imposte di assicurazione sul premio, dovuto alla Compagnia di Assicurazione per la copertura prestata.
</p>
<p align="JUSTIFY">
    <u><strong>Sinistro:</strong></u>
    Il verificarsi dell’evento dannoso per il quale è prestata la copertura assicurativa.
</p>
<p align="JUSTIFY">
    <u><strong>Territorio</strong></u>
    Italia, Città del Vaticano, Repubblica di San Marino e Unione Europea limitatamente ai paesi presenti nella carta verde di circolazione.
</p>
<p align="JUSTIFY">
    <u><strong>Usura</strong></u>
    <strong>:</strong>
    l’usura delle parti, componenti e materiali che richiedono la manutenzione, la sostituzione o la riparazione nel corso dell’uso dell’Autoveicolo.
</p>
<p align="JUSTIFY">
    <u><strong>Valore dell’Autoveicolo</strong></u>
    : il valore commerciale dell’Autoveicolo al momento del Sinistro, riportato in Eurotax Blu.
</p>
<p align="CENTER">
    <strong>SEZIONE I</strong>
</p>
<p align="CENTER">
    <strong>CONDIZIONI SPECIALI DELLA POLIZZA DI ASSICURAZIONE N° </strong>
    <strong>__________</strong>
</p>
<p align="CENTER">
    “<strong>Garanzia Guasti Autoveicoli Usati Opel”</strong>
</p>
<p align="JUSTIFY">
    <strong>Art. 1 - OGGETTO E LIMITI DELLE COPERTURE </strong>
</p>
<p align="JUSTIFY">
    La Compagnia, previo pagamento del premio di Polizza, si impegna al rimborso all’Assicurato, a titolo di indennizzo, dei costi, IVA esclusa, dei ricambi e
    della manodopera necessari alla riparazione dell’Autoveicolo, direttamente o indirettamente effettuata a seguito del verificarsi di un Guasto, in ragione
    della presente garanzia per gli Autoveicoli usati appartenenti al marchio Opel.
</p>
<p align="JUSTIFY">
    L’indennizzo è limitato ai Guasti delle componenti elencate all’art. 1.3 e ai Massimali di seguito indicati:
</p>
<p align="JUSTIFY">
    <strong>1.1 - Autoveicoli assicurabili</strong>
</p>
<p align="JUSTIFY">
    La copertura può essere concessa solo in caso di Autoveicolo di marca Opel, immatricolato in Italia con numero di targa italiano, che abbia più di 4
    (quattro) anni e meno di 8 (otto) dalla data di prima immatricolazione e non abbia superato i 120.000 Km al momento dell’effettuazione del tagliando di
    manutenzione, il cui peso complessivo utilizzato a pieno carico non ecceda i 35 q.li, che è configurato al trasporto di persone e beni e che non ricade
    nelle esclusioni previste dall’art. 2 delle condizioni di assicurazione.
</p>
<p align="JUSTIFY">
    <strong>1.2 - Prestazioni oggetto della Garanzia e Massimali</strong>
</p>
<p align="JUSTIFY">
    La garanzia riconosce, a titolo di indennizzo per un Guasto, il pagamento di un importo pari ai costi sostenuti in relazione all’acquisto di pezzi di
    ricambio ed alla manodopera necessaria per la riparazione del Guasto stesso.
</p>
<p lang="it-IT">
    In particolare, sono rimborsabili in base al presente contratto i seguenti costi:
</p>
<p lang="it-IT" align="JUSTIFY">
    - con riferimento al costo delle componenti sostituite: la Compagnia di Assicurazione rimborsa prezzo di vendita al dettaglio rigenerati Opel con sconto
    del 10%;
</p>
<p align="JUSTIFY">
    - con riferimento al costo della manodopera: la Compagnia di Assicurazione rimborsa € 35,00/ora+IVA.
</p>
<p align="JUSTIFY">
    Il massimale per anno e per Sinistro è di € 2.000,00 IVA esclusa. L’Indennizzo non può mai superare il valore commerciale dell’Autoveicolo riportato in
    Eurotax Blu, al momento in cui viene richiesta la effettuazione della riparazione; tale limitazione troverà applicazione sia con riferimento a una singola
    riparazione e/o sostituzione, sia con riferimento a più riparazioni e/o sostituzioni tra loro aggregate
</p>
<p align="JUSTIFY">
    Ai fini di cui sopra, l’Assicurato dovrà premurarsi che il documento fiscale esponga separatamente i costi relativi ai ricambi ed alla manodopera,
    comprensivi di IVA. Il limite di rimborso massimo per anno e per Sinistro non potrà mai essere superiore al valore commerciale dell’Autoveicolo (Eurotax
    Blu) alla data del Sinistro.
</p>
<p align="JUSTIFY">
    <strong>1.3 – Estensione territoriale</strong>
</p>
<p align="JUSTIFY">
    La garanzia ha effetto unicamente per atti e fatti occorsi sul territorio Italiano, Città del Vaticano, Repubblica di San Marino inclusi e dell’Unione
    Europea limitatamente ai paesi presenti nella carta verde di circolazione.
</p>
<p align="JUSTIFY">
    Si segnala che un Guasto che si verifichi al di fuori del territorio italiano, Città del Vaticano, Repubblica di San Marino, ma nell’ambito di uno dei
    paesi dove la Carta Verde è valida, è oggetto della copertura di cui alla presente Polizza, a condizione che, al momento del Sinistro, l’Autoveicolo non si
    trovasse fuori dall’Italia da più di 90 (novanta) giorni consecutivi.
</p>
<p align="JUSTIFY">
    <strong>1.4 - Componenti coperte dalla garanzia Guasti </strong>
</p>
<p align="JUSTIFY">
    La copertura assicurativa viene prestata unicamente in relazione ai Guasti che si verifichino a carico dei componenti di seguito elencati:
</p>
<ul>
    <li>
        <p>
            <strong>Motore </strong>
            <strong>　</strong>
            <strong> </strong>
        </p>
        <ul>
            <li>
                <p>
                    contralberi o albero di equilibratura e cuscinetti
                </p>
            </li>
            <li>
                <p>
                    alberi a camme, cuscinetto di banco, punterie e punterie idrauliche
                </p>
            </li>
            <li>
                <p>
                    ingranaggi albero a camme
                </p>
            </li>
            <li>
                <p>
                    variatore di fase
                </p>
            </li>
            <li>
                <p>
                    bielle e cuscinetti
                </p>
            </li>
            <li>
                <p>
                    albero motore e cuscinetti di banco
                </p>
            </li>
            <li>
                <p>
                    monoblocco o basamento
                </p>
            </li>
            <li>
                <p>
                    testata e relativa guarnizione
                </p>
            </li>
            <li>
                <p>
                    puleggia albero motore
                </p>
            </li>
            <li>
                <p>
                    coppa olio
                </p>
            </li>
            <li>
                <p>
                    pompa olio
                </p>
            </li>
            <li>
                <p>
                    pistoni e fasce elastiche
                </p>
            </li>
            <li>
                <p>
                    aste delle punterie
                </p>
            </li>
            <li>
                <p>
                    bilancieri, boccole
                </p>
            </li>
            <li>
                <p>
                    kit guarnizioni motore e gommini valvole (da sole escluse dalla copertura)
                </p>
            </li>
            <li>
                <p>
                    comando distribuzione a catena, guide e pattini
                </p>
            </li>
            <li>
                <p>
                    catena di distribuzione e ingranaggi albero a camme
                </p>
            </li>
            <li>
                <p>
                    tenditori della distribuzione
                </p>
            </li>
            <li>
                <p>
                    carter distribuzione
                </p>
            </li>
            <li>
                <p>
                    coperchio valvole
                </p>
            </li>
            <li>
                <p>
                    collettore aspirazione
                </p>
            </li>
            <li>
                <p>
                    valvole, guida valvole
                </p>
            </li>
            <li>
                <p>
                    volano, volano bimassa
                </p>
            </li>
        </ul>
    </li>
</ul>
<ul>
    <li>
        <p>
            <strong>Sistema di raffreddamento del motore </strong>
        </p>
        <ul>
            <li>
                <p>
                    pompa dell’acqua
                </p>
            </li>
            <li>
                <p>
                    ventola e relativo giunto viscostatico
                </p>
            </li>
            <li>
                <p>
                    termostato e tazza termostato
                </p>
            </li>
            <li>
                <p>
                    radiatore di raffreddamento motore, elettroventola e resistenza elettroventola del radiatore
                </p>
            </li>
            <li>
                <p>
                    radiatore olio e sensore livello del liquido di raffreddamento
                </p>
            </li>
            <li>
                <p>
                    radiatore di riscaldamento o scambiatore
                </p>
            </li>
        </ul>
    </li>
</ul>
<ul>
    <li>
        <p>
            <strong>Cambio </strong>
        </p>
        <ul>
            <li>
                <p>
                    Automatico/Manuale/CVT - tutte le parti interne lubrificate del cambio manuale, tutte le parti interne lubrificate del cambio automatico,
                    tutte le parti interne lubrificate del cambio CVT (cambio a variazione continua)
                </p>
            </li>
        </ul>
    </li>
</ul>
<ul>
    <li>
        <p>
            <strong>Dispositivi di trasmissione </strong>
        </p>
        <ul>
            <li>
                <p>
                    albero cardanico, semiasse anteriore destro e sinistro, semiasse posteriore destro e sinistro, giunto omocinetico destro e sinistro
                    (esclusi i danni causati da usura)
                </p>
            </li>
            <li>
                <p>
                    giunto universale e giunto d’accoppiamento
                </p>
            </li>
            <li>
                <p>
                    mozzo ruota.
                </p>
            </li>
        </ul>
    </li>
</ul>
<p align="JUSTIFY">
    <strong>Art. 2 - ESCLUSIONI DELLA GARANZIA</strong>
</p>
<p align="JUSTIFY">
    Sono esclusi dalla presente garanzia i seguenti Autoveicoli:
</p>
<ul>
    <li>
        <p>
            con oltre 8 (otto) anni di vetustà e oltre 120.000 km alla data del tagliando;
        </p>
    </li>
    <li>
        <p>
            adibiti ad uso professionale come: taxi, noleggio con conducente, veicoli autoscuola e scuole di pilotaggio, veicoli a noleggio a breve e lungo
            termine, ambulanze o soccorso pubblico o privato, servizi di polizia pubblica o privata;
        </p>
    </li>
    <li>
        <p align="JUSTIFY">
            ciclomotori e motoveicoli;
        </p>
    </li>
    <li>
        <p align="JUSTIFY">
            autoveicoli diversi dalle autovetture definite dal Codice della Strada, quali ad esempio autobus, autocarri, trattori stradali, autotreni,
            autoarticolati, autosnodati, autocaravan e mezzi d'opera;
        </p>
    </li>
    <li>
        <p>
            autoveicoli utilizzati per il servizio di trasporto di cose per conto terzi, autoveicoli per trasporti specifici e/o spedizioni postali;
        </p>
    </li>
    <li>
        <p align="JUSTIFY">
            autoveicoli utilizzati per competizioni sportive di qualsiasi genere, o che siano stati oggetto di modifiche/trasformazioni tecniche;
        </p>
    </li>
    <li>
        <p align="JUSTIFY">
            gli autoveicoli con un peso a vuoto (tara), più il trasporto/carico - di oltre 3.500 kg o adattati al trasporto di più di 8 persone, incluso il
            conducente;
        </p>
    </li>
    <li>
        <p>
            autoveicoli ibridi a idrogeno o con alimentazione elettrica; o metano
        </p>
    </li>
</ul>
<ul>
    <li>
        <p align="JUSTIFY">
            autoveicoli assistiti dalla garanzia del venditore o del costruttore;
        </p>
    </li>
</ul>
<ul>
    <li>
        <p align="JUSTIFY">
            tutte le parti non assemblate originalmente dalla casa costruttrice o dal concessionario e/o officina autorizzata Opel prima dell’acquisto del
            veicolo da parte del cliente, anche se menzionate dall’art. 1.3 che precede.
        </p>
    </li>
    <li>
        <p align="JUSTIFY">
            tutte le parti non espressamente previste dall’art. 1.3 che precede.
        </p>
    </li>
</ul>
<p align="JUSTIFY">
    La garanzia inoltre non copre i seguenti casi:
</p>
<ul>
    <li>
        <p>
            autoveicoli che al momento del guasto abbiano una percorrenza superiore di 50.000 Km rispetto ai chilometri registrati alla data di effettuazione
            del tagliando di manutenzione;
        </p>
    </li>
    <li>
        <p align="JUSTIFY">
            derivanti da dolo, colpa grave, negligenza o uso improprio;
        </p>
    </li>
    <li>
        <p align="JUSTIFY">
            derivanti da un uso del veicolo non previsto dalle caratteristiche tecniche e dai limiti di portata dello stesso;
        </p>
    </li>
    <li>
        <p align="JUSTIFY">
            imputabili ad usura o logoramento di quelle parti, componenti o materiali naturalmente soggetti ad un progressivo consumo o deterioramento e per le
            quali sono indispensabili periodici interventi di manutenzione, rinnovo o ripristino per mantenerne il normale stato di efficienza e funzionamento;
        </p>
    </li>
    <li>
        <p align="JUSTIFY">
            causati da interventi del riparatore di cui lo stesso deve rispondere;
        </p>
    </li>
    <li>
        <p align="JUSTIFY">
            a componenti oggetto di garanzia, quando causati da componenti non oggetto di garanzia (guasti indiretti); viceversa i componenti esclusi rimangono
            sempre esclusi;
        </p>
    </li>
    <li>
        <p align="JUSTIFY">
            derivanti da modifiche od elaborazioni apportate all’autoveicolo rispetto alle condizioni originarie, anche se apportate da elaboratori ufficiali
            del costruttore;
        </p>
    </li>
    <li>
        <p align="JUSTIFY">
            derivanti da difetti di costruzione o di assemblaggio di componenti del veicolo, pubblicamente noti come difetti di serie;
        </p>
    </li>
    <li>
        <p align="JUSTIFY">
            derivanti da una non corretta o inadeguata riparazione, ovvero da riparazioni effettuate con utilizzo di ricambi e materiali non conformi alle
            specifiche della casa costruttrice;
        </p>
    </li>
    <li>
        <p align="JUSTIFY">
            causati da inosservanza delle norme di manutenzione previste dalla casa costruttrice;
        </p>
    </li>
    <li>
        <p align="JUSTIFY">
            derivati da impurità o errato rifornimento del carburante o lubrificante o refrigeranti rispetto a quelli specificatamente prescritti per il tipo
            di Autoveicolo;
        </p>
    </li>
    <li>
        <p align="JUSTIFY">
            verificatisi in conseguenza di riparazioni in concessionarie e/o officine non autorizzate Opel;
        </p>
    </li>
    <li>
        <p align="JUSTIFY">
            per interventi relativi a regolazioni o messe a punto, salvo che l’intervento si renda necessario a seguito di guasto coperto dall’Assicurazione;
        </p>
    </li>
    <li>
        <p align="JUSTIFY">
            conseguenza di incendio, urto, collisione, scoppio, gelo ed in genere di tutti gli agenti atmosferici, furto, danneggiamento ed atti dolosi in
            genere, anche se conseguenti a Guasto coperto dalla Polizza;
        </p>
    </li>
    <li>
        <p align="JUSTIFY">
            in caso di manomissione, alterazione del contachilometri o di qualsiasi altra strumentazione o componente dell’Autoveicolo.
        </p>
    </li>
</ul>
<p align="JUSTIFY">
    Non è inoltre rimborsabile e, quindi, non è oggetto di copertura, qualsiasi altro costo (e/o perdita), diretto o indiretto, consequenziale o meno, in cui
    incorra l’Assicurato.
</p>
<p align="JUSTIFY">
    <strong>Art. 3 - DECORRENZA E DURATA DELLA GARANZIA</strong>
</p>
<p align="JUSTIFY">
    <strong>3.1</strong>
    La garanzia decorre ed ha efficacia dalle ore 00.00 del trentesimo giorno successivo alla data del tagliando di manutenzione dell’Autoveicolo. La garanzia
    avrà una durata pari a 12 mesi dalla suddetta Data di Decorrenza.
</p>
<p align="JUSTIFY">
    <strong>3.2</strong>
    La garanzia termina alle ore 24.00 del giorno in cui si verifica il primo tra i seguenti eventi:
</p>
<ul>
    <li>
        <p align="JUSTIFY">
            la scadenza del periodo di copertura concesso dall'Assicuratore;
        </p>
    </li>
    <li>
        <p align="JUSTIFY">
            il raggiungimento del limite massimo di 50.000 Km di percorrenza dell’Autoveicolo dalla data del tagliando di manutenzione.
        </p>
    </li>
</ul>
<p align="JUSTIFY">
    <strong>Art. 4 - OBBLIGHI DELL’ASSICURATO IN CASO DI SINISTRO</strong>
</p>
<p align="JUSTIFY">
    <strong>4.1 </strong>
    In caso di sinistro, l’Assicurato si metterà in contatto con la Centrale Operativa fornendo:
</p>
<p align="JUSTIFY">
    a) la targa dell’Autoveicolo;
</p>
<p align="JUSTIFY">
    b) modello dell’Autoveicolo;
</p>
<p align="JUSTIFY">
    c) copia dell’ultimo tagliando di manutenzione;
</p>
<p align="JUSTIFY">
    c) tipologia del guasto;
</p>
<p align="JUSTIFY">
    d) preventivo e foto delle parti danneggiate dell’Autoveicolo.
</p>
<p align="JUSTIFY">
    Per ogni intervento, l’Assicurato deve usare ricambi rigenerati Opel, o se non disponibili, ricambi originali Opel.
</p>
<p align="JUSTIFY">
    La Centrale Operativa, esaminata la richiesta ed accertati i motivi della richiesta di intervento:
</p>
<ul>
    <li>
        <p align="JUSTIFY">
            autorizza la riparazione e fornisce il numero di sinistro che deve essere incluso in tutte le future
        </p>
    </li>
</ul>
<p align="JUSTIFY">
    comunicazioni relative al Sinistro.
</p>
<p align="JUSTIFY">
    Oppure
</p>
<ul>
    <li>
        <p align="JUSTIFY">
            Fornisce un rifiuto motivato.
        </p>
    </li>
</ul>
<p align="JUSTIFY">
    Oppure
</p>
<ul>
    <li>
        <p align="JUSTIFY">
            Si riserva di chiedere maggiori informazioni o documentazione aggiuntiva sul sinistro al fine di valutare la relativa autorizzazione.
        </p>
    </li>
</ul>
<p align="JUSTIFY">
    Oppure
</p>
<ul>
    <li>
        <p align="JUSTIFY">
            Stabilisce la necessità di un intervento di un perito della Compagnia al fine di valutare il danno. In tal caso l’Assicurato dovrà mettere a
            disposizione dell’Assicuratore o di suoi delegati l’Autoveicolo oggetto del sinistro.
        </p>
    </li>
</ul>
<p align="JUSTIFY">
    Una volta autorizzata la riparazione, la fattura sarà pagata direttamente dalla Compagnia all’Assicurato.
</p>
<p align="JUSTIFY">
    <strong>Nessuna riparazione dovrà essere effettuata dall’Assicurato senza la previa autorizzazione da parte della Centrale Operativa.</strong>
</p>
<p align="JUSTIFY">
    <strong>4.2</strong>
    Qualora il Guasto si verifichi al di fuori del territorio italiano, Città del Vaticano, Repubblica di San Marino, ma comunque in uno dei paesi dove la
    Carta Verde è valida, e il Beneficiario abbia provveduto alla riparazione dell’Autoveicolo senza preventiva Autorizzazione, la denuncia del Guasto dovrà
    essere effettuata dal Beneficiario all’indirizzo di seguito riportato, entro e non oltre il 30° (trentesimo) giorno successivo alla data in cui il
    Beneficiario è venuto a conoscenza del Guasto.
</p>
<p align="JUSTIFY">
    La Compagnia di Assicurazione valuterà il Sinistro in conformità con le presenti condizioni di assicurazione.
</p>
<p align="JUSTIFY">
    Si ricorda che la mancata tempestiva denuncia comporta la perdita del diritto all’Indennizzo:
</p>
<p align="CENTER">
    <strong>Inter Partner Assistance S.A.</strong>
</p>
<p align="CENTER">
    <strong>sede in Corso Como, 17</strong>
</p>
<p align="CENTER">
    <strong>20154 – Milano </strong>
</p>
<p align="CENTER">
    Tel. _____________
</p>
<p align="CENTER">
    Fax: _______________
</p>
<p align="CENTER">
    __________________@axa-assistance.com
</p>
<ol start="4">
    <ol start="3">
        <li>
            <p align="JUSTIFY">
                L’Assicurato deve altresì, a pena di decadenza dal diritto all’indennizzo:
            </p>
        </li>
    </ol>
</ol>
<ul>
    <li>
        <p align="JUSTIFY">
            evitare ulteriore utilizzo dell’ Autoveicolo assicurato al fine di non aggravarne il danno;
        </p>
    </li>
    <li>
        <p align="JUSTIFY">
            non procedere a riparazioni in autonomia salvo il caso previsto all’Art. 5.2;
        </p>
    </li>
    <li>
        <p align="JUSTIFY">
            fornire alla Centrale Operativa la seguente documentazione per la definizione del Sinistro:
        </p>
    </li>
</ul>
<ul>
    <li>
        <p align="JUSTIFY">
            descrizione delle cause del guasto;
        </p>
    </li>
    <li>
        <p align="JUSTIFY">
            fattura di riparazione;
        </p>
    </li>
    <li>
        <p align="JUSTIFY">
            copia fronte/retro del libretto di circolazione del veicolo Assicurato;
        </p>
    </li>
    <li>
        <p align="JUSTIFY">
            copia del libretto tagliandi dove risulti effettuata il tagliando presso la rete autorizzata Opel;
        </p>
    </li>
    <li>
        <p align="JUSTIFY">
            ogni altra documentazione tecnica necessaria alla valutazione del Sinistro.
        </p>
    </li>
</ul>
<p align="JUSTIFY">
    <em><strong>N.B.</strong></em>
    <em> In caso di necessità di perizia tecnica l’Assicurato dovrà mettere il veicolo oggetto del sinistro</em>
    <em>a disposizione della Compagnia o di suoi delegati. </em>
</p>
<p align="JUSTIFY">
    <strong>Art. 5 - LIQUIDAZIONE DELL’INDENNIZZO</strong>
</p>
<p align="JUSTIFY">
    <strong>5.1 Riparazioni eseguite nel territorio italiano:</strong>
</p>
<p align="JUSTIFY">
    La Compagnia di Assicurazione liquiderà all'Assicurato l'importo relativo alla riparazione autorizzata, ai sensi delle presenti condizioni di
    assicurazione. L’Assicurato dovrà farsi carico dell’IVA applicata all’importo relativo alla riparazione autorizzata.
</p>
<p align="JUSTIFY">
    L’Assicuratore si riserva facoltà di richiedere all’Assicurato copia della documentazione comprovante l’avvenuta riparazione del guasto.
</p>
<p align="JUSTIFY">
    <strong>5.2 Riparazioni eseguite al di fuori del territorio italiano:</strong>
</p>
<p align="JUSTIFY">
    Per le riparazioni effettuate all’estero ma comunque in uno dei paesi dove la Carta Verde è valida, la Compagnia di Assicurazione liquiderà al Beneficiario
    il costo delle parti di ricambio e della manodopera necessari alla riparazione, inclusa l’IVA applicabile nel Paese estero, tuttavia nei limiti dei
    Massimali descritti all’art. 1.2 delle presenti condizioni di assicurazione.
</p>
<p align="JUSTIFY">
    <strong>5.3</strong>
    L’Assicuratore pagherà all’Assicurato, a seconda dei casi e di quanto previsto all’art. 6, il relativo indennizzo entro il 30° giorno successivo alla data
    di definizione del sinistro. Il rimborso dei Sinistri autorizzati avverrà mediante bonifico bancario direttamente sul numero di conto corrente
    dell’Assicurato, o del Beneficiario in caso di Sinistro avvenuto all’estero.
</p>
<p align="JUSTIFY">
    <strong>Art. 6 – DECADENZA</strong>
</p>
<p align="JUSTIFY">
    Il mancato adempimento agli obblighi previsti al punto 5 può comportare la decadenza dell’Assicurato da ogni diritto all’indennizzo, valendo quale
    omissione dolosa agli obblighi normativi previsti negli artt. 1913 – 1914 – 1915 del Codice Civile.
</p>
<p align="JUSTIFY">
    <strong>Art. 7 - PREMIO </strong>
</p>
<p align="JUSTIFY">
    Il premio di Polizza comprensivo delle tasse assicurative pari al 21,25% viene versato all’Assicuratore in unica soluzione per tutta la durata della
    copertura da parte del Contraente per singolo tagliando di manutenzione effettuato dagli Assicurati che hanno contestualmente omaggiato i Beneficiari della
    garanzia Guasti per Autoveicoli a marchio Opel.
</p>
<p align="JUSTIFY">
    <strong>Art. 8 – RECESSO</strong>
</p>
<p align="JUSTIFY">
    Il Contraente potrà recedere da ogni singola attivazione entro 30 giorni dalla data di decorrenza della copertura mediante comunicazione alla Compagnia. In
    tal caso il premio gli sarà interamente rifuso.
</p>
<p align="JUSTIFY">
    Qualora il recesso avvenga oltre il termine di 30 giorni dalla decorrenza dell’Assicurazione, il Contraente non avrà diritto alla restituzione del premio.
</p>
<p align="JUSTIFY">
    <strong>Art. 9 - LEGGE APPLICABILE E FORO COMPETENTE</strong>
</p>
<p align="JUSTIFY">
    <strong>9.1</strong>
    La legge applicabile è la Legge Italiana.
</p>
<p align="JUSTIFY">
    <strong>9.2</strong>
    Per qualsiasi controversia che dovrebbe insorgere tra la Compagnia e l’Assicurato inerente l’interpretazione, l’esecuzione o la validità del Contratto di
    Assicurazione è competente il foro di residenza o di domicilio dell’Assicurato.
</p>
<p align="JUSTIFY">
    <strong>Art. 10 - SERVIZIO CLIENTI</strong>
</p>
<p align="JUSTIFY">
    <strong>10.1</strong>
    Ogni eventuale informazione relativa al rapporto contrattuale o alla gestione dei sinistri potrà essere richiesta a:
</p>
<p align="CENTER">
    <strong>Inter Partner Assistance S.A.</strong>
</p>
<p align="CENTER">
    <strong>sede in Corso Como, 17</strong>
</p>
<p align="CENTER">
    <strong>20154 – Milano</strong>
</p>
<p align="CENTER">
    Tel. _____________
</p>
<p align="CENTER">
    Fax: _______________
</p>
<p align="CENTER">
    <u><a class="testoParagraph" href="mailto:__________________@axa-assistance.com">__________________@axa-assistance.com</a></u>
</p>
<p>
    <strong>10.2</strong>
    Tutti i sinistri saranno liquidati dall’<strong>Assicuratore</strong> senza IVA, ad eccezione delle riparazioni effettuate all’estero che saranno
rimborsate sino all’importo massimo autorizzato per la riparazione, comprensivo dell’IVA straniera, calcolata sull’importo autorizzato.
</p>
<p align="JUSTIFY">
    <strong>Art. 11 - RECLAMI</strong>
</p>
<p align="JUSTIFY">
    <strong>11.1</strong>
    Eventuali reclami possono essere presentati alla Compagnia, all’Istituto per la Vigilanza sulle Assicurazioni (IVASS) e all’autorità francese competente
    (FFSA) secondo le disposizioni che seguono:
</p>
<p align="JUSTIFY">
    <strong>- Alla Compagnia</strong>
</p>
<p align="JUSTIFY">
    Vanno indirizzati i reclami aventi ad oggetto la gestione del rapporto Contrattuale, segnatamente sotto il profilo dell’attribuzione di responsabilità,
    della effettività della prestazione, della quantificazione ed erogazione delle somme dovute all’avente diritto o dei sinistri.
</p>
<p align="JUSTIFY">
    I reclami devono essere inoltrati a:
</p>
<p align="CENTER">
    via posta:<strong> Axa France Iard S.A. </strong>
</p>
<p align="CENTER">
    <strong>sede in 313 Terrasses de l’Arche</strong>
</p>
<p align="CENTER">
    <strong>92727 Nanterre (Francia)</strong>
</p>
<p align="CENTER">
    Tel. _____________
</p>
<p align="CENTER">
    Fax: _______________
</p>
<p align="CENTER">
    __________________@axa.fr
</p>
<p align="JUSTIFY">
    I reclami devono contenere i seguenti elementi: nome, cognome e domicilio del reclamante, denominazione dell’impresa, dell’Intermediario Assicurativo o dei
    soggetti di cui si lamenta l’operato, breve descrizione del motivo della lamentela ed ogni documento utile a descrivere compiutamente il fatto e le
    relative circostanze.
</p>
<p align="JUSTIFY">
    La Compagnia ricevuto il reclamo deve fornire riscontro entro il termine di 45 giorni dalla data di ricevimento del reclamo, all’indirizzo fornito dal
    reclamante.
</p>
<p align="JUSTIFY">
    <strong>- All’IVASS </strong>
</p>
<p align="JUSTIFY">
    Vanno indirizzati i reclami:
</p>
<p align="JUSTIFY">
    - aventi ad oggetto l’accertamento dell’osservanza delle disposizioni del Codice delle Assicurazioni Private, delle relative norme di attuazione e del
    Codice del Consumo (relative alla commercializzazione a distanza di servizi finanziari al consumatore), da parte delle imprese di Assicurazione e di
    Riassicurazione, degli intermediari e dei periti assicurativi;
</p>
<p align="JUSTIFY">
    - nei casi in cui l’esponente non si ritenga soddisfatto dall’esito del reclamo inoltrato alla Compagnia o in caso di assenza di riscontro da parte della
    Compagnia nel termine di 45 giorni.
</p>
<p align="JUSTIFY">
    I reclami devono essere inoltrati per iscritto a:
</p>
<p align="CENTER">
    <strong>I.V.A.S.S.</strong>
</p>
<p align="CENTER">
    Istituto per la Vigilanza sulle Assicurazioni,
</p>
<p align="CENTER">
    Servizio Tutela degli Utenti,
</p>
<p align="CENTER">
    Via del Quirinale 21, 00187 Roma
</p>
<p align="CENTER">
    Fax numero: 06/42.133.745/353
</p>
<p align="JUSTIFY">
    corredando l’esposto della documentazione relativa al reclamo trattato dalla Società.
</p>
<p align="JUSTIFY">
    Per la risoluzione delle liti transfrontaliere è possibile presentare il reclamo all’IVASS o direttamente al sistema estero competente - individuabile al
    sito www.ec.europa.eu/fin-net - e chiedendo l’attivazione della procedura FIN-NET.
</p>
<p align="JUSTIFY">
    <strong>- All’Autorità francese competente</strong>
</p>
<p align="JUSTIFY">
    I reclami possono essere indirizzati a Le Médiateur de la Fédération Française des Sociétés d'Assurances (FFSA), BP 290, 75425 PARIS CEDEX 09, fax
    01.45.23.27.15 e-mail: le.mediateur@mediation-assurance.org, sito web: www.ffsa.fr chiedendo l’attivazione della procedura FIN-NET.
</p>
<p align="JUSTIFY">
    <strong>11.2</strong>
    Resta salva la facoltà di adire l’Autorità Giudiziaria.
</p>
<p align="CENTER">
    <strong>SEZIONE II</strong>
</p>
<p align="CENTER">
    <strong>CONDIZIONI GENERALI APPLICABILI A TUTTE LE COPERTURE</strong>
</p>
<p>
    <strong>1. DICHIARAZIONI CONCERNENTI IL RISCHIO</strong>
</p>
<p align="JUSTIFY">
    La presente Polizza è sottoscritta sulla base di informazioni che hanno determinato l’accettazione del rischio da parte della Compagnia di Assicurazione e
    in relazione a loro è stato calcolato il Premio.
</p>
<p align="JUSTIFY">
    Il Contraente e/o l’Assicurato ha il dovere di informare l’Assicuratore della natura e delle circostanze del rischio ed all’occorrenza di ogni fatto a sua
    conoscenza che può aggravarlo o modificarlo.
</p>
<p align="JUSTIFY">
    Secondo quanto previsto dall’art. 1893 cod. civ., la Compagnia di Assicurazione avrà diritto di recedere dalla Polizza per mezzo di raccomandata A.R. da
    indirizzarsi all’Assicurato nei 3 (tre) mesi dall’avvenuta conoscenza del fatto che questi abbia fornito informazioni inesatte o reticenti sul rischio. In
    tal caso il Premio già versato sarà trattenuto dall’Assicuratore.
</p>
<p align="JUSTIFY">
    Se il danno interviene prima che l’Assicuratore invii tale comunicazione, il pagamento dell’Indennizzo avverrà secondo quanto previsto dall’art. 1893,
    secondo comma, cod. civ.
</p>
<p align="JUSTIFY">
    Secondo quanto previsto dall’art. 1892 cod. civ. quando il Contraente e/o l’Assicurato abbiano agito con dolo o colpa grave, l’Assicuratore decade dal
    diritto di impugnare il contratto se, entro 3 (tre) mesi dal giorno in cui ha conosciuto l’inesattezza della dichiarazione o la reticenza, non dichiara al
    Contraente e/o l’Assicurato di voler esercitare l’impugnazione. Tuttavia, se il Sinistro si verifica prima che siano decorsi 3 (tre) mesi dal giorno in cui
    l’Assicuratore ha conosciuto l’inesattezza della dichiarazione o la reticenza, l’Assicuratore medesimo sarà liberato dal pagamento dell’Indennizzo. In ogni
    caso, qualora l’inesattezza della dichiarazione o la reticenza si traducano in un aggravamento del rischio, l’Assicuratore avrà comunque diritto ad un
    adeguamento del premio, con effetto retroattivo, in ragione del maggior rischio assicurato anche decorsi 3 (tre) mesi dalla scoperta dell’inesattezza o
    della reticenza.
</p>
<p align="JUSTIFY">
    <strong>2, AGGRAVAMENTO DEL RISCHIO E POTERI DELL’ASSICURATORE</strong>
</p>
<p align="JUSTIFY">
    Il Contraente e/o l’Assicurato deve informare l’Assicuratore nel più breve tempo possibile di ogni circostanza che possa aggravare il rischio e di ogni
    altra circostanza che, se conosciuta dall’Assicuratore al momento del perfezionamento della Polizza, non avrebbe fatto stipulare a quest’ultimo la Polizza,
    o lo avrebbe fatto stipulare con Premio una maggiorazione dello stesso.
</p>
<p align="JUSTIFY">
    Qualora durante la vigenza del Contratto all’Assicuratore sia comunicato un aggravamento del rischio, questi avrà diritto di proporre una modificazione del
    Premio del Contratto nel periodo di 30 (trenta) giorni dalla data in cui l’aggravamento del rischio sia stato comunicato. In tal caso l’Assicurato entro 30
    (trenta) giorni dalla sua ricezione potrà accettare o rifiutare la proposta. Una volta che tale termine sia trascorso, sia che la proposta sia stata
    rifiutata, sia che l’Assicurato non abbia manifestato alcuna risposta, l’Assicuratore avrà facoltà di recedere dalla Polizza ai sensi e per gli effetti
    dell’art. 1898 del Codice Civile.
</p>
<p align="JUSTIFY">
    <strong>3. MANCATA COMUNICAZIONE DELL’AGGRAVAMENTO DEL RISCHIO</strong>
</p>
<p align="JUSTIFY">
    Se accade un Sinistro prima che l’aggravamento del rischio sia stato a lui dichiarato, l’Assicuratore sarà liberato dal pagamento dell’Indennizzo se
    l’Assicurato abbia agito con dolo o colpa grave ovvero se l’aggravamento è tale che l’assicuratore non avrebbe stipulato la Polizza se l’avesse conosciuto.
    Qualora non ricorra tale caso, il pagamento dell’Indennizzo dall’Assicuratore sarà ridotto proporzionalmente sulla base della differenza tra il Premio
    concordato e quello che sarebbe stato applicabile qualora fosse stata conosciuta la vera natura del rischio.
</p>
<p align="JUSTIFY">
    <strong>4. ALTRE ASSICURAZIONI</strong>
</p>
<p align="JUSTIFY">
    Il Contraente e/o l’Assicurato devono informare l’Assicuratore dell’esistenza di ogni altra polizza sottoscritta con altri assicuratori che coprano il
    medesimo rischio per un uguale periodo di tempo.
</p>
<p align="JUSTIFY">
    Qualora la dichiarazione di esistenza di altre polizze di Assicurazione sia stata omessa volontariamente, nel caso in cui ricorra un Sinistro in situazione
    di sovra-Assicurazione, l’Assicuratore opererà come assicuratore di secondo rischio e non procederà al pagamento dell’Indennizzo, se non per la parte di
    danno effettivamente scoperta.
</p>
<p align="JUSTIFY">
    <strong>5. SURROGAZIONE</strong>
</p>
<p align="JUSTIFY">
    A seguito del pagamento dell’Indennizzo, l’Assicuratore potrà surrogarsi nei diritti e nelle azioni spettanti all’Assicurato, a seguito della richiesta di
    risarcimento, nei confronti dei terzi responsabili.
</p>
<p align="JUSTIFY">
    <strong>6. PRESCRIZIONE</strong>
</p>
<p align="JUSTIFY">
    I diritti spettanti all’Assicurato in base al presente Contratto possono essere comunque esercitati entro 2 (due) anni dal verificarsi del Sinistro, così
    come indicato dall’Articolo 2952 Codice Civile e successive modificazioni.
</p>
<p align="JUSTIFY">
    <strong>8. ESCLUSIONI RELATIVE A TUTTE LE COPERTURE</strong>
</p>
<p align="JUSTIFY">
    Senza pregiudizio alcuno delle esclusioni indicate nelle rispettive sezione, l’Assicuratore non coprirà:
</p>
<p align="JUSTIFY">
    - Sinistri originati da dolo o colpa grave, da parte dell’Assicurato
</p>
<p align="JUSTIFY">
    - Sinistri che avvengano o siano conseguenza di:
</p>
<p align="JUSTIFY">
    (i) eventi sociali o politici, scioperi, sommosse e simili;
</p>
<p align="JUSTIFY">
    (ii) guerre civili o internazionali, benché non ufficialmente dichiarate, e in generale ogni tipo di operazione militare o di polizia;
</p>
<p align="JUSTIFY">
    (iii) eruzioni vulcaniche, terremoti, tifoni ed ogni altro evento sismico o meteorologico straordinario o di natura catastrofica;
</p>
<p align="JUSTIFY">
    (iv) sinistri conseguenza di eventi causati da reazioni nucleari;
</p>
<p align="JUSTIFY">
    (v) danni cagionati indirettamente dal Sinistro o che ne siano ulteriore conseguenza;
</p>
<p align="JUSTIFY">
    (vi) sinistri occorsi a beni in genere che non siano oggetto di produzione in serie;
</p>
<p align="JUSTIFY">
    (vii) sinistri che costituiscono effetto di un rischio che non sia espressamente garantito.
</p>
</body>
</html>