<html>
<head>
    <meta name="layout" content="main">
   %{-- <meta http-equiv="refresh" content="0; url=http://opel1.nais.biz/OPEL/">--}%
    <title>Login</title>

</head>
<body>

<div class="rowLogin animated fadeInUp">
    <div class="col-xs-12 col-md-12">
        <div class="login-panel">
            <div>
                <div class="row">
                    <div class="col-xs-12 col-md-12 text-center" style="margin-right: 12px;">
                        <asset:image src="loghi/Opel_Financial_Services.png" height="80px" id="logo-financial" class="loghi-opel"/>
                        <asset:image src="loghi/logo_Opel.png" height="80px" id="logo-opel" style="vertical-align: inherit;"/>
                        %{--<h1 class="col-md-12">Portale OPEL</h1>--}%
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-8 col-xs-offset-2 col-md-offset-3 login-body">
                <g:renderFlash/>
                <f:form>
                    <sec:authenticationError/>
                    <div class="col-md-10 col-xs-10 col-xs-push-1 col-md-offset-3">
                        <input type="text" align="center" name="username" class="form-control inputLogin"  id="username" required="true"  placeholder="CODICE" >
                    </div>
                    <div class="col-md-10 col-xs-10 col-xs-push-1 col-md-offset-3">
                        <input type="text" align="center" name="password" class="form-control inputLogin" id="password" required="true"  placeholder="PARTITA IVA" >
                    </div>
                %{-- <f:field control-class="col-md-5 col-xs-6 margin-login"  type="text" name="username" required="true" label=" " placeholder="CODICE" />
                 <f:field control-class="col-md-5 col-xs-6 margin-login" type="text"   name="password" required="true" label =" " placeholder="PARTITA IVA"/>--}%
                %{--<f:field name="remember-me" type="checkbox" label="Ricordami" control-class="col-md-10"/>--}%
                    <div class="form-group">
                        <label for="remember-me" class="checkbox col-md-12 col-md-offset-5 col-xs-offset-2 col-xs-6">
                            <input type="checkbox"  id="remember-me" name="remember-me"  value="1" > Ricordami
                        </label>
                    </div>
                    <div class="col-md-10 col-md-offset-3 col-xs-push-1 col-xs-10">
                        <button type="submit" class="btn btn-yellow btn-login"><b>Login&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></button>
                    </div>

                </f:form>
            </div>
            <div class="col-md-12">
                <footer>
                    %{--<div class="col-md-10 col-md-offset-5 col-xs-10 col-xs-offset-7" style="margin-top:90px;">
                       <p><asset:image src="/loghi/logo_mach1_small.png" style="" /></p>
                   </div>--}%
                    <div class="col-md-12  col-xs-push-0 col-xs-10" style="margin-top:90px;">
                        <p class="testofooter" align="center"> Mach1 s.r.l. - Via Vittor Pisani, 13/B  – 20124 Milano - TEL. 02 30465068 FAX 02 62694254 - Email PEC: mach1@registerpec.it - CCIAA Milano - REA MI 1908726 - C.F.,  P. IVA e Reg. Imprese Milano 06680830962 - Capitale sociale 100.000 EURO - Registro Unico Intermediari  A000317603 - Banco Posta Sede di Milano, Piazza Cordusio, 1 - IBAN IT 54 C 07601 01600 0000 99701393</p>
                    </div>
                </footer>
            </div>
        </div>
    </div>
</div>
</body>
</html>