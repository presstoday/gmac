<html>
<head>
    <meta name="layout" content="navigazione_menu"/>
    <title>Scelta Menu</title>
</head>
<body>
<div class="rowMenu">
        <sec:ifHasNotRoles roles="RCA_GRATUITA, DEMO_OPEN">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-body">
                        <f:form controller="menu" id="menuform" class="col-md-12 col-xs-12">
                            <g:link action="extensionWarranty"><div class="btn btn-yellow col-md-6 col-md-offset-3 col-xs-12"><b>GARANZIA MOTORE TRASMISSIONE CAMBIO</b></div></g:link>
                        </f:form>
                    </div>
                </div>
            </div>
        </sec:ifHasNotRoles>
        <sec:ifHasNotRoles roles="EXTENSION_WARRANTY,DEMO_OPEN">
            <sec:ifHasRole role="RCA_GRATUITA">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-body">
                            <f:form controller="menu" id="menuform" class="col-md-12">
                                <g:link action="rcaGratuita"><div class="btn btn-yellow col-md-6 col-md-offset-3 col-xs-12"><b>RCA GRATUITA</b></div></g:link>
                            </f:form>
                        </div>
                    </div>
                </div>
            </sec:ifHasRole>
        </sec:ifHasNotRoles>
        <sec:ifHasNotRoles roles="RCA_GRATUITA,EXTENSION_WARRANTY">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-body">
                        <f:form controller="menu" id="menuform" class="col-md-12">
                            <g:link action="demoOpen"><div class="btn btn-yellow col-md-6 col-md-offset-3 col-xs-12"><b>DEMO OPEN</b></div></g:link>
                        </f:form>
                    </div>
                </div>
            </div>
        </sec:ifHasNotRoles>
        <sec:ifHasAllRoles roles="RCA_GRATUITA,EXTENSION_WARRANTY,DEMO_OPEN">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-body">
                        <f:form controller="menu" id="menuform" class="col-md-12">
                                    <g:link action="rcaGratuita"><div class="btn btn-yellow col-md-2 col-xs-6"><b>RCA GRATUITA</b></div></g:link>
                            <div class="col-md-1"></div>
                                    <g:link  action="extensionWarranty"><div class="btn btn-yellow col-md-6 col-xs-12"><b>GARANZIA MOTORE TRASMISSIONE CAMBIO</b> </div></g:link>
                            <div class="col-md-1"></div>
                                    <g:link action="demoOpen"><div class="btn btn-yellow col-md-2 col-xs-6"><b>DEMO OPEN</b></div></g:link>
                        </f:form>
                    </div>
                </div>
            </div>
        </sec:ifHasAllRoles>
        <sec:ifHasNotRole role="RCA_GRATUITA">
            <sec:ifHasAllRoles roles="EXTENSION_WARRANTY, DEMO_OPEN" >
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-body">
                            <f:form controller="menu" id="menuform" class="col-md-12">
                                <div class="col-md-2"></div>
                                <g:link  action="extensionWarranty"><div class="btn btn-yellow  col-md-5 col-xs-10"><b>GARANZIA MOTORE TRASMISSIONE CAMBIO</b> </div></g:link>
                                <div class="col-md-1 col-xs-1"></div>
                                <g:link action="demoOpen"><div class="btn btn-yellow  col-md-2 col-xs-6"><b>DEMO OPEN</b></div></g:link>
                            </f:form>
                        </div>
                    </div>
                </div>
            </sec:ifHasAllRoles>
        </sec:ifHasNotRole>
    <sec:ifHasNotRole role="EXTENSION_WARRANTY">
        <sec:ifHasAllRoles roles="RCA_GRATUITA, DEMO_OPEN">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-body">
                        <f:form controller="menu" id="menuform" class="col-md-12">
                            <div class="col-md-2"></div>
                            <g:link  action="rcaGratuita"><div class="btn btn-yellow  col-md-4  col-xs-6"><b>RCA GRATUITA</b> </div></g:link>
                            <div class="col-md-1"></div>
                            <g:link action="demoOpen"><div class="btn btn-yellow  col-md-2  col-xs-6"><b>DEMO OPEN</b></div></g:link>
                        </f:form>
                    </div>
                </div>
            </div>
        </sec:ifHasAllRoles>
    </sec:ifHasNotRole>
    <sec:ifHasNotRole role="DEMO_OPEN">
        <sec:ifHasAllRoles roles="RCA_GRATUITA, EXTENSION_WARRANTY">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-body">
                        <f:form controller="menu" id="menuform" class="col-md-12">
                            <div class="col-md-1"></div>
                            <g:link  action="rcaGratuita"><div class="btn btn-yellow  col-md-4  col-xs-6"><b>RCA GRATUITA</b> </div></g:link>
                            <div class="col-md-1 col-xs-1"></div>
                            <g:link action="extensionWarranty"><div class="btn btn-yellow  col-md-5  col-xs-10"><b>GARANZIA MOTORE TRASMISSIONE CAMBIO</b></div></g:link>
                        </f:form>
                    </div>
                </div>
            </div>
        </sec:ifHasAllRoles>
    </sec:ifHasNotRole>
        <sec:ifHasNotRoles roles="RCA_GRATUITA,EXTENSION_WARRANTY,DEMO_OPEN">
            <div class="col-xs-10 col-xs-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title text-center">Non hai nessun accesso ancora</h3>
                    </div>
                </div>
            </div>
        </sec:ifHasNotRoles>
</div>
<script type="text/javascript">
   /* $( document ).ready(function() {
        %{--<g:if test="${utente?.accettazione==false}">--}%
            $("#modalCondizioniPolizza").modal({
                backdrop: 'static',
                keyboard: false
            });
        %{--</g:if>--}%

    });
    function closewindow() {
        $("#modalCondizioniPolizza").modal.close();
    }
    function acettoValidate(){
        %{--$.post( "${createLink(action: 'accettazione')}", $("#menuform").serialize(), function() {--}%

        });
    }*/
</script>
</body>
</html>