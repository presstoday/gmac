
<%@ page contentType="text/html;charset=UTF-8"%>
<g:javascript>
   window.appContext = '${request.contextPath}';
</g:javascript>
<html>
<head>
    <meta name="layout" content="navigazione"/>
    <title>GARANZIA MOTORE TRASMISSIONE CAMBIO</title>
    <script language="javascript">
        if(history.length > 0) history.forward()
    </script>
</head>

<body>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="text-center">GARANZIA MOTORE TRASMISSIONE CAMBIO</h3>
            </div>
            <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <f:form  id="listExtensionform" method="get">
                                <sec:ifHasRole role="DEALER">
                                        <div class="col-md-6 col-xs-12 pull-left" style="margin-bottom: 4px;">
                                            <g:link controller="polizze" action="creaPolizzaExtension"><div class="btn btn-nuovaPolizza col-md-5 col-xs-8"><b>Nuova polizza</b></div></g:link>
                                        </div>
                                </sec:ifHasRole>
                                <sec:ifHasRole role="ADMIN">
                                    <div class="col-md-8 col-xs-12 pull-left" style="margin-bottom: 4px;">
                                        <a href="${createLink(controller: "polizze", action: "generaFilePolizzeattivate")}" target="_blank" class="btn  btn-small" title="Estrazione polizze"><b>Estrarre polizze </b> <i class="fa fa-download"></i></a>
                                    </div>

                                </sec:ifHasRole>
                                <div class="col-md-3 col-xs-12 pull-right">
                                    <div class="input-group input-ricerca">
                                        <input type="text" class="form-control" placeholder="ricerca polizze" name="search" id="search" value="${params.search}">
                                        <span class="input-group-btn">
                                            <button type="submit" id="button_search" class="btn btn-ricerca" onchange="window.location.href = window.location.href.substr(0,window.location.href.lastIndexOf('?'));"><i class="glyphicon glyphicon-search"></i></button>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-12 col-xs-12">
                                    <a href="${createLink(controller: "polizze", action: "fascicoloInformativoGuasti", id:1)}" target="_blank" class="btn  btn-small" title="Fascicolo informativo"><b>Fascicolo informativo </b><i class="fa fa-download"></i></a>
                                    <a href="${createLink(controller: "polizze", action: "guidautilizzoPortale")}" target="_blank" class="btn  btn-small" title="Procedura operativa Dealer"><b>Guida all'utilizzo del Portale </b><i class="fa fa-download"></i></a>
                                    <a href="${createLink(controller: "polizze", action: "proceduraSinistri")}" target="_blank" class="btn  btn-small" title="Procedura sinistri"><b>Procedura sinistri </b><i class="fa fa-download"></i></a>
                                </div>
                            </f:form>
                        </div>
                        <div class="table-responsive col-md-12">
                            <table class="table table-striped">
                                <thead>
                                <tr><sec:ifHasRole role="ADMIN">
                                    <th class="text-center">DEALER&nbsp&nbsp </th>
                                    <th class="text-center">TRACC.</th>
                                </sec:ifHasRole>
                                    <th class="text-center vcenter-column">LEAFLET</th>
                                    <th class="text-center vcenter-column">NO. POLIZZA</th>
                                    <th class="text-center">&nbspMODELLO&nbsp</th>
                                    <th class="text-center">TARGA</th>
                                    <th class="text-center vcenter-column">TELAIO</th>
                                    <th class="text-center">KM</th>
                                    <th class="text-center">DATA&nbsp&nbsp IMMATR.&nbsp&nbsp&nbsp</th>
                                    <th class="text-center">DATA TAGLIANDO</th>
                                    <th class="text-center">DATA DECORRENZA</th>
                                    <th class="text-center">DATA SCADENZA</th>
                                    <th class="text-center">ANNULLATA</th>
                                </tr>
                                </thead>
                                <tbody>
                                <g:each var="polizza" in="${polizze}">
                                    <input type="hidden" name="idPolizza" id="idPolizza" value="${polizza.id}"/>
                                    <tr>
                                        <sec:ifHasRole role="ADMIN">
                                        <td class="text-center vcenter-column dealerNome" data-id="${polizza.dealer.id}"  id="dealerNome" name="dealerNome" value="${polizza.dealer.id}">${polizza.dealer.ragioneSociale}</td>
                                            <g:if test="${polizza.tracciato && polizza.tracciatoCompagnia}"><td class="text-center vcenter-column">Si</td></g:if><g:else><td class="text-center vcenter-column">
                                            %{--<g:link action="generaTracciati" id="${polizza.id}" class="bottoneDoc" data-toggle="tooltip" data-container="body" data-placement="left" title="Genera Tracciato a ${polizza.noPolizza}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-file-text" style="cursor: pointer; font-size: 1.40em; color: #555555"></i></g:link>--}%
                                            <a href="${createLink(controller: "polizze", action: "generaTracciati", id: polizza.id)}" class="btn btn-xs" title="Tracciato polizza ${polizza.noPolizza}"><i class="fa fa-file-text-o"></i></a>
                                        </td></g:else>

                                        </sec:ifHasRole>
                                        <td class="text-center vcenter-column"><a href="${createLink(controller: "polizze", action: "leaflet_polizza", id: polizza.id)}" target="_blank" class="btn btn-xs" title="Leaflet polizza ${polizza.noPolizza}"><i class="fa fa-file-pdf-o"></i></a></td>
                                        <td class="text-center vcenter-column">${polizza.noPolizza}</td>
                                        <td class="text-center vcenter-column">${polizza.telaio.modello}</td>
                                        <td class="text-center vcenter-column">${polizza.telaio.targa}</td>
                                        <td class="text-center vcenter-column">${polizza.telaio.telaio}</td>
                                        <td class="text-center vcenter-column">${polizza.km}</td>
                                        <td class="text-center vcenter-column">${polizza.telaio.dataImmatricolazione.format('dd-MM-yyyy')}</td>
                                        <td class="text-center vcenter-column">${polizza.dataTagliando.format('dd-MM-yyyy')}</td>
                                        <td class="text-center vcenter-column">${polizza.dataDecorrenza.format('dd-MM-yyyy')}</td>
                                        <td class="text-center vcenter-column">${polizza.dataScadenza.format('dd-MM-yyyy')}</td>
                                        <td class="text-center vcenter-column"><input type="checkbox"  class="centered annullata" name="annullata${polizza.id}"  value="${polizza.id}" id="annullata${polizza.id}" <g:if test="${polizza.annullata}">checked="checked" </g:if>/></td>
                                    </tr>
                                </g:each>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th colspan="18" class="text-center">Sono state trovate ${polizze?.totalCount ?: 0} polizze</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                <nav aria-label="Page navigation">
                    <ul class="pagination pull-right">
                        <g:if test="${totalPages>1}">
                            <li>
                                <link:list page="1" controller="${controllerName}">
                                    <span aria-hidden="true">&laquo;</span>
                                </link:list>
                            </li>
                        </g:if>
                        <g:each var="p" in="${pages}">
                            <g:if test="${p == page}"><li class="active"></g:if><g:else><li></g:else>
                            <g:if test="${params.search}"><link:list page="${p}" controller="${controllerName}" search="${params.search}">${p}</link:list></g:if>
                            <g:else><link:list page="${p}" controller="${controllerName}">${p}</link:list></g:else>
                            </li>
                        </g:each>
                        <g:if test="${totalPages>1}">
                            <li>
                                <link:list page="${totalPages}" controller="${controllerName}">
                                    <span aria-hidden="true">&raquo;</span>
                                </link:list>
                            </li>
                        </g:if>
                    </ul>
                </nav>

            </div>
        </div>
    </div>
    <div class="modal" id="modalCondizioniPolizza">
        <div class="modal-dialog" >
            <div class="modal-content">
                <div class="modal-header" style="background: #f7d900">
                    <h3 class="article-title text-center"><fa:icon name="info-circle"/> Consenso condizioni polizza</h3>
                </div>
                %{--<div class="modal-body testoParagraph" style="overflow-y: auto; height: 350px;">
                    <g:render template="/polizze/condizioni"/>
                </div>--}%
                <div class="modal-footer">
                    <a href="${createLink(controller: "polizze", action: "fascicoloInformativoGuasti")}" class="btn btn-yellow bottone-scarica" target="_blank" title="Download"><span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span> Scarica</a>
                    <button type="button" onclick="return acettoValidate();" id="bottonmodal" class="btn btn-yellow bottone-accetto"  data-dismiss="modal">Accetto</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="modal fade bs-example-modal-lg"  id="datiDealer">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" style="background: #f7d900">
                    <button type="button" class="close article-title" data-dismiss="modal">&times;</button>
                    <h3 class="article-title text-center"><fa:icon name="info-circle"/> Dati Dealer</h3>
                </div>
                <div class="modal-body testo-datiDealer col-md-12">
                    <input type="hidden" name="idDealer" id="idDealer" value=""/>
                    <div class="col-md-6 testoParagraph" ><label class="testoParagraph control-label">RAGIONE SOCIALE:</label>
                        <div class="input-group">
                            <span class="input-group-addon"><span class="fa fa-user"></span></span>
                            <f:input control-class="col-sm-6" type="text" name="ragionesociale"  id="ragionesociale" value="" readonly="true"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="testoParagraph control-label">PARTITA IVA</label>
                        <div class="input-group">
                            <span class="input-group-addon"><span class="fa fa-list-alt"></span></span>
                            <f:input control-class="col-sm-6" type="text" name="partitaiva"  value="" readonly="true"/>
                        </div>
                    </div>
                    <div class="col-md-6 testoParagraph" ><label class="testoParagraph control-label">EMAIL:</label>
                        <div class="input-group">
                            <span class="input-group-addon"><span class="fa fa-envelope"></span></span>
                            <f:input control-class="col-sm-6" type="text" name="email"  id="email" value="" readonly="true"/>
                        </div>
                    </div>
                    <div class="col-md-6 testoParagraph" ><label class="testoParagraph control-label">INDIRIZZO:</label>
                        <div class="input-group">
                            <span class="input-group-addon"><span class="fa fa-home"></span></span>
                            <f:input control-class="col-sm-6" type="text" name="indirizzo"  id="indirizzo" value="" readonly="true"/>
                        </div>
                    </div>
                    <div class="col-md-6 testoParagraph" ><label class="testoParagraph control-label">LOCALITA':</label>
                        <div class="input-group">
                            <span class="input-group-addon"><span class="fa fa-globe"></span></span>
                            <f:input control-class="col-sm-6" type="text" name="localita"  id="localita" value="" readonly="true"/>
                        </div>
                    </div>
                    <div class="col-md-6 testoParagraph" ><label class="testoParagraph control-label">PROVINCIA:</label>
                        <div class="input-group">
                            <span class="input-group-addon"><span class="fa fa-globe"></span></span>
                            <f:input control-class="col-sm-6" type="text" name="provincia"  id="provincia" value="" readonly="true"/>
                        </div>
                    </div>
                    <div class="col-md-6 testoParagraph" ><label class="testoParagraph control-label">CAP:</label>
                        <div class="input-group">
                            <span class="input-group-addon"><span class="fa fa-inbox"></span></span>
                            <f:input control-class="col-sm-6" type="text" name="cap"  id="cap" value="" readonly="true"/>
                        </div>
                    </div>
                    <div class="col-md-6 testoParagraph" ><label class="testoParagraph control-label">TELEFONO:</label>
                        <div class="input-group">
                            <span class="input-group-addon"><span class="fa fa-phone"></span></span>
                            <f:input control-class="col-sm-6" type="text" name="telefono"  id="telefono" value="" readonly="true"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label class="testoParagraph control-label">RUOLI:</label>
                    </div>
                    <div class="col-md-12 testoParagraph rouli">
                        <div class="col-md-4">
                            <label for="ruolo1">
                                <input type="checkbox" class="extension"  id="ruolo1" name="ruolo1"  value="extension_warranty" >GARANZIA MOTORE TRASMISSIONE CAMBIO
                            </label>
                        </div>
                        <div class="col-md-4">
                            <label for="ruolo2">
                                <input type="checkbox" class="demo" id="ruolo2" name="ruolo2"  value="demo_open" > DEMO OPEN
                            </label>
                        </div>
                        <div class="col-md-4">
                            <label for="ruolo3">
                                <input type="checkbox" class="rca" id="ruolo3" name="ruolo3"  value="rca_gratuita" > RCA GRATUITA
                            </label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    %{--<button type="button"  id="bottonmodalPolizza" class="btn btn-yellow" data-dismiss="modal">Chiudere</button>--}%
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
</div>
<script type="text/javascript">
    $( document ).ready(function() {
        <sec:ifHasRole role="DEALER">
            <g:if test="${utente?.accettazione==false}">
            $("#modalCondizioniPolizza").modal({
                backdrop: 'static',
                keyboard: false
            });
            </g:if>
        </sec:ifHasRole>
        <g:if test="${flash.message}">
            swal({
                title: "Polizza salvata!",
                type: "success",
                timer: "2000",
                showConfirmButton: false
            });
        </g:if>
        <g:if test="${flash.error}">
            swal({
                title: "${flash.error}",
                type: "error",
                timer: "2000",
                showConfirmButton: false
            });
        </g:if>
    });

    $('.annullata').change(function() {
        var idPolizza=$(this).val();
        if($(this).is(":checked")) {
            $(this).prop("checked", true);
            $.post("${createLink(controller: "polizze", action: 'annullaPolizza')}",{ id: idPolizza, check:true, _csrf: "${request._csrf.token}"}, function(response) {
                var risposta=response.risposta;
                if(risposta=="aggiornata"){
                    swal({
                        title: "La polizza è stata annullata!",
                        type: "success",
                        timer: "2000",
                        showConfirmButton: false
                    });
                }
            }, "json");
        }else{
            var token = "<sec:csrfToken/>";
            $.post("${createLink(controller: "polizze", action: 'annullaPolizza')}",{ id: idPolizza, check:false, _csrf: token}, function(response) {
                var risposta=response.risposta;
                if(risposta=="aggiornata"){
                    swal({
                        title: "La polizza non è più annullata!",
                        type: "success",
                        timer: "2000",
                        showConfirmButton: false
                    });
                }
            }, "json");
        }
    });
    /*$('.dealerNome').click(function() {
        //alert($(this).data('id'));
        var idPolizza=$(this).data('id');
        $.post("%{--${createLink(controller: "polizze", action: 'getDatiPolizza')}",{ id: idPolizza, _csrf: "${request._csrf.token}--}%"}, function(response) {
            var risposta=response.id;
            if(risposta!="errore"){
                var nome=response.nome +" "+ response.cognome;
                var noPolizza=response.noPolizza;
                var dataDecorrenza=response.dataDecorrenza;
                var dataImmatricolazione=response.dataImmatricolazione;
                var dataTagliando=response.dataTagliando;
                var targa=response.targa;
                var telaio=response.telaio;
                var km=response.km;
                var modello=response.modello;
                var trasmissione=response.trasmissione;
                var versione=response.versione;
                var cilindrata=response.cilindrata;
                $("#datiPolizza").modal({
                    show:true
                });

                $(".testo-datiPolizza #nome").val( nome );
                $(".testo-datiPolizza #noPolizza").val(noPolizza );
                $(".testo-datiPolizza #dataDecorrenza").val(dataDecorrenza );
                $(".testo-datiPolizza #dataImmatricolazione").val(dataImmatricolazione );
                $(".testo-datiPolizza #dataTagliando").val(dataTagliando);
                $(".testo-datiPolizza #targa").val(targa);
                $(".testo-datiPolizza #telaio").val(telaio);
                $(".testo-datiPolizza #km").val(km);
                $(".testo-datiPolizza #modello").val(modello);
                $(".testo-datiPolizza #trasmissione").val(trasmissione);
                $(".testo-datiPolizza #versione").val(versione);
                $(".testo-datiPolizza #cilindrata").val(cilindrata);
            }
        }, "json");

    });*/
    /*$("#button_search").click(function(){
        alert('entro qui');
        var newpath=window.location.href.substr(0,window.location.href.lastIndexOf("?"));
        alert (newpath);
        window.location.assign(newpath);
    });*/
    $('.dealerNome').click(function() {
        //alert($(this).data('id'));
        var idDealer=$(this).data('id');
        $.post("${createLink(controller: "user", action: 'datiDealer')}",{ id: idDealer, _csrf: "${request._csrf.token}"}, function(response) {
            var risposta=response.id;
            if(risposta!="errore"){
                var idDealer=response.id
                var ragionesociale=response.ragionesociale;
                var partitaiva=response.partitaiva;
                var email=response.email;
                var indirizzo=response.indirizzo;
                var localita=response.localita;
                var provincia=response.provincia;
                var cap=response.cap;
                var telefono=response.telefono;
                var ruoli=response.ruoli.split(",");
                var ruolo1;
                var ruolo2;
                var ruolo3;
                var extension_warranty = "EXTENSION_WARRANTY";
                var demo_open = "DEMO_OPEN";
                var rca_gratuita = "RCA_GRATUITA";
                $(".testo-datiDealer #ruolo1").prop('checked', false);
                $(".testo-datiDealer #ruolo2").prop('checked', false);
                $(".testo-datiDealer #ruolo3").prop('checked', false);
                for(i=0; i<ruoli.length; i++){
                    if (ruoli[i].indexOf(extension_warranty)>-1){
                        ruolo1=ruoli[1];
                        $(".testo-datiDealer #ruolo1").prop('checked', true);
                    }else if (ruoli[i].indexOf(demo_open)>-1){
                        ruolo2=ruoli[i];
                        $(".testo-datiDealer #ruolo2").prop('checked', true);
                    }else if (ruoli[i].indexOf(rca_gratuita)>-1){
                        ruolo3=ruoli[i];
                        $(".testo-datiDealer #ruolo3").prop('checked', true);
                    }
                }
                $("#datiDealer").modal({
                    show:true
                });

                $(".testo-datiDealer #idDealer").val( idDealer );
                $(".testo-datiDealer #ragionesociale").val( ragionesociale );
                $(".testo-datiDealer #partitaiva").val(partitaiva );
                $(".testo-datiDealer #email").val(email );
                $(".testo-datiDealer #indirizzo").val(indirizzo );
                $(".testo-datiDealer #localita").val(localita);
                $(".testo-datiDealer #provincia").val(provincia);
                $(".testo-datiDealer #cap").val(cap);
                $(".testo-datiDealer #telefono").val(telefono);
            }
        }, "json");

    });


    function acettoValidate(){
        $.post( "${createLink(controller: "polizze", action: 'accettazione')}", {_csrf: "${request._csrf.token}"}, function() {
        });
    }
    $('.extension').change(function() {
        var extension=$(this).val();
        var idDealer=$(".testo-datiDealer #idDealer").val();
        if($(this).is(":checked")) {
            $(this).prop("checked", true);
            $.post("${createLink(controller: "user", action: 'aggiornaRuolo')}",{ id:idDealer, ruolo: extension, check:true, _csrf: "${request._csrf.token}"}, function(response) {
                var risposta=response.risposta;
                if(risposta=="aggiornata"){
                    swal({
                        title: "Il dealer è stato aggiornato!",
                        type: "success",
                        timer: "2000",
                        showConfirmButton: false
                    });
                }
            }, "json");
        }else{
            $.post("${createLink(controller: "user", action: 'aggiornaRuolo')}",{  id:idDealer, ruolo: extension, check:false, _csrf: "${request._csrf.token}"}, function(response) {
                var risposta=response.risposta;
                if(risposta=="aggiornata"){
                    swal({
                        title: "Il dealer è stato aggiornato!",
                        type: "success",
                        timer: "2000",
                        showConfirmButton: false
                    });
                }
            }, "json");
        }
    });
    $('.rca').change(function() {
        var rca=$(this).val();
        var idDealer=$(".testo-datiDealer #idDealer").val();
        if($(this).is(":checked")) {
            $(this).prop("checked", true);
            $.post("${createLink(controller: "user", action: 'aggiornaRuolo')}",{ id:idDealer, ruolo: rca, check:true, _csrf: "${request._csrf.token}"}, function(response) {
                var risposta=response.risposta;
                if(risposta=="aggiornata"){
                    swal({
                        title: "Il dealer è stato aggiornato!",
                        type: "success",
                        timer: "2000",
                        showConfirmButton: false
                    });
                }
            }, "json");
        }else{
            $.post("${createLink(controller: "user", action: 'aggiornaRuolo')}",{  id:idDealer, ruolo: rca, check:false, _csrf: "${request._csrf.token}"}, function(response) {
                var risposta=response.risposta;
                if(risposta=="aggiornata"){
                    swal({
                        title: "Il dealer è stato aggiornato!",
                        type: "success",
                        timer: "2000",
                        showConfirmButton: false
                    });
                }
            }, "json");
        }
    });
    $('.demo').change(function() {
        var demo=$(this).val();
        var idDealer=$(".testo-datiDealer #idDealer").val();
        if($(this).is(":checked")) {
            $(this).prop("checked", true);
            $.post("${createLink(controller: "user", action: 'aggiornaRuolo')}",{ id:idDealer, ruolo: demo, check:true, _csrf: "${request._csrf.token}"}, function(response) {
                var risposta=response.risposta;
                if(risposta=="aggiornata"){
                    swal({
                        title: "Il dealer è stato aggiornato!",
                        type: "success",
                        timer: "2000",
                        showConfirmButton: false
                    });
                }
            }, "json");
        }else{
            $.post("${createLink(controller: "user", action: 'aggiornaRuolo')}",{  id:idDealer, ruolo: demo, check:false, _csrf: "${request._csrf.token}"}, function(response) {
                var risposta=response.risposta;
                if(risposta=="aggiornata"){
                    swal({
                        title: "Il dealer è stato aggiornato!",
                        type: "success",
                        timer: "2000",
                        showConfirmButton: false
                    });
                }
            }, "json");
        }
    });

</script>
</body>
</html>