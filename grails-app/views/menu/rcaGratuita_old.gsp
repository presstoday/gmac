
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="navigazione"/>
    <title>RCA Gratuita</title>
    <script language="javascript">
        if(history.length > 0) history.forward()
    </script>
</head>

<body>
<div class="row">
    <div class="col-xs-12 col-sm-offset-3 col-sm-6">
        <div class="login-panel">
            <div class="login-heading page-header">
                <div class="row">
                    <div class="col-xs-9 text-left">
                        <h3>RCA GRATUITA</h3>
                        <p>Sito in lavorazione</p>
                    </div>
                    <div class="col-xs-3 text-right">
                        <i class="fa fa-cogs fa-spin fa-5x"></i>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
</body>
</html>