<html>
<head>
    <meta name="layout" content="navigazione"/>
    <title>Dati Dealer</title>
</head>
<body>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <h2 class="text-center">Dati Anagrafici </h2>
                %{--<fa:icon name="user-plus" large="5x"/>--}%
            </div>
            <div class="panel-body">
                <f:form>
                    <div class="col-md-12">
                    <sec:ifHasRole role="DEALER">
                        <div class="col-md-12"   role="alert">
                           <div> <i class="fa fa-hand-o-right fa-3x"></i><p class="testo-datiAnagrafici"> Verifica la correttezza dei dati. <br> Per segnalare eventuale modifiche mandare una mail a <ins>ewgmf@mach-1.it</ins></p></div>
                        </div>
                        <div class="col-md-6">
                            <label class="testoParagraph control-label">RAGIONE SOCIALE</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                <f:input control-class="col-sm-6" type="text" name="ragioneSociale"  value="${utente.ragioneSociale}" readonly="true"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="testoParagraph control-label">P. IVA</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-list-alt"></span></span>
                                <f:input control-class="col-sm-6" type="text" name="partitaiva"  value="${utente.piva}" readonly="true"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="testoParagraph control-label">Email</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-envelope"></span></span>
                                <f:input control-class="col-sm-6" type="text" name="email"  value="${utente.email}" readonly="true"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="testoParagraph control-label">Indirizzo</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-home"></span></span>
                                <f:input control-class="col-sm-6" type="text" name="indirizzo" readonly="true"  value="${utente.indirizzo}"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="testoParagraph control-label">Località</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-globe"></span></span>
                                <f:input control-class="col-sm-6" type="text" name="localita" readonly="true"  value="${utente.localita}"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="testoParagraph control-label">Provincia</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-globe"></span></span>
                                <f:input control-class="col-sm-6" type="text" name="provincia" readonly="true"  value="${utente.provincia}"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="testoParagraph control-label">Cap</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-inbox"></span></span>
                                <f:input control-class="col-sm-6" type="text" name="cap" readonly="true"  value="${utente.cap}"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="testoParagraph control-label">Telefono</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-phone"></span></span>
                                <f:input control-class="col-sm-6" type="text" name="telefono" readonly="true"  value="${utente.telefono}"/>
                            </div>
                        </div>
                    </div>
                    </sec:ifHasRole>
                    <sec:ifHasRole role="ADMIN">
                        <div class="col-md-6">
                            <label class="control-label">Nome</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-users"></span></span>
                                <f:input control-class="col-sm-6" type="text" name="nome"  value="${utente.nome}" readonly="true"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Cognome</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-users"></span></span>
                                <f:input control-class="col-sm-6" type="text" name="cognome"  value="${utente.cognome}" readonly="true"/>
                            </div>
                        </div>
                    </sec:ifHasRole>
                    <div class="col-md-4 col-xs-offset-9">
                        <br>
                        <g:link controller="menu" action="sceltaMenu"><div class="btn btn-yellow col-md-offset-2  col-md-6 col-xs-10"><b>Ok</b></div></g:link>
                        %{--<button type="submit" class="btn btn-yellow col-xs-offset-2 col-sm-6">Menu principale</button>--}%
                    </div>
                </f:form>
            </div>
        </div>
    </div>
</div>
</body>
</html>