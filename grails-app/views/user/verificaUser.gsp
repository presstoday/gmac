<html>
<head>
    <meta name="layout" content="main">
    <title>Registrazione</title>
    </head>

<body>

<div class="row">
    <div class="col-xs-12 col-sm-offset-3 col-sm-6">
        <div class="registered-only">
            <div class="login-heading page-header">
                <div class="row">
                    <div class="col-xs-12 col-md-12 text-center">
                        <asset:image src="loghi/opel2.png" height="80px" id="logo-mach1" style="margin-top: 2px; margin-right: 55px;"/>
                        <h3>Portale dedicato ai dealer OPEL</h3>
                    </div>

                </div>
            </div>
                <div class="login-body">
                    <g:renderFlash/>
                    <h4>Inserisci la tua Partita IVA e la password per l'accesso:</h4>
                    <f:form action="verificaUser" method="post">
                        <f:field control-class="col-md-6" type="text" name="partitaIva" required="true"/>
                        <f:field control-class="col-md-6" type="password"  bean="${dealer}" name="password" label="Password"  required="true"/>
                        <f:field control-class="col-md-6" type="password" name="password2" label="Verifica password" required="true"/>
                        <button type="submit" class="btn btn-warning col-xs-offset-2 col-xs-6">Registrami</button>
                    </f:form>
                </div>



    </div>
</div>
</div>
</body>
</html>