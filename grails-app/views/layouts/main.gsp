<!doctype html>
<html lang="en" class="no-js">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><g:layoutTitle default="Grails"/></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}" type="image/x-icon">
        <link rel="apple-touch-icon" href="${assetPath(src: 'apple-touch-icon.png')}">
        <link rel="apple-touch-icon" sizes="114x114" href="${assetPath(src: 'apple-touch-icon-retina.png')}">
        <%-- Styles --%>
        <asset:stylesheet src="gmac.css"/>
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/css/bootstrap-select.min.css"/>
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker3.min.css"/>

        <%-- Scripts --%>
        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <asset:javascript src="gmac.js"/>
        <script src="//cdnjs.cloudflare.com/ajax/libs/autosize.js/3.0.14/autosize.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/js/bootstrap-select.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/js/i18n/defaults-it_IT.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/locales/bootstrap-datepicker.it.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
        <%-- Autocomplete --%>
        <script src="//cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
        <g:layoutHead/>
    </head>
    <body>
        <div class="container">
            <g:layoutBody/>
        </div>
   <div class="container">
        <g:if test="${actionName == ("creaPolizzaExtension") || actionName == ("creaPolizzaRCA") || actionName == ("creaPolizzaDemo") ||actionName==("sceltaMenu") ||actionName==("extensionWarranty")||actionName==("rcaGratuita") ||actionName==("demoOpen") || actionName==("datiUtente")}">
            <footer class="footer">
        %{--  <div class="col-md-12 col-md-offset-5 col-xs-10 col-xs-offset-4">
             <p ><asset:image src="/loghi/logo_mach1_small.png" style="" /></p>
         </div>--}%
                 <div class="col-xs-12 col-md-12">
                     <p class="testofooter" align="center"><br>  Mach1 s.r.l. - Via Vittor Pisani, 13/B  – 20124 Milano - TEL. 02 30465068 FAX 02 62694254 - Email PEC: mach1@registerpec.it - CCIAA Milano - REA MI 1908726 - C.F.,  P. IVA e Reg. Imprese Milano 06680830962 - Capitale sociale 100.000 EURO - Registro Unico Intermediari  A000317603 - Banco Posta Sede di Milano, Piazza Cordusio, 1 - IBAN IT 54 C 07601 01600 0000 99701393</p>
                 </div>
            </footer>
        </g:if>
   </div>

        <script type="text/javascript">
            $('[data-toggle="tooltip"]').tooltip({html: true});
        </script>
    </body>
</html>
