<g:applyLayout name="main">
    
    <html>
    <head>
        <title><g:layoutTitle/></title>
        <g:layoutHead/>
            <style>
            .page-header h1 { display: inline-block; }
            </style>
    </head>
    <body>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div>
                <div class="bs-component">
                    <nav role="navigation" class="navbar navbar-default">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <asset:image src="/loghi/Opel_Financial_Services.png" style="height:60px; margin-top: 2px;" />
                                <asset:image src="/loghi/logo_Opel.png" style="height:60px; margin-top: 2px;" />
                            </div>
                            <!-- Collection of nav links, forms, and other content for toggling -->
                            <div id="navbarCollapse" class="navbar-collapse">
                                %{--<ul class="nav navbar-nav">
                                    <li><h3 class="testo-navBarTitolo">Convenzioni OPEL</h3></li>
                                </ul>--}%
                                <ul class="nav navbar-nav navbar-right">
                                    <li><a  href="${createLink(controller: "menu", action: "sceltaMenu")}" class="testo-navBar">Menu Prodotti</a></li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style=""> <sec:ifHasRole role="DEALER">00000</sec:ifHasRole><sec:loggedInUser/><span class="caret"></span></a>
                                        <ul class="dropdown-menu navbar-right" role="menu" >
                                            <sec:ifHasRole role="DEALER">
                                                <li><a href="${createLink(controller: "user", action: "datiUtente")}" id="profilo" >Profilo</a></li>
                                                <li class="divider"></li>
                                            </sec:ifHasRole>
                                       <li><g:link uri="/logout">Esci</g:link> </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <g:layoutBody/>
    </body>
    </html>
</g:applyLayout>