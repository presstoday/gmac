package gmac

import com.jcraft.jsch.ChannelSftp
import com.jcraft.jsch.JSch
import gmac.polizze.Polizza
import gmac.polizze.Telai
import gmac.utenti.Dealer
import gmac.utenti.Log
import grails.transaction.Transactional
import groovy.time.TimeCategory
import excel.reader.*
import java.text.DecimalFormat
import java.util.regex.Pattern
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

import static org.apache.poi.ss.usermodel.DateUtil.getJavaDate

@Transactional
class CaricaPolizzeJob {
    def tracciatiService
    def mailService
    static triggers = {
       // cron name: "CaricaPolizze", cronExpression: "0 0 10 * * ?"
    }
    def group = "CaricamentoPolizze"
    def description = "job per caricare le polizze"
    def execute() {
        try {
            downloadFile()

        } catch (Exception e) {
            log.error e.message
        }
    }
    def downloadFile() {
        def logg
        def host = "5.249.141.51"
        def port = 22
        def username = "flussi-nais"
        def password = "XYZrrr655"
        try {
            def jsch = new JSch()
            def session = jsch.getSession(username, host, port)
            def properties = new Properties()
            properties.put("StrictHostKeyChecking", "no")
            session.config = properties
            session.userName = username
            session.password = password
            session.connect()
            def channel = session.openChannel("sftp") as ChannelSftp
            channel.connect()
            logg =new Log(parametri: "Connected to $host:$port", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            println "Connected to $host:$port"
            channel.cd("GMAC GUASTI TEST/polizze")
            println "Current sftp directory: ${channel.pwd()}"
            logg =new Log(parametri: "Connected to $host:$port", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            def streamRiassunti = new ByteArrayOutputStream()
            def zipRiassunti = new ZipOutputStream(streamRiassunti)
            Vector<ChannelSftp.LsEntry> list = channel.ls("*.xls")
            def inserimento, fileNameRisposta, risposta, rispostaFinale
            def i=0
            if(list.size()>0){
                for(ChannelSftp.LsEntry entry : list) {
                    println "downloaded: ${entry.filename}"
                    logg =new Log(parametri: "downloaded: ${entry.filename}", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    InputStream stream =  channel.get(entry.filename)
                    inserimento =createPolizze(stream)
                    fileNameRisposta = "risposta_${entry.filename}${i}_${new Date().format("yyyyMMdd")}.txt"
                    risposta="Riassunto inserimento:\r\n"

                    inserimento.collect{ commento ->
                        if (commento.messaggio){
                            risposta =risposta +" "+ commento.codice +" "+ commento.messaggio+"\r\n"
                        }else if (commento.errore){
                            risposta =risposta +" "+ commento.codice +" "+ commento.errore+"\r\n"
                        }
                    }.grep().join("\n")
                    rispostaFinale=risposta
                    i++
                    zipRiassunti.putNextEntry(new ZipEntry(fileNameRisposta))
                    zipRiassunti.write(rispostaFinale.bytes)
                    channel.rename("/home/flussi-nais/GMAC GUASTI TEST/polizze/"+entry.filename,"/home/flussi-nais/GMAC GUASTI TEST/polizzeCaricate/"+entry.filename)
                    println "moved: ${entry.filename}"
                    logg =new Log(parametri: "moved: ${entry.filename}", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"

                }
                zipRiassunti.close()
                streamRiassunti.close()
                channel.put(new ByteArrayInputStream(streamRiassunti.toByteArray()), "riassunto_${new Date().format("yyyyMMdd")}.zip")
                logg =new Log(parametri: "riassunto_${new Date().format("yyyyMMdd")}", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                println "riassunto_${new Date().format("yyyyMMdd")}"
                println "Current sftp directory: ${channel.pwd()}"
                logg =new Log(parametri: "Current sftp directory: ${channel.pwd()}", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                /*mailService.sendMail {
                    to "priscila@presstoday.com"
                    subject "riassunto trasferimento polizze portale OPEL"
                    msg "<p style=\"font-family:sans-serif; font-size:14px;\">In allegato troverete l'elenco dei file delle polizze caricate il ${new Date().format("dd-MMM-yyyy")}.<p>\n"
                    attach streamRiassunti.toByteArray(), "riassunto_${new Date().format("dd-MM-yyyy")}.zip", "application/x-compressed-zip"
                }*/
            }
            channel.disconnect()
            session.disconnect()


        } catch(e) { e.printStackTrace() }

    }
    def createPolizze(InputStream file){
        def logg
        def response =[]
        boolean checkDataImmatr48 = false
        boolean checkDataImmatr96 = false
        boolean checkTarga=false
        Pattern exprRegTarga = ~/(^[a-zA-Z]{2}[0-9]{3,4}[a-zA-Z]{2}$)/
        def telaio, verificaPolizza, dealer,dataImmatricolazione,targa
        try {
            def wb = ExcelReader.readXls(file) {
                sheet {
                    rows(from: 1) {
                        def cellaCodice = cell("A").value
                        def cellTelaio = cell("B").value
                        def cellNome  = cell("C").value
                        def cellCognome = cell("D").value
                        def cellKm = cell("E").value
                        def cellTarga = cell("F").value
                        def cellData = cell("G").value
                        if(cellaCodice.toString().equals("null")){
                            cellaCodice="";
                        }else{
                            cellaCodice=cellaCodice.toString().replaceFirst ("^0*", "")
                            if(cellaCodice.toString().contains(".")){
                                def punto=cellaCodice.toString().indexOf(".")
                                cellaCodice=cellaCodice.toString().substring(0,punto).replaceAll("[^0-9]", "")
                            }
                            dealer=Dealer.findByUsername(cellaCodice.toString())
                        }
                        def  minimo = use(TimeCategory) { new Date() - 6.months }
                        if(cellTelaio != '' && cellTelaio.toString().trim()!= null && !(cellTelaio.toString().trim().contains("null"))){
                            cellTelaio=cellTelaio.toString().replaceAll("[^a-z,A-Z,0-9]","")
                            telaio=Telai.findByTelaio(cellTelaio.toString().trim())
                            if(telaio) {
                               // verificaPolizza=Polizza.findByTelaio(telaio)
                                verificaPolizza=  Polizza.where {
                                     telaio == telaio && dataDecorrenza > minimo
                                }
                                dataImmatricolazione=telaio.dataImmatricolazione
                                targa=telaio.targa
                                checkTarga=exprRegTarga.matcher(targa.toString().trim()).matches()
                                if(!checkTarga && cellTarga.toString().trim() !='' && exprRegTarga.matcher(cellTarga.toString().trim()).matches()){
                                    targa=cellTarga
                                    telaio.targa=cellTarga
                                    if(telaio.save(flush:true)){
                                        checkTarga=true
                                        response.add(codice:cellaCodice, messaggio:"la targa ${cellTarga} \u00E8 stata aggiornata per il telaio ${telaio.telaio}")
                                        logg =new Log(parametri: "la targa ${cellTarga} \u00E8 stata aggiornata per il telaio ${telaio.telaio}", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    }
                                    else{
                                        checkTarga=true
                                        logg =new Log(parametri: "non \u00E8 stato possibile aggiornare la targa ${cellTarga} per il telaio ${telaio.telaio}", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        response.add(codice:cellaCodice, errore:"non \u00E8 stato possibile aggiornare la targa ${cellTarga} per il telaio ${telaio.telaio}")
                                    }
                                }else targa=''
                            }
                        }else{
                            telaio=''
                        }

                        if(cellKm.toString().trim().equals("null")){
                            cellKm="";
                        }else if(cellKm.toString().contains(".")){
                            def punto=cellKm.toString().indexOf(".")
                            cellKm=cellKm.toString().substring(0,punto).replaceAll("[^0-9]", "")
                        }
                        if(dataImmatricolazione) {
                            checkDataImmatr48 = dataImmatricolazione >= use(TimeCategory) { new Date() - 3.years - 1.day }
                            checkDataImmatr96 = dataImmatricolazione <= use(TimeCategory) { new Date() - 8.years }
                        }
                        //def dataDecorrenza=use(TimeCategory){new Date() +30.days}
                        //def dataScadenza=use(TimeCategory){new Date() +1.year + 30.days}

                        boolean polizzaCreata =false
                        if(verificaPolizza){
                                polizzaCreata=true
                        }
                        else {
                            polizzaCreata=false
                        }

                        if(!polizzaCreata && checkTarga && !checkDataImmatr48 && !checkDataImmatr96 && telaio && dealer){
                            def polizza = Polizza.findOrCreateWhere(
                                    telaio: telaio,
                                    dealer: dealer,
                                    nome:  cellNome.toString().trim(),
                                    cognome: cellCognome.toString().trim(),
                                    km: Integer.parseInt(cellKm),
                                    dataTagliando: new Date(),
                                    dataDecorrenza: use(TimeCategory){new Date() +30.days},
                                    dataScadenza: use(TimeCategory){new Date() +30.days + 1.year}
                            )
                            if(polizza.save(flush:true)) {
                                logg =new Log(parametri: "polizza caricata", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                def tracciatoPolizza= tracciatiService.generaTracciatoIAssicur(polizza)
                                if(tracciatoPolizza.toString().contains("Errore nel")){
                                    logg =new Log(parametri: "generazione tracciato iassicur errore:${tracciatoPolizza.toString()}", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    response.add(codice:cellaCodice, errore:tracciatoPolizza.toString())
                                }else{
                                    def tracciatoCompagnia=tracciatiService.generaTracciatoCompagnia(polizza)
                                    if(tracciatoCompagnia.toString().contains("Errore nel")){
                                        logg =new Log(parametri: "generazione tracciato compagnia errore:${tracciatoCompagnia.toString()}", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        response.add(codice:cellaCodice, errore:tracciatoCompagnia.toString())
                                    }
                                    else{
                                        logg =new Log(parametri: "polizza creata correttamente ${polizza.noPolizza}", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        response.add(codice:cellaCodice, messaggio:"polizza creata correttamente ${polizza.noPolizza}")
                                    }
                                }
                            }
                            else {
                                logg =new Log(parametri: "Errore creazione polizza: ${polizza.errors}", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                response.add(codice:cellaCodice, errore:"Errore creazione polizza: ${polizza.errors}")
                            }
                        }else if(!telaio){
                            logg =new Log(parametri: "Errore creazione polizza: il telaio non \u00E8 presente", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            response.add(codice:cellaCodice, errore:"Errore creazione polizza: il telaio non \u00E8 presente")
                        }else if(polizzaCreata){
                            logg =new Log(parametri: "Errore creazione polizza: essiste gi\u00E0 una polizza negli ultimi 6 mesi", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            response.add(codice:cellaCodice, errore:"Errore creazione polizza: essiste gi\u00E0 una polizza negli ultimi 6 mesi")
                        }else if(!checkTarga){
                            logg =new Log(parametri: "Errore creazione polizza: verificare il formato della targa", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            response.add(codice:cellaCodice, errore:"Errore creazione polizza: verificare il formato della targa")
                        }else if(checkDataImmatr48){
                            logg =new Log(parametri: "Errore creazione polizza: la data immatricolazione \u00E8 inferiore a 4 anni", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            response.add(codice:cellaCodice, errore:"Errore creazione polizza: la data immatricolazione \u00E8 inferiore a 4 anni")
                        }else if(checkDataImmatr96){
                            logg =new Log(parametri: "Errore creazione polizza: la data immatricolazione \u00E8 inferiore a 8 anni", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            response.add(codice:cellaCodice, errore:"Errore creazione polizza: la data immatricolazione  \u00E8 superiore a 8 anni")
                        }else if(!dealer){
                            logg =new Log(parametri: "Errore creazione polizza: il dealer non \u00E8 presente", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            response.add(codice:cellaCodice, errore:"Errore creazione polizza: il dealer non \u00E8 presente")
                        }

                    }
                }
            }
        }catch (e){
            logg =new Log(parametri: "c'\u00E8 un problema con il file, ${file}--> ${e.toString()}", operazione: " caricamento polizza", pagina: "CARICA POLIZZE JOB")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            println "c'\u00E8 un problema con il file, ${file} verificare che non ci siano righe con valori nulli"
            response.add(codice:null, errore:"c'\u00E8 un problema con il file, ${file} verificare che non ci siano righe con valori nulli")
        }

        return response
    }

}
