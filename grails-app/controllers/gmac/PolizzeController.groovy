package gmac

import com.itextpdf.text.BaseColor
import com.itextpdf.text.Element
import com.itextpdf.text.Font
import com.itextpdf.text.Phrase
import com.itextpdf.text.pdf.BaseFont
import com.itextpdf.text.pdf.ColumnText
import com.itextpdf.text.pdf.PdfContentByte
import com.itextpdf.text.pdf.PdfGState
import com.itextpdf.text.pdf.PdfPageEventHelper
import com.itextpdf.text.pdf.PdfStamper
import com.presstoday.groovy.Stopwatch
import excel.builder.ExcelBuilder
import excel.reader.ExcelReader
import gmac.polizze.*
import gmac.polizze.Telai
import gmac.utenti.Admin
import gmac.utenti.Dealer
import gmac.utenti.Log
import grails.converters.JSON
import org.apache.poi.hssf.util.HSSFColor
import org.apache.poi.ss.usermodel.CellStyle
import org.apache.poi.ss.usermodel.IndexedColors
import org.springframework.web.context.support.WebApplicationContextUtils
import security.SecurityUtils
import com.itextpdf.text.pdf.PdfReader
import java.text.SimpleDateFormat
import groovy.time.TimeCategory as TC

import java.util.regex.Pattern


class PolizzeController {
    def tracciatiService
    def mailService
    def index() {}

    def creaPolizzaExtension(){
        def principal = SecurityUtils.principal
        def utente=Dealer.get(principal.id)
        def polizza = new Polizza()
        def logg
        if(request.post){
            def targaForm =params.targa
            def telaioForm=params.telaio.id
            def telaio=Telai.get(telaioForm)

           if(telaio.targa.toString().trim() != targaForm.toString().trim()){
                telaio.targa=targaForm.toString().toUpperCase()
                if(telaio.save(flush:true)){
                    bindData(polizza, params, params, [exclude :['tipoPolizza']])
                    polizza.tipoPolizza=TipoPolizza.EXTENSION
                }
               else{
                    logg =new Log(parametri: "Errore nell'aggiornamento del telaio ${renderErrors(bean: telaio)}", operazione: "modifica telaio", pagina: "crea polizza Extension")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    flash.error  = "Errore nell'aggiornamento del telaio ${renderErrors(bean: telaio)}"
                }
            }else{
               bindData(polizza, params, [exclude :['tipoPolizza']])
               polizza.tipoPolizza=TipoPolizza.EXTENSION
           }
            if(polizza.save(flush:true)){
                def tracciatoPolizza= tracciatiService.generaTracciatoIAssicur(polizza)
                if(!tracciatoPolizza.save(flush: true)){
                    logg =new Log(parametri: "Errore nella generazione del tracciato ${tracciatoPolizza.toString()}", operazione: "genera tracciato IAssicur", pagina: "crea polizza Extension")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    flash.error=tracciatoPolizza.toString()
                }else{
                    def tracciatoCompagnia=tracciatiService.generaTracciatoCompagnia(polizza)
                    if(!tracciatoCompagnia.save(flush: true)){
                        logg =new Log(parametri: "Errore nella generazione del tracciato ${tracciatoPolizza.toString()}", operazione: "genera tracciato IAssicur", pagina: "crea polizza Extension")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        flash.error=tracciatoCompagnia.toString()
                    }
                    else{
                        flash.message="polizza salvata"
                        logg =new Log(parametri: "polizza salvata ${polizza.id}", operazione: "Salva polizza", pagina: "crea polizza Extension")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    }
                }
                redirect controller: "menu", action: "extensionWarranty"
            }
            else{
                logg =new Log(parametri: "Errore nel salvataggio della polizza ${renderErrors(bean: polizza)}", operazione: "Salva polizza", pagina: "crea polizza Extension")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                flash.error = "Errore nel salvataggio della polizza ${renderErrors(bean: polizza)}"
                println "Errore caricamento polizza: ${polizza.errors}"
            }
        }
        [utente:utente, limite: Telai.count(), polizza: polizza]

    }

    def creaPolizzaDEMO(){
        def marche=Marche.list().marca
        def principal = SecurityUtils.principal
        def utente=Dealer.get(principal.id)
        def polizza = new Polizza(tipoPolizza: TipoPolizza.DEMO_OPEL)
        if(request.post){
            def dealer=Dealer.get(Long.parseLong(params.dealer))
            def idDealer=dealer.id
            bindData(polizza, params, [exclude :['dataScadenza']])
            def dataCorrente=new Date()
            GregorianCalendar calendar = new GregorianCalendar()
            def yearInt = Integer.parseInt(dataCorrente.format("yyyy"))
            def monthInt = 11
            calendar.set(yearInt, monthInt, 31)
            //def dayInt = calendar.getActualMaximum(GregorianCalendar.DAY_OF_MONTH)
            def dataScadenza=new GregorianCalendar(yearInt,monthInt,31).format("dd-MM-yyyy")
            Date dataFormatata = Date.parse( "dd-MM-yyyy", dataScadenza )
            polizza.dataScadenza=dataFormatata
            boolean risposta=false
            def logg
            if(polizza.save(flush:true)){
                /*def tracciatoPolizza= tracciatiService.generaTracciatoIAssicur(polizza)
                if(!tracciatoPolizza.save(flush: true)){
                    flash.error=tracciatoPolizza.toString()
                }else{
                    def tracciatoCompagnia=tracciatiService.generaTracciatoCompagnia(polizza)
                    if(!tracciatoCompagnia.save(flush: true)){
                        flash.error=tracciatoCompagnia.toString()
                    }
                    else{*/
                if(params.aggiungi=="true"){
                    def file =params.fileLibretto
                    def filename=""
                    if(file.bytes) {
                        def libretto=new Documenti()
                        libretto.fileName=file.originalFilename?:null
                        libretto.fileContent=file.bytes?:null
                        libretto.dealer=dealer
                        libretto.tipo= TipoDocumento.LIBRETTO
                        libretto.targa=polizza.targa.toUpperCase()
                        if(libretto.save(flush:true)){
                            logg =new Log(parametri: "polizza salvata ${polizza.id}", operazione: "aggiunge polizza", pagina: "aggiungi Polizza")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            flash.message="polizza salvata"
                            //risposta=true
                           // def stringaresponse=[risposta:risposta]
                           // render stringaresponse as JSON
                        }else{
                            logg =new Log(parametri: "errore caricamento libretto ${renderErrors(bean: libretto)} polizza: ${polizza.id}", operazione: "salvataggio libretto", pagina: "aggiungi Polizza")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            flash.error="errore caricamento libretto ${renderErrors(bean: libretto)}"
                        }
                    }else {
                        logg =new Log(parametri: "Specificare il file contenente il libretto polizza: ${polizza.id}", operazione: "salvataggio polizza", pagina: "aggiungi Polizza")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        flash.error = "Specificare il file contenente il libretto"}
                    redirect controller: "polizze", action:"creaPolizzaDEMO"
                }else{
                    def file =params.fileLibretto
                    if(file.bytes) {
                        def libretto=new Documenti()
                        libretto.fileName=file.originalFilename?:null
                        libretto.fileContent=file.bytes?:null
                        libretto.dealer=dealer
                        libretto.tipo=TipoDocumento.LIBRETTO
                        libretto.targa=polizza.targa.toUpperCase()
                        if(libretto.save(flush:true)){
                            def invioLibretti=mailService.invioMailLibretto(idDealer)
                            if(invioLibretti.contains("queued")||invioLibretti.contains("sent") ||invioLibretti.contains("scheduled") ){
                                logg = new Log(parametri: "la mail con i libretti caricati \u00E8 stata inviata ${invioLibretti}", operazione: "invio libretti caricati", pagina: "Salva e chiudi")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                flash.message="polizza salvata"
                                flash.success="Richiesta inviata a Mach1"
                            }else{
                                logg =new Log(parametri: "Non \u00E8 stato possibile inviare la mail dei libretti caricati", operazione: "invio libretti caricati", pagina: "Salva e chiudi")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                flash.error="Non \u00E8 stato possibile inviare la mail dei libretti caricati\n"
                                println "Non \u00E8 stato possibile inviare la mail dei libretti caricati \n ${invioLibretti}"
                            }
                        }else{
                            logg =new Log(parametri: "errore caricamento libretto ${renderErrors(bean: libretto)} polizza: ${polizza.id}", operazione: "salvataggio libretto", pagina: "Salva e chiudi")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            flash.error="errore caricamento libretto ${renderErrors(bean: libretto)}"
                        }
                    }else {
                        logg =new Log(parametri: "Specificare il file contenente il libretto polizza: ${polizza.id}", operazione: "salvataggio polizza", pagina: "Salva e chiudi")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        flash.error = "Specificare il file contenente il libretto"
                    }
                    redirect controller: "menu", action: "demoOpen"
                }
                /*  }
              }*/
            }
            else{
                logg =new Log(parametri: "Errore caricamento polizza: ${polizza.errors}", operazione: "salvataggio polizza", pagina: "Salva e chiudi")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                flash.error = "Errore nel salvataggio della polizza ${renderErrors(bean: polizza)}"
                println "Errore caricamento polizza: ${polizza.errors}"
            }
        }
            [utente:utente, polizza: polizza, marche:marche]

    }
    def accettazione() {
        def principal = SecurityUtils.principal
        def dealer
        def logg
        if (SecurityUtils.hasRole("DEALER")) {
            dealer = Dealer.get(principal.id)
            dealer.accettazione = true
            if (dealer.save(flush: true)) {
                logg =new Log(parametri: "il dealer ha accettato le condizioni", operazione: "accettazione ", pagina: "Extension Warranty")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                redirect controller: "menu", action: "sceltaMenu"
            } else {
                println dealer.errors
            }
        }
    }
    def fascicoloInformativoGuasti(){
        //def springContext = WebApplicationContextUtils.getWebApplicationContext(servletContext)
        def src = this.class.getResource("/pdf/fascicoloInformativoGuasti.pdf").file
        src = src.replaceAll("%23", "#")
        if (src) {
            def stream = new FileInputStream(src)
            PdfReader reader = new PdfReader(stream)
            def (canvas, output, writer) = createCanvasAndStreams(reader, 1)
            writer.close()
            reader.close()
            response.contentType = "application/pdf"
            response.addHeader "Content-disposition", "inline; filename=fascicoloInformativoGuasti.pdf"
            response.outputStream << output.toByteArray()
        }


    }
    def leaflet_polizza(String id){
        def polizza=Polizza.get(id)
        def src = this.class.getResource("/pdf/leaflet.pdf").file
        src = src.replaceAll("%23", "#")
        if (src) {
            def stream = new FileInputStream(src)
            PdfReader reader = new PdfReader(stream)
            def (canvas, output, writer) = createCanvasAndStreams(reader, 3)
            canvas.saveState()
            writeText(canvas, 493, 318, polizza.telaio.targa, 15, "grigio","bold")
            writeText(canvas, 490, 270, polizza.dataTagliando.format("dd/MM/yyy"), 15, "grigio","bold")
            writeText(canvas, 480, 225, polizza.km.toString(), 16, "grigio","bold")
            canvas = writer.getOverContent(2)
            writer.close()
            reader.close()
            response.contentType = "application/pdf"
            response.addHeader "Content-disposition", "inline; filename=leaflet.pdf"
            response.outputStream << output.toByteArray()
        }
    }
    def guidautilizzoPortale(){

        def src = this.class.getResource("/pdf/guidautilizzoPortale.pdf").file
        src = src.replaceAll("%23", "#")
        if (src) {
            def stream = new FileInputStream(src)
            PdfReader reader = new PdfReader(stream)
            def (canvas, output, writer) = createCanvasAndStreams(reader, 1)
            /*canvas.saveState()
            writeText(canvas, 400, 350, "ok questo e il primo", 10, "nero","normal")
            writeText(canvas, 340, 765, "ok questo e il primo", 11, "nero","normal")
            writeText(canvas, 375, 701, "Riparatore", 10, "nero","normal")
            canvas = writer.getOverContent(2)*/
            writer.close()
            reader.close()
            response.contentType = "application/pdf"
            response.addHeader "Content-disposition", "inline; filename=guidautilizzoPortale.pdf"
            response.outputStream << output.toByteArray()
        }


    }
    def proceduraSinistri(){
        def src = this.class.getResource("/pdf/proceduraSinistri.pdf").file
        src = src.replaceAll("%23", "#")
        if (src) {
            def stream = new FileInputStream(src)
            PdfReader reader = new PdfReader(stream)
            def (canvas, output, writer) = createCanvasAndStreams(reader, 1)
            /*canvas.saveState()
            writeText(canvas, 400, 350, "ok questo e il primo", 10, "nero","normal")
            writeText(canvas, 340, 765, "ok questo e il primo", 11, "nero","normal")
            writeText(canvas, 375, 701, "Riparatore", 10, "nero","normal")
            canvas = writer.getOverContent(2)*/
            writer.close()
            reader.close()
            response.contentType = "application/pdf"
            response.addHeader "Content-disposition", "inline; filename=proceduraSinistri.pdf"
            response.outputStream << output.toByteArray()
        }


    }
    def writeText(canvas, x, y, String text, fontSize, color, font){
        def fontsB = [
                "normal": BaseFont.HELVETICA,
                "bold": BaseFont.HELVETICA_BOLD
        ]
        def carattere = BaseFont.createFont(fontsB[font], BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        canvas.beginText()
        canvas.moveText(x as float,y as float)
        canvas.setFontAndSize(carattere, fontSize as float)
        if(color == "rosso") canvas.setRGBColorFill(192, 0, 0)
        else if (color == "bianco") canvas.setRGBColorFill(255, 255, 255)
        else if (color == "grigio") canvas.setRGBColorFill(147, 147, 147)
        else canvas.setRGBColorFill(0, 0, 0)
        canvas.showText(text)
        canvas.endText()
    }
    def createCanvasAndStreams(document, page) {
        def output = new ByteArrayOutputStream()
        def writer = new PdfStamper(document, output)
        PdfContentByte canvas = writer.getOverContent(page)
        return [canvas, output, writer]
    }
    def getTelai(String elencoTelaio){
        def telaiotrovato
        if(elencoTelaio) {
            telaiotrovato =  Telai.findAllByTelaioIlike("%${elencoTelaio}%")
         }
        if(telaiotrovato){
            render telaiotrovato as JSON
        }else{
            telaiotrovato=[Telai:0]
            render telaiotrovato as JSON
        }

    }
    def datiTelai(String id) {
        def datiTelai
        def telai
        def checkDataImmatr48 = false
        def checkDataImmatr96 = false
        def checkTarga=false
        Pattern exprRegTarga = ~/(^[a-zA-Z]{2}[0-9]{3,4}[a-zA-Z]{2}$)/
        //def exprRegTarga=/(^[a-zA-Z]{2}+[0-9]{3,4}+[a-zA-Z]{2}$)/
        if(request.post) {
            if(id!=0){
                telai= Telai.findById(id)
                checkDataImmatr48 = telai.dataImmatricolazione >= use(TC) { new Date() - 3.years - 1.day }
                checkDataImmatr96 = telai.dataImmatricolazione <= use(TC) { new Date() - 8.years }
                checkTarga=exprRegTarga.matcher(telai.targa).matches()
                datiTelai = [id:telai.id, targa:telai.targa, modello: telai.modello, dataImmatricolazione:telai.dataImmatricolazione,checkTarga:checkTarga, checkDataImmatr48: checkDataImmatr48, checkDataImmatr96:checkDataImmatr96]
            }else{
                datiTelai = [id:0,targa:"", modello: "", dataImmatricolazione:"",checkTarga:checkTarga, checkDataImmatr48: checkDataImmatr48, checkDataImmatr96:checkDataImmatr96]
            }
            render datiTelai as JSON
        }
        else response.sendError(404)
    }
    def annullaPolizza(String id, String check){
        def polizza= Polizza.get(id)
        def risposta
        def logg
        if(check=="true"){
            polizza.annullata=true
            polizza.tracciato.annullata=true
            polizza.tracciatoCompagnia.annullata=true
        }else{
            polizza.annullata=false
            polizza.tracciato.annullata=false
            polizza.tracciatoCompagnia.annullata=false
        }
        if(polizza.save(flush:true)){
            logg =new Log(parametri: "polizza aggiornata valore annullata: ${polizza.annullata}", operazione: "annullaPolizza ", pagina: "Extension Warranty")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            risposta=[risposta:"aggiornata"]
            render risposta as JSON
        }
        else {
            logg =new Log(parametri: "Errore nel salvataggio della polizza", operazione: "annullaPolizza ", pagina: "Extension Warranty")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            risposta =[risposta: "Errore nel salvataggio della polizza ${renderErrors(bean: polizza)}"]
            println "Errore caricamento polizza: ${polizza.errors}"
            render risposta as JSON
        }

    }
    def getPolizza(String noTelaio){
        def telaio=Telai.findByTelaio(noTelaio)
        def polizza= Polizza.findByTelaio(telaio)
        def risposta
        if(polizza){
            def today = new Date(), minimo = use(TC) { today - 5.months }
            if(polizza.dataDecorrenza <= minimo) {
                risposta=[risposta:true]
            }else{
                risposta=[risposta:false]
            }
        }else{risposta=[risposta:"nonEsistenessuna"]}
        render risposta as JSON
    }
    def getDatiPolizza(String id){
        def polizza= Polizza.get(id)
        def risposta
        if(polizza){
            risposta=[id:polizza.id, nome:polizza.nome, cognome:polizza.cognome, noPolizza: polizza.noPolizza, telaio:polizza.telaio.telaio, km:polizza.km, dataImmatricolazione: polizza.telaio.dataImmatricolazione.format("dd/MM/yyyy"), dataTagliando:polizza.dataTagliando.format("dd/MM/yyyy"), targa: polizza.telaio.targa, dataDecorrenza:polizza.dataDecorrenza.format("dd/MM/yyyy"), modello:polizza.telaio.modello, trasmissione:polizza.telaio.trasmissione,versione:polizza.telaio.versione,cilindrata:polizza.telaio.cilindrata]

        }else{risposta=[risposta:"errore"]}
        render risposta as JSON
    }
    def generaFilePolizzeattivate(){
        def polizze=Polizza.createCriteria().list(){ }
        def wb = Stopwatch.log { ExcelBuilder.create {
            style("header") {
                background bisque
                font {
                    bold(true)
                }
            }
            sheet("Polizze attivate") {
                row(style: "header") {
                    cell("Dealer")
                    cell("Numero")
                    cell("Modello")
                    cell("Targa")
                    cell("Telaio")
                    cell("Km")
                    cell("Data Immatricolazione")
                    cell("Data Tagliando")
                    cell("Data Decorrenza")
                    cell("Data Scadenza")
                    cell("Annullata")
                    cell("Tracciato IAssicur")
                    cell("Tracciato Compagnia")
                }
                polizze.each { polizza ->
                    row {
                        cell(polizza.dealer.ragioneSociale)
                        cell(polizza.noPolizza)
                        cell(polizza.telaio.modello)
                        cell(polizza.telaio.targa)
                        cell(polizza.telaio.telaio)
                        cell(polizza.km.format("######"))
                        cell(polizza.telaio.dataImmatricolazione)
                        cell(polizza.dataTagliando)
                        cell(polizza.dataDecorrenza)
                        cell(polizza.dataScadenza)
                        if(polizza.annullata==true){cell("SI")}
                        else cell("NO")
                        cell(polizza.tracciato)
                        cell(polizza.tracciatoCompagnia)
                    }
                    polizza.discard()
                }
                for (int i = 0; i < 12; i++) {
                    sheet.autoSizeColumn(i);
                }
            }

        } }

        def stream = new ByteArrayOutputStream()
        stream.write(wb)
        stream.close()
        response.contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        response.addHeader "Content-disposition", "inline; filename=polizze_${new Date().format("ddMMyyyy")}.xlsx"
        response.outputStream << stream.toByteArray()
    }
    def generaTracciati(long id){
        if(Polizza.exists(id)) {
            def polizza=Polizza.get(id)
            def tracciatoPolizza, tracciatoCompagnia
            if(!polizza.tracciato){
                tracciatoPolizza= tracciatiService.generaTracciatoIAssicur(polizza)
                if(!tracciatoPolizza.save(flush: true)){
                    flash.error = renderErrors(bean: tracciatoPolizza)
                }
            }
            if(!polizza.tracciatoCompagnia){
                tracciatoCompagnia= tracciatiService.generaTracciatoCompagnia(polizza)
                if(!tracciatoCompagnia.save(flush: true)){
                    flash.error = renderErrors(bean: tracciatoCompagnia)
                }
                /*else {
                    //flash.message="Tracciati generati"
                     response.with {
                          contentType = "application/octet-stream"
                          setHeader("Content-disposition", "attachment; filename=tracciatopolizza_${polizza.noPolizza}.txt")
                          outputStream << tracciatoPolizza + tracciatoCompagnia
                      }
                }*/
            }
        }
        redirect controller: "menu", action: "extensionWarranty"
    }
    def escludiPolizza(){
        def id=Long.parseLong(params.idPolizza)
        boolean risposta=false
        def strresponse=""
        def errore=[]
        def logg
        if(Polizza.exists(id)) {
            def dataEsclusione=use(TC) {Date.parse("dd-MM-yyyy", params.dataEsclusi)}
            def flussoIassicur, flussoDL
            def polizza = Polizza.get(id)
            polizza.annullata=true
            polizza.dataEsclusione=dataEsclusione
            if (!polizza.save(flush: true)) {
                risposta=false
                flash.error = "Si è verificato un errore durante l'esclusione della polizza"
                log.error renderErrors(bean: polizza)
                logg =new Log(parametri: "Si è verificato un errore durante l'esclusione della polizza: ${polizza.id}", operazione: "esclude polizza", pagina: "Polizze DEMO")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            }else{
                def inviaMialRichiestaEsclusione=mailService.invioMailRichiestaEsclusione(polizza.id)
                if(inviaMialRichiestaEsclusione.contains("queued")||inviaMialRichiestaEsclusione.contains("sent") ||inviaMialRichiestaEsclusione.contains("scheduled") ){
                    logg = new Log(parametri: "la mail con la richiesta esclusione della polizza ${polizza.id} \u00E8 stata inviata ${inviaMialRichiestaEsclusione}", operazione: "invio richiesta esclusione", pagina: "Demo Open")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    risposta=true
                    flash.info ="Polizza esclusa"
                    logg = new Log(parametri: "polizza: ${polizza.id} esclusa", operazione: "esclude polizza", pagina: "Polizze DEMO")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                }else{
                    logg =new Log(parametri: "Non \u00E8 stato possibile inviare la mail di richiesta esclusione per la polizza ${polizza.id}", operazione: "invio richiesta esclusione", pagina: "Demo Open")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    flash.error="Non \u00E8 stato possibile inviare la mail di richiesta esclusione per la polizza ${polizza.id}\n"
                    println "Non \u00E8 stato possibile inviare la mail di richiesta esclusione per la polizza \n ${inviaMialRichiestaEsclusione}"
                }
                /*flussoIassicur = tracciatiService.generaTracciatoIAssicur(polizza)
                if (!flussoIassicur.save(flush: true)) {
                    risposta=false
                    flash.error = "Si è verificato un errore durante la generazione del flusso iAssicur"
                    log.error renderErrors(bean: flussoIassicur)
                    redirect action: "lista"
                }else{

                    flussoDL = tracciatiService.generaTracciatoCompagnia(polizza)
                    if (!flussoDL.save(flush: true)) {
                        flash.error = "Si è verificato un errore durante la generazione del flusso Direct Line "
                        log.error renderErrors(bean: flussoDL)
                        redirect action: "lista"
                    }else{*/

                   /* }
                }*/
            }
        }
        if(risposta) strresponse=[risposta:risposta]
        else strresponse=[risposta:risposta,errore:errore]
        errore=[]
        render strresponse as JSON
    }
    def esclusaAdmin(String id, String check){
        def polizza= Polizza.get(id)
        def risposta
        def log
        if(check=="true"){
            polizza.esclusa=true
        }else{
            polizza.esclusa=false
        }
        if(polizza.save(flush:true)){
            log = new Log(parametri: "polizza: ${polizza.id} esclusa dal Admin", operazione: "esclusione polizza da Admin", pagina: "Polizze DEMO")
            if(!log.save(flush: true)) println "Errori salvataggio log reponse: ${log.errors}"
            risposta=[risposta:"aggiornata"]
            render risposta as JSON
        }
        else {
            log =new Log(parametri: "Errore nel salvataggio della polizza ${renderErrors(bean: polizza)}", operazione: "esclusione polizza da Admin", pagina: "Polizze DEMO")
            if(!log.save(flush: true)) println "Errori salvataggio log reponse: ${log.errors}"
            risposta =[risposta: "Errore nel salvataggio della polizza ${renderErrors(bean: polizza)}"]
            println "Errore caricamento polizza: ${polizza.errors}"
            render risposta as JSON
        }

    }
    def annullaInserimento(){
        boolean risposta=false
        def stringaresponse=""
        def logg
        def dealer=Dealer.get(Long.parseLong(params.dealer))
        def idDealer=dealer.id
        def invioLibretti=mailService.invioMailLibretto(idDealer)
        if(invioLibretti.contains("queued")||invioLibretti.contains("sent") ||invioLibretti.contains("scheduled") ){
            risposta=true
            logg = new Log(parametri: "la mail con i libretti caricati \u00E8 stata inviata ${invioLibretti}", operazione: "invio libretti caricati", pagina: "Salva e chiudi")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            flash.message="polizza salvata"
            flash.success="Richiesta inviata a Mach1"
        }else if(invioLibretti.contains("non ci sono libretti da inviare")){
            risposta=true
            logg =new Log(parametri: "Non  ci sono libretti da inviare", operazione: "invio libretti caricat annullai", pagina: "Annulla")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            println "Non ci sono libretti da inviare \n"
        }
        else{
            risposta=false
            logg =new Log(parametri: "Non \u00E8 stato possibile inviare la mail dei libretti caricati", operazione: "invio libretti caricati", pagina: "Salva e chiudi")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            flash.error="Non \u00E8 stato possibile inviare la mail dei libretti caricati\n"
            println "Non \u00E8 stato possibile inviare la mail dei libretti caricati \n ${invioLibretti}"
        }
        if(risposta) stringaresponse=[risposta:risposta]
        else stringaresponse=[risposta:risposta]
        render stringaresponse as JSON
    }

}
