package gmac

import gmac.polizze.Polizza
import gmac.utenti.Dealer
import gmac.utenti.Utente
import gmac.utenti.*
import grails.converters.JSON
import mandrill.*
import security.SecurityUtils

class UserController {


    def mailService

    def index() {}
    def verificaUser(){
        def dealer


        if(request.post) {
            if (params){
                def pwd1 = params.password, pwd2 = params.password2, registrami=params.registrami, partitaiva=partitaIva
                dealer= Dealer.findByPiva(partitaiva)
                if((pwd1 == pwd2) && (pwd1!='' || pwd2!='')) {
                    if(dealer){
                        if(dealer.enabled){
                                flash.error = "Il codice e' stato registrato in precedenza. In caso di problemi contattare MACH1. <br> <a href=\"${createLink(controller: 'menu', action: 'sceltaMenu')}\" style='font-size:medium;'>Tornare alla pagina di login</a>"
                        }else{
                            /*def username=dealer.username
                            dealer.username=username
                            dealer.password = pwd1
                            def email=dealer.email
                            dealer.enabled=true
                            if(dealer.save(flush: true)) {
                                def success, statoInvioCliente
                                def invioMail =mailService.invioMail(email,dealer.ragioneSociale,username,pwd1)
                                if (invioMail){
                                    flash.success ="Utenza registrata! <br>Una mail con le seguente credenziali: <br> codice: ${username} <br> password: ${pwd1} <br>  e' stata inviata al seguente indirizzo ${email}"
                                    *//*flash.messagge="${invioMail}"*//*
                                    redirect uri: "/login"
                                    return

                                }
                                else{
                                    flash.error="Errore nell'invio della email al dealer"
                                }
                                redirect uri: "/"
                            }
                            else {
                                flash.error = renderErrors(bean: dealer)
                                println dealer.errors
                            }*/
                        }
                    }else{
                        //def user = new Dealer()
                        flash.error = "La partita iva non risulta registrata. In caso di problemi contattare "
                    }
                } else {
                    flash.error = "Le password non corrispondono"
                }

            }else{

            }

        }else{

        }
        [dealer:dealer]
        //render view: "verificaUser"
    }

    def datiUtente() {
        def principal = SecurityUtils.principal
        def dealer, utente, admin
        //utente=Utente.get(principal.id)
        if(SecurityUtils.hasRole("ADMIN")){
            admin=Admin.get(principal.id)
            utente=admin
        }
        if(SecurityUtils.hasRole("DEALER")){
            dealer=Dealer.get(principal.id)
            utente=dealer
        }

        if(request.post) {
            if(SecurityUtils.hasRole("ADMIN")){
                redirect controller: "menu", action: "sceltaMenu"
            }else if(SecurityUtils.hasRole("DEALER")){
                dealer=Dealer.get(principal.id)
                dealer.attivo=true
                if(dealer.save(flush: true)) {
                    //render (view:"nu/sceltaMenu", model: [utente: dealer] )
                    redirect uri:  "/", model: [utente: utente]
                }else{
                    println dealer.errors
                }
            }
        }
        [utente:utente]
    }

    def datiDealer(String id){
        def dealer= Dealer.get(id)
        def risposta
        if(dealer){
            risposta=[id:dealer.id, ragionesociale:dealer.ragioneSociale, partitaiva:dealer.piva, ruoli: dealer.ruoli.toString(), email:dealer.email, indirizzo:dealer.indirizzo, localita: dealer.localita, provincia:dealer.provincia,cap:dealer.cap, telefono:dealer.telefono]

        }else{risposta=[risposta:"errore"]}
        render risposta as JSON
    }
    def aggiornaRuolo(String id, String ruolo, String check){
        def dealer= Dealer.get(id)
        def risposta
        //println "id ${id},  ruolo ${ruolo}, check ${check} "
        if(check=="true" && ruolo=="extension_warranty"){
            dealer.addRuolo(Ruolo.EXTENSION_WARRANTY)
        }else if(check=="true" && ruolo=="demo_open"){
            dealer.addRuolo(Ruolo.DEMO_OPEN)
        }else if(check=="true" && ruolo=="rca_gratuita"){
            dealer.addRuolo(Ruolo.RCA_GRATUITA)
        }else if(check=="false" && ruolo=="rca_gratuita"){
            dealer.removeRuolo(Ruolo.RCA_GRATUITA)
        }else if(check=="false" && ruolo=="demo_open"){
            dealer.removeRuolo(Ruolo.DEMO_OPEN)
        }else if(check=="false" && ruolo=="extension_warranty"){
            dealer.removeRuolo(Ruolo.EXTENSION_WARRANTY)
        }
        if(dealer.save(flush:true)){
            risposta=[risposta:"aggiornata"]
            render risposta as JSON
        }
        else {
            risposta =[risposta: "Errore nel aggiornamento del dealera ${renderErrors(bean: dealer)}"]
            println "Errore caricamento polizza: ${dealer.errors}"
            render risposta as JSON
        }
    }

}
