package gmac

import gmac.polizze.Polizza
import gmac.polizze.TipoPolizza
import gmac.utenti.*
import security.SecurityUtils

class MenuController {

    def index() {

    }
    def sceltaMenu(){
        def principal = SecurityUtils.principal
        def utente,dealer,admin
        if(SecurityUtils.hasRole("ADMIN")){
            admin=Admin.get(principal.id)
            utente=admin
            //render view: "/menu/sceltaMenu"
        }
        else if(SecurityUtils.hasRole("DEALER")){
            dealer=Dealer.get(principal.id)
            utente=dealer
        }
        [utente:utente]

    }
    def rcaGratuita(){
        def principal = SecurityUtils.principal
        def utente,dealer,admin
        if(SecurityUtils.hasRole("ADMIN")){
            admin=Admin.get(principal.id)
            utente=admin
        }
        else if(SecurityUtils.hasRole("DEALER")){
            dealer=Dealer.get(principal.id)
            utente=dealer
        }

        /*def polizze = Polizza.createCriteria().list(max: max, offset: offset) {
            // eq "attiva", true
            if(!utente.isAdmin) eq "dealer", dealer
            //else eq "user", utente
            *//* user {
                 datiConcessionariodes {
                     ilike "concessOfficina", "%auto%"
                 }
             }*//*
            if(cerca) {
                or {
                    ilike "noPolizza", "%${cerca}%"
                    ilike "telaio", "%${cerca}%"
                    ilike "targa", "%${cerca}%"
                   *//* dealer {
                        ilike "ragioneSociale", "%${cerca}%"
                    }*//*
                }
            }
        }*/

        def max = 15, pagesPerPage = 7
        def page = (params.page ?: 1) as int
        def offset = (page - 1) * max
        def cerca = params.search


        def pages = Math.ceil(15 / max) as int
        if(pages <= 1) pages = []
        else if(pages <= pagesPerPage) pages = 1..pages
        else if(page < (pagesPerPage / 2)) pages = 1..pagesPerPage
        else {
            def minPage = page - ((pagesPerPage / 2) as int)
            def maxPage = page + ((pagesPerPage / 2) as int)
            if(maxPage > pages) pages = (maxPage - pagesPerPage)..pages
            else pages = minPage..maxPage
        }
        if(!pages && page > 1) redirect action: "rcaGratuita", params: params.search ? [search: params.search] : null
        else if(pages && page > pages.max()) redirect action: "rcaGratuita", params: [page: 1, search: params.search]
        //println page
        if(request.post) {

        }
        [pages: pages, page: page, utente:utente]


    }

    def extensionWarranty(){
       // CaricaPolizzeJob.triggerNow()
        def principal = SecurityUtils.principal
        def utente,dealer,admin
        if(SecurityUtils.hasRole("ADMIN")){
            admin=Admin.get(principal.id)
            utente=admin
        }
        else if(SecurityUtils.hasRole("DEALER")){
            dealer=Dealer.get(principal.id)
            utente=dealer
        }
        def max = 15, pagesPerPage = 7
        def page = (params.page ?: 1) as int
        def offset = (page - 1) * max
        def cerca = params.search ? "%${params.search}%" : null

        def polizze = Polizza.createCriteria().list(max: max, offset: offset) {
            if(!admin) {
                eq "dealer", dealer
                projections{
                    createAlias ('telaio', 't')
                }
            }
            else {
                projections {
                    createAlias ('dealer', 'd')
                    createAlias ('telaio', 't')
                }
            }
            eq "tipoPolizza",TipoPolizza.EXTENSION
            if(cerca) {
                or {
                    ilike "noPolizza", "%${cerca}%"
                    ilike "t.telaio", "%${cerca}%"
                    ilike "t.targa", "%${cerca}%"
                    ilike "t.modello", "%${cerca}%"
                    if(admin){
                        ilike "d.ragioneSociale", "%${cerca}%"
                    }
                }
            }
        }
        def pages = Math.ceil(polizze.totalCount / max) as int
        def totalPages=pages
        if(pages <= 1) pages = []
        else if(pages <= pagesPerPage) pages = 1..pages
        else if(page < (pagesPerPage / 2))  pages = 1..pagesPerPage
        else {
            def minPage = page - ((pagesPerPage / 2) as int)
            def maxPage = page + ((pagesPerPage / 2) as int)
            if(maxPage > pages) pages = (maxPage - pagesPerPage)..pages
            else pages = minPage..maxPage
        }
        if(!pages && page > 1) {
            redirect action: "extensionWarranty", params: params.search ? [search: params.search] : null
        }
        else if(pages && page > pages.max()){
            redirect action: "extensionWarranty", params: [page: 1, search: params.search]
        }
        [polizze: polizze, pages: pages, page: page,utente:utente, totalPages:totalPages]
    }
    def extensionWarranty1(){



        redirect action: "extensionWarranty", params: [page: 1, search: params.search]
    }
    def demoOpen(){
        def principal = SecurityUtils.principal
        def utente,dealer,admin
        if(SecurityUtils.hasRole("ADMIN")){
            admin=Admin.get(principal.id)
            utente=admin
        }
        else if(SecurityUtils.hasRole("DEALER")){
            dealer=Dealer.get(principal.id)
            utente=dealer
        }
        def max = 15, pagesPerPage = 7
        def page = (params.page ?: 1) as int
        def offset = (page - 1) * max
        def cerca = params.search ? "%${params.search}%" : null

        def polizze = Polizza.createCriteria().list(max: max, offset: offset) {
            if(!admin) {
                eq "dealer", dealer

            }
            else {
                projections {
                    createAlias ('dealer', 'd')
                }
            }
            eq "tipoPolizza",TipoPolizza.DEMO_OPEL
            if(cerca) {
                or {
                    ilike "noPolizza", "%${cerca}%"
                    ilike "telaioDemo", "%${cerca}%"
                    ilike "targa", "%${cerca}%"
                    if(admin){
                        ilike "d.ragioneSociale", "%${cerca}%"
                    }
                }
            }
        }
        def pages = Math.ceil(polizze.totalCount / max) as int

        if(pages <= 1) pages = []
        else if(pages <= pagesPerPage) pages = 1..pages
        else if(page < (pagesPerPage / 2))  pages = 1..pagesPerPage
        else {
            def minPage = page - ((pagesPerPage / 2) as int)
            def maxPage = page + ((pagesPerPage / 2) as int)
            if(maxPage > pages) pages = (maxPage - pagesPerPage)..pages
            else pages = minPage..maxPage
        }
        if(!pages && page > 1) {
            redirect action: "demoOpen", params: params.search ? [search: params.search] : null
        }
        else if(pages && page > pages.max()){
            redirect action: "demoOpen", params: [page: 1, search: params.search]
        }
        [polizze: polizze, pages: pages, page: page,utente:utente]
    }
}
