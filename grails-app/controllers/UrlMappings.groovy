import org.springframework.security.access.AccessDeniedException

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                /*controller="menu"
                action="extensionWarranty"*/
            }
        }
        name list: "/$controller/extensionWarranty/$page" {
            action = "extensionWarranty"
        }
        name listDemo: "/$controller/demoOpen/$page" {
            action = "demoOpen"
        }


        "/login"(view: "/login")
        "/passwordScaduta"(controller: "security", action: "passwordScaduta")
        "/attivaDealer"(controller: "security", action: "attivaDealer")
        "/datiUtente"(view: "/user/datiUtente")
        "/notAuthorized"(view: "/notAuthorized")
        //"/" (view:"/index")
        "/"(controller: "menu", action: "sceltaMenu")
        "/grails"(view: "/grails")
        "500"(view:'/error')
        "500"(view: "/notAuthorized", exception: AccessDeniedException)
        "403"(view:'/notAuthorized')
        "404"(view:'/notFound')
    }
}
