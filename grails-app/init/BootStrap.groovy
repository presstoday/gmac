import gmac.polizze.Telai
import gmac.utenti.*
import grails.util.Environment
import org.apache.poi.hssf.usermodel.HSSFWorkbook
import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.util.CellReference
import org.springframework.web.context.support.WebApplicationContextUtils

import java.text.DecimalFormat

import static org.apache.poi.ss.usermodel.DateUtil.getJavaDate

class BootStrap {


    private def cellValue(cell) {
        switch(cell.cellType) {

            case Cell.CELL_TYPE_BOOLEAN: return cell.booleanCellValue
            case Cell.CELL_TYPE_ERROR: return cell.errorCellValue
            case Cell.CELL_TYPE_NUMERIC: return cell.numericCellValue
            case Cell.CELL_TYPE_STRING: return cell.stringCellValue
            case Cell.CELL_TYPE_FORMULA: return cell.getRichStringCellValue()
            default: return null
        }
    }

    private def cell(row, index, date = false) {
        def value = cellValue(row.getCell(index, Row.CREATE_NULL_AS_BLANK))
        if(date) return value ? getJavaDate(value) : null
        return value
    }

    void caricaExcelDealer() {
        def excel = this.class.getResource("excel/AnagraficaOpel4YExtendedWarrantyETG_v2.xls")
        println "Loading AnagraficaOpel4YExtendedWarrantyETG_v2.xls from: ${excel.path}"
        def wb = new HSSFWorkbook(excel.openStream())
        def decimalFormat = new DecimalFormat("#")

        def format = { dato ->
            if(dato instanceof String) return dato
            try {
                return decimalFormat.format(dato)
            } catch(e) {
                println "$dato: ${dato.getClass()} non convertibile"
                return null
            }
        }

        Dealer.withTransaction {
            wb.getSheetAt(0).with {
                def cellaCodice = new CellReference("B2")
                def cellaRagioneSociale = new CellReference("I2")
                def cellIndirizzo = new  CellReference("N2")
                def cellLocalita = new  CellReference("P2")
                def cellProvincia = new  CellReference("R2")
                def cellCap = new  CellReference("S2")
                def cellTelefono = new  CellReference("V2")
                def cellEmail = new  CellReference("W2")
                def cellPartitaIva = new  CellReference("X2")
                def cellExtension=new CellReference("Y2")
                def cellRCA=new CellReference("Z2")
                def cellDemo=new CellReference("AA2")
                def ultimaRiga = new CellReference("A543")
                def firstRow = cellaCodice.row
                def lastRow = ultimaRiga.row
                (firstRow..lastRow).each { rowIndex ->
                    println "Analizzo riga: ${rowIndex}"
                    def row = getRow(rowIndex)
                    def codice=cell(row,cellaCodice.col)
                    /*def codice = cell(row, cellaCodice.col)
                    codice=codice.toString().replaceAll("[^0-9]", "")
                    if (codice.toString().matches("[0].*")) {

                        codice=codice.toString().replaceFirst("[^1-9]", "")
                    }                    */
                    codice=codice.toString().replaceFirst ("^0*", "");
                    if(codice.toString().equals("null")){
                        codice="";
                    }
                    println("codice modificato..."+codice)
                    def partitaIva = cell(row, cellPartitaIva.col)
                    partitaIva=partitaIva.toString().replaceAll("[^a-z,A-Z,0-9]","")
                    def ragioneSociale = cell(row, cellaRagioneSociale.col)
                    def indirizzo = cell(row, cellIndirizzo.col)
                    def email = cell(row, cellEmail.col)
                    def localita = cell(row, cellLocalita.col).toString()
                    def subProv = cell(row, cellProvincia.col).toString()
                    /*int indexProv1=  localita.toString().indexOf("(")
                    int indexProv2=  localita.toString().indexOf(")")
                    if(indexProv1>0 && indexProv2>0){subProv=localita.toString().substring(indexProv1+1,indexProv2)}*/
                    def cap = cell(row, cellCap.col)
                    def telefono = cell(row, cellTelefono.col)
                    telefono=telefono.toString().replaceAll("[^a-z,A-Z,0-9]","")
                    def dealer = Dealer.findOrCreateWhere(
                        username: codice,
                        ragioneSociale:ragioneSociale,
                        piva:  format(partitaIva),
                        indirizzo: indirizzo,
                        localita: localita,
                        cap: format(cap),
                        telefono:   telefono ,
                        email:  email,
                        provincia: subProv,
                        password: format(partitaIva),
                        enabled: true,
                        attivo: false,
                        passwordScaduta: false,
                        accettazione: false
                    )
                    def extension,rca,demo
                    extension=cell(row, cellExtension.col)
                    rca=cell(row, cellRCA.col)
                    demo=cell(row, cellDemo.col)
                    if(extension.toString().equalsIgnoreCase("X") ) dealer.addRuolo(Ruolo.EXTENSION_WARRANTY)
                    if(rca.toString().equalsIgnoreCase("X") ) dealer.addRuolo(Ruolo.RCA_GRATUITA)
                    if(demo.toString().equalsIgnoreCase("X")) dealer.addRuolo(Ruolo.DEMO_OPEN)
                    if(dealer.save(flush:true)) println "Dealer creato correttamente ${codice}"
                    else println "Errore creazione Dealer: ${dealer.errors}"
                }
            }
            wb.close()
        }

        def userAdmin= new Admin()
        userAdmin.email= "backoffice@mach-1.it"
        userAdmin.username= "ITADMIN"
        userAdmin.password="adminMach1"
        userAdmin.nome="admin"
        userAdmin.cognome="admin"
        userAdmin.passwordScaduta=false
        userAdmin.addRuolo(Ruolo.EXTENSION_WARRANTY)
        userAdmin.addRuolo(Ruolo.RCA_GRATUITA)
        userAdmin.addRuolo(Ruolo.DEMO_OPEN)

        if(userAdmin.save(flush: true)) println "L'utente admin è stato creato"
        else println "Errore creazione utente admin: ${userAdmin.errors}"
    }

    void caricaExcelTelai() {
        def excel = this.class.getResource("excel/telaiOpel.xls")
        println "Loading telaiOpel.xls from: ${excel.path}"
        def wb = new HSSFWorkbook(excel.openStream())
        def decimalFormat = new DecimalFormat("#")

        def format = { dato ->
            if(dato instanceof String) return dato
            try {
                return decimalFormat.format(dato)
            } catch(e) {
                println "$dato: ${dato.getClass()} non convertibile"
                return null
            }
        }

        Telai.withTransaction {
            wb.getSheetAt(0).with {
                def cellaImmatricolazione = new CellReference("A4")
                def cellModello = new  CellReference("B4")
                def cellTrasmissione = new  CellReference("C4")
                def cellCilindrata = new  CellReference("D4")
                def cellVersione = new  CellReference("E4")
                def cellTelaio = new  CellReference("F4")
                def cellTarga = new  CellReference("G4")
                def ultimaRiga = new CellReference("A53")
                def firstRow = cellaImmatricolazione.row
                def lastRow = ultimaRiga.row
                (firstRow..lastRow).each { rowIndex ->
                    println "Analizzo riga: ${rowIndex}"
                    def row = getRow(rowIndex)
                    def immatricolazione=cell(row,cellaImmatricolazione.col,true)
                    def noTelaio = cell(row, cellTelaio.col)
                    noTelaio=noTelaio.toString().replaceAll("[^a-z,A-Z,0-9]","")
                    def modello = cell(row, cellModello.col)
                    def trasmissione = cell(row, cellTrasmissione.col)
                    def cilindrata = cell(row, cellCilindrata.col)
                    cilindrata=cilindrata.toString().replaceAll("[.][0-9]", "");
                    def versione = cell(row, cellVersione.col)
                    def targa = cell(row, cellTarga.col)
                    def telaio = Telai.findOrCreateWhere(
                            targa: targa,
                            telaio: noTelaio,
                            cilindrata: cilindrata,
                            modello: modello,
                            trasmissione: trasmissione,
                            versione: versione,
                            dataImmatricolazione: immatricolazione
                    )
                    if(telaio.save()) println "Telaio creato correttamente"
                    else println "Errore caricamento telaio: ${telaio.errors}"
                }
            }
            wb.close()
        }
    }

    void aggiungeRuoli(){
        def excel = this.class.getResource("excel/Anagrafica_aggiornata.xls")
        println "Loading Anagrafica_aggiornata.xls from: ${excel.path}"
        def wb = new HSSFWorkbook(excel.openStream())
        Dealer.withTransaction {
            wb.getSheetAt(0).with {
                def cellaCodice = new CellReference("B2")
                def cellRCA=new CellReference("AA2")
                def ultimaRiga = new CellReference("A543")
                def firstRow = cellRCA.row
                def lastRow = ultimaRiga.row
                (firstRow..lastRow).each { rowIndex ->
                    println "Analizzo riga: ${rowIndex}"
                    def row = getRow(rowIndex)
                    def codice=cell(row,cellaCodice.col)
                    codice=codice.toString().replaceFirst ("^0*", "");
                    if(codice.toString().equals("null")){
                        codice="";
                    }

                    def rca=cell(row, cellRCA.col)
                    def dealer = Dealer.findByUsername(codice)
                    if(dealer){
                        if(rca.toString().equalsIgnoreCase("x") ){
                            println("codice trovato..."+codice)
                            dealer.addRuolo(Ruolo.RCA_GRATUITA)
                        }
                        if(dealer.save(flush:true)) println "Dealer creato correttamente ${dealer.ruoli}"
                        else println "Errore creazione Dealer: ${dealer.errors}"
                    }


                }
            }
            wb.close()
        }




    }
    def init = { servletContext ->
        if(Environment.current == Environment.DEVELOPMENT) {
            //caricaExcelDealer()
            // caricaExcelTelai()
            // aggiungeRuoli()
        }


        println "GMAC started!"
    }

    def destroy = {
    }
}
