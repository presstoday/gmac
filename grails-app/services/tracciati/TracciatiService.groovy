package tracciati

import gmac.polizze.DatiContabili
import gmac.polizze.Polizza
import gmac.polizze.Tracciato
import gmac.polizze.TracciatoCompagnia
import gmac.utenti.Dealer
import grails.converters.JSON
import grails.transaction.Transactional

import java.time.Period

import static grails.util.Environment.executeForCurrentEnvironment

@Transactional
class TracciatiService {
    static transactional = false
    def mailService
/***marzo 3 nuova impostazione calcoli pdu,imposte e totale caricamenti***/
    def generaTracciatoIAssicur(Polizza polizza) {
        def tracciatoPolizza
        //try{
            def datiContabili=DatiContabili.getAll().first()
            def tracciato = [
                    "Kasko": "I".padLeft(1),
                    "Codice modello": "".padLeft(5),
                    "Modello veicolo": polizza.telaio.modello.trim().padLeft(20),
                    "Targa": polizza.telaio.targa.trim().padLeft(15),
                    "Cognome": polizza.dealer.ragioneSociale.padLeft(40),
                    "Nome": "".padLeft(40),
                    "Provincia": polizza.dealer.provincia.padLeft(2),
                    "Valuta": "E".padLeft(1),
                    "Valore assicurato": "".padLeft(9),
                    "Data immatricolazione": polizza.telaio.dataImmatricolazione.format('yyyyMMdd').padLeft(8),
                    "Durata furto incendio": "".padLeft(3),
                    "Indirizzo": polizza.dealer.indirizzo.padLeft(30),
                    "Cap": polizza.dealer.cap.padLeft(5),
                    "Città": polizza.dealer.localita.padLeft(30),
                    "Data di nascita": "".padLeft(8),
                    "Provincia nascita": "".padLeft(2),
                    "Sesso": "".padLeft(1),
                    "Numero polizza": polizza.noPolizza.padLeft(10),
                    "Codice operazione": "0".padLeft(3),
                    "Data apertura contratto": "".padLeft(8),
                    "Importo assicurato": "".padLeft(9),
                    "Codice fiscale / Partita IVA": polizza.dealer.piva.padLeft(16),
                    "Marca":  datiContabili.marca.padLeft(5),
                    "Autocarro": "N".padLeft(1),
                    "Uso": "".padLeft(1),
                    "Scadenza vincolo": "".padLeft(8),
                    "Codice dealer": "".padLeft(5),
                    "Numero telaio": polizza.telaio.telaio.trim().padLeft(17),
                    "Numero rinnovo": "".padLeft(2),
                    "Vincolo": "".padLeft(1),
                    "Pcl":  "${(datiContabili.pcl* 100).round()}".padLeft(9),
                    "Old contra": "".padLeft(10),
                    "Cv fisc": "".padLeft(4),
                    "Quintali": "".padLeft(4),
                    "Data decorrenza": polizza.dataDecorrenza.format("yyyyMMdd").padLeft(8),
                    "Buyback": "".padLeft(1),
                    "Satellitare": "".padLeft(1),
                    "Data scadenza": polizza.dataScadenza.format("yyyyMMdd").padLeft(8),
                    "Rc": "N".padLeft(1),
                    "Telefono casa": polizza.dealer.telefono.padLeft(15),
                    "Telefono ufficio": "".padLeft(15),
                    "Telefono": "".padLeft(15),
                    "Cellulare": "".padLeft(15),
                    "Provvigioni dealer":  "${(datiContabili.provvigioniDealer* 100).round()}".padLeft(9),
                    "Provvigioni Mach1":  "${(datiContabili.provvigioniMach1* 100).round()}".padLeft(9),
                    "Provvigioni società commerciale": "".padLeft(9),
                    "Totale caricamenti":  "${((datiContabili.provvigioniDealer + datiContabili.provvigioniMach1 + datiContabili.provvigioniSocietaCommerciale)* 100).round()}".padLeft(9),
                    "Imponibile":  "${(datiContabili.imponibile* 100).round()}".padLeft(9),
                    "Imposte":  "${((datiContabili.pcl-datiContabili.imponibile)* 100).round()}".padLeft(9),
                    "Pdu":  "${((datiContabili.imponibile-datiContabili.provvigioniDealer)* 100).round()}".padLeft(9),
                    "Iban": "".padLeft(34),
                    "Collisione": "N".padLeft(1),
                    "Data operazione": "".padLeft(8),
                    "Codice prodotto":  datiContabili.codiceProdotto.padLeft(2),
                    "Tipologia veicolo": "".padLeft(1),
                    "Rischio": "".padLeft(1),
                    "Zona": "".padLeft(1),
                    "Email": polizza.dealer.email.padLeft(50),
                    "Mini collisione": "".padLeft(2),
                    "Garanzia2": "".padLeft(2),
                    "Durata garanzia2": "".padLeft(2),
                    "Durata kasko": "00".padLeft(2),
                    "Durata collisione": "00".padLeft(2),
                    "Durata valore a nuovo": "00".padLeft(2),
                    "Codice società commerciale": "".padLeft(3),
                    "Codice venditore": "".padLeft(3),
                    "Provvigioni venditore": "".padLeft(9),
                    "Codice segnalatore": "".padLeft(3),
                    "Provvigioni segnalatore": "".padLeft(9),
                    "Integra": "".padLeft(1),
                    "Numero polizza integrata": "".padLeft(10),
                    "Durata integra": "".padLeft(2),
                    "Codice iban": "".padLeft(27),
                    "Intestatario iban": "".padLeft(30),
                    "Token": "".padLeft(16),
                    "Cognome nome assicurato": "".padLeft(40),
                    "Partita iva assicurato": "".padLeft(11),
                    "Indirizzo assicurato": "".padLeft(30),
                    "Citta assicurato": "".padLeft(30),
                    "Cap assicurato": "".padLeft(5),
                    "Provincia assicurato": "".padLeft(2),
                    "Codice fiscale assicurato": "".padLeft(16),
                    "Data nascita assicurato": "".padLeft(8),
                    "Sesso assicurato": "".padLeft(1),
                    "Seriale Lojack": "".padLeft(7)
            ]
            def dataInvioTracciato=new Date()
            /***
             * da usare per rca gratuita
             def datecreatedPolizza= Integer.parseInt(polizza.dateCreated.format('H'))
             def dataInvioTracciato=new Date()
             if(datecreatedPolizza>10){
             dataInvioTracciato=new Date(dataInvioTracciato.getTime() + (24 * 60 * 60 * 1000));
             }
             def tracciatoCompagnia= generaTracciatoCompagnia(polizza)
             def tracciatoPolizza = new Tracciato(polizza: polizza, tracciato: tracciato.values().join(), tracciatoCompagnia:tracciatoCompagnia, dataInvio: dataInvioTracciato)*/
            //tracciatoPolizza = new Tracciato(polizza: polizza, tracciato: tracciato.values().join(), dataInvio: dataInvioTracciato)
            //if(tracciatoPolizza.save(flush:true)){

            //}else{
              //  println tracciatoPolizza.errors
               // tracciatoPolizza ="Errore nel salvataggio del tracciato della polizza ${tracciatoPolizza.errors}"
            //}
       /* }catch(e){
            tracciatoPolizza ="Errore nella creazione del tracciato polizza "
        }*/
        return new Tracciato(polizza: polizza, tracciato: tracciato.values().join(), dataInvio: dataInvioTracciato)
    }
    def generaTracciatoCompagnia(Polizza polizza) {
        def trasmissione=polizza.telaio.trasmissione
        def vehicletrasmission
        if(trasmissione.contains("TRANS-MAN")){
            vehicletrasmission="Manuale"
        }else{
            vehicletrasmission="Automatico"
        }
        //def anniMacchina=diffDateInYears(polizza.dataImmatricolazione,new Date())
        def differenza= diffYears(polizza.telaio.dataImmatricolazione, new Date())
        /*def comma_delimiter = ","
        def new_line_separator="\n"
        def file_header="st Dealer Code,strUserReference,strPolPostingType,strPolicyNumber,strCustomerType,strVehicleType,strCustomerTitle,strCustomerForename,strCustomerSurname,strCompanyName,strCustomerAddress1,strCustomerAddress2," +
                "strCustomerAddress3,StrCustomerPostCode,strCountryCode,strCustomerTelephone,strCustomerWorkTelephone,strCustomerMobileTelephone,strCustomerEmailAddress,strEuroTax,strVehicleMake,strVehicleModel,strVehicleModelDesc," +
                "strVehicleModelCode,strVehicleAge,strVehicleAgeinMonths,strVehicleReg,strVehicleRegDate,strVehicleMileage,strPurchasePrice,strVehicleEngineSize,str VehicleTransmission,strFuelType,strChassisNumber,strLengthofCover," +
                "strCommencementDate,strExpiryDate,strPurchaseDate,strSellingPrice,strDealerPrice,strEntryDate,strPolicyStatus,strProductDescription,strTransactionDesc,strOriginalPolicy,strComment,strInsurerCode,strExcessValue" +
                "strClaimLimitValue,strFinance_YN,strFinanceCompanyName,strStatusCode,strCoverType,strAddOnTurbo,strAddOnCat,strAddOnABS,strAddOnAWD,strProductTypeCode,strCoverCode,strMOTduedate"*/
        def nome =polizza.nome.trim().toString()
        nome= nome.replaceAll(",",""",""").replaceAll("(\\\\r\\\\n|\\\\n)", " ")
        def cognome= polizza.cognome.trim().toString()
        cognome=cognome.replaceAll(",",""",""").replaceAll("(\\\\n|\\\\\r\\\\\n)", " ")
        def versione= polizza.telaio.versione.trim().toString()
        versione=versione.replaceAll(",",""",""").replaceAll("(\\\\n|\\\\r\\\\\n)", " ")
        def modello=polizza.telaio.modello.trim().toString()
        modello=modello.replaceAll(",",""",""").replaceAll("(\\\\n|\\\\r\\\\n)", " ")
        def cilindrata=polizza.telaio.cilindrata.trim().toString()
        cilindrata=cilindrata.replaceAll(",",""",""").replaceAll("(\\\\n|\\\\r\\\\n)", " ")
        def telaio=polizza.telaio.telaio.trim().toString()
        telaio=telaio.replaceAll(",",""",""").replaceAll("(\\\\n|\\\\r\\\\n)", " ")
        def tracciato = [
                "st Dealer Code":polizza.dealer.username.trim(),
                "strUserReference":"8355",
                "strPolPostingType":"R",
                "strPolicyNumber":"xxxxxxxxxx",
                "strCustomerType":"PRIV",
                "strVehicleType":"",
                "strCustomerTitle":"",
                "strCustomerForename":nome,
                "strCustomerSurname":cognome,
                "strCompanyName":"",
                "strCustomerAddress1":"",
                "strCustomerAddress2":"",
                "strCustomerAddress3":"",
                "StrCustomerPostCode":"",
                "strCountryCode":"",
                "strCustomerTelephone":"",
                "strCustomerWorkTelephone":"",
                "strCustomerMobileTelephone":"",
                "strCustomerEmailAddress":"",
                "strEuroTax":"",
                "strVehicleMake":"OPEL",
                "strVehicleModel":modello,
                "strVehicleModelDesc":versione,
                "strVehicleModelCode":"",
                "strVehicleAge":"",
                "strVehicleAgeinMonths":"",
                "strVehicleReg":polizza.telaio.targa.trim(),
                "strVehicleRegDate":polizza.telaio.dataImmatricolazione.format("dd/MM/yyyy"),
                "strVehicleMileage":polizza.km.toString(),
                "strPurchasePrice":"",
                "strVehicleEngineSize":cilindrata,
                "str VehicleTransmission":"",
                "strFuelType":"",
                "strChassisNumber":telaio,
                "strLengthofCover":"12",
                "strCommencementDate":polizza.dataDecorrenza.format("dd/MM/yyyy"),
                "strExpiryDate":polizza.dataScadenza.format("dd/MM/yyyy"),
                "strPurchaseDate":polizza.dataTagliando.format("dd/MM/yyyy"),
                "strSellingPrice":"",
                "strDealerPrice":"",
                "strEntryDate":polizza.dateCreated.format("dd/MM/yyyy"),
                "strPolicyStatus":"",
                "strProductDescription":"MBI",
                "strTransactionDesc":"USED",
                "strOriginalPolicy":"",
                "strComment":"",
                "strInsurerCode":"",
                "strExcessValue":"0",
                "strClaimLimitValue":"2000.00",
                "strFinance_YN":"N",
                "strFinanceCompanyName":"",
                "strStatusCode":"",
                "strCoverType":"MBI",
                "strAddOnTurbo":"N",
                "strAddOnCat":"N",
                "strAddOnABS":"N",
                "strAddOnAWD":"N",
                "strProductTypeCode":"",
                "strCoverCode":"",
                "strMOTduedate":""
        ]
        def dataInvioTracciato=new Date()
        def tracciatoCompagnia= new JSON(tracciato.values()).toString()
        /*def tracciatoCompagniaPolizza = new TracciatoCompagnia(polizza: polizza, tracciatoCompagnia:tracciatoCompagnia, dataInvio:  dataInvioTracciato)
        tracciatoCompagniaPolizza.save(flush: true)
        if(tracciatoCompagniaPolizza.hasErrors()) {
            println "errori ${tracciatoCompagniaPolizza.errors}"
            tracciatoCompagnia= "Errore nel salvataggio della polizza ${tracciatoCompagniaPolizza.errors}"
        }*/
        return new TracciatoCompagnia(polizza: polizza, tracciatoCompagnia:tracciatoCompagnia, dataInvio:  dataInvioTracciato)
    }
    private int diffDateInYears(Date start, Date end) {
        if(start > end) {
            def temp = end
            end = start
            start = temp
        }
        //def diffMesi = (start[Calendar.MONTH] - end[Calendar.MONTH]) + 1
        def diffAnni = start[Calendar.YEAR] - end[Calendar.YEAR]
        return diffAnni
    }
    private int diffYears(Date first, Date last) {
        Calendar a = getCalendar(first);
        Calendar b = getCalendar(last);
        int diff = b.get(Calendar.YEAR) - a.get(Calendar.YEAR);
        if (a.get(Calendar.MONTH) > b.get(Calendar.MONTH) || (a.get(Calendar.MONTH) == b.get(Calendar.MONTH) && a.get(Calendar.DATE) > b.get(Calendar.DATE))) {
            diff--;
        }
        return diff;
    }
    private Calendar getCalendar(Date date) {
        Calendar cal = Calendar.getInstance(Locale.ITALY);
        cal.setTime(date);
        return cal;
    }
}
