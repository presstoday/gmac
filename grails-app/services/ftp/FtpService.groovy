package ftp

import excel.reader.ExcelReader
import gmac.polizze.Polizza
import gmac.polizze.Telai
import gmac.utenti.Dealer
import gmac.utenti.Ruolo
import grails.transaction.Transactional
import groovy.time.TimeCategory
import org.apache.poi.hssf.usermodel.HSSFWorkbook
import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.util.CellReference

//import org.codehaus.groovy.grails.web.util.WebUtils
import org.grails.web.util.WebUtils
import com.jcraft.jsch.*


import javax.servlet.http.HttpSession
import java.text.DecimalFormat
import java.util.regex.Pattern

import static org.apache.poi.ss.usermodel.DateUtil.getJavaDate
@Transactional
class FtpService {

    def tracciatiService



    private def cellValue(cell) {
        switch(cell.cellType) {

            case Cell.CELL_TYPE_BOOLEAN: return cell.booleanCellValue
            case Cell.CELL_TYPE_ERROR: return cell.errorCellValue
            case Cell.CELL_TYPE_NUMERIC: return cell.numericCellValue
            case Cell.CELL_TYPE_STRING: return cell.stringCellValue
            case Cell.CELL_TYPE_FORMULA: return cell.getRichStringCellValue()
            default: return null
        }
    }
    private def cell(row, index, date = false) {
        def value = cellValue(row.getCell(index, Row.CREATE_NULL_AS_BLANK))
        if(date) return value ? getJavaDate(value) : null
        return value
    }
    private HttpSession getSession() { WebUtils.retrieveGrailsWebRequest().getSession() }
}
