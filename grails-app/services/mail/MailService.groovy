package mail

import gmac.polizze.Documenti
import gmac.polizze.Polizza
import gmac.polizze.TipoDocumento
import gmac.utenti.Dealer
import gmac.utenti.Log
import grails.core.GrailsApplication
import grails.transaction.Transactional
import grails.util.Environment
import mandrill.MandrillAttachment
import mandrill.MandrillMessage
import mandrill.MandrillRecipient
import mandrill.MergeVar
import org.apache.commons.mail.EmailException
import org.apache.commons.mail.HtmlEmail
import org.grails.web.util.WebUtils

import javax.mail.util.ByteArrayDataSource as ByteArrayDS
import javax.servlet.http.HttpSession
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

@Transactional
class MailService {
    def mandrillService
    GrailsApplication grailsApplication

    def invioMail(email, ragioneSociale, username, password) {
        def recpts = []
        def attachs=[]
        def contents = []
        def credenziali= "codice: ${username} <br> password: ${password} <br><br>"
        def vars = [new MergeVar(name: "CREDENZIALI", content: "${credenziali}")]
        recpts.add(new MandrillRecipient(name:ragioneSociale, email: "priscila@presstoday.com"))
        /** quando andremmo in prodrecpts.add(new MandrillRecipient(name:ragioneSociale, email: email))*/
        contents.add([name:"", content:""])
        def mandrillMessage = new MandrillMessage(
                text:"this is a text message",
                subject:"Credenziali accesso portale dealer OPEL",
                from_email:"assistenzaclienti@mach-1.it",
                to:recpts,
                attachments : attachs,
                global_merge_vars:vars
        )
        def mailinviata = mandrillService.sendTemplate(mandrillMessage, "credenziali-accesso-opel", contents )
        def inviatoDealer = mailinviata.count { r -> r.success == false } == 0
        def response1 = mailinviata.collect { response1 ->
            [
                    status: response1?.status,
                    message: response1?.message,
                    emailCliente: response1?.email,
                    id: response1?.id,
                    rejectReason: response1?.rejectReason,
                    successCliente: response1?.success
            ].collect { k, v -> "$k: $v" }.join(", ")
        }.join("\n")/*.collect { k, v -> "$k: $v" }.join("\n")*/


        return inviatoDealer
    }
    def invioMailRichiestaEsclusione( polizzaId) {
        def logg
        def polizza=Polizza.get(polizzaId)
        def recpts = []
        def attachs=[]
        def contents = []
        def credenziali= "Il Dealer: ${polizza.dealer.ragioneSociale} <br> <br> ha chiesto la esclusione della seguente polizza: ${polizza.noPolizza} - ${polizza.targa.toUpperCase()} in data ${polizza.dataEsclusione.format("dd-MMM-yyyy")}  "
        def vars = [new MergeVar(name: "CREDENZIALI", content: "${credenziali}")]
        if (Environment.current==Environment.DEVELOPMENT){
            recpts.add(new MandrillRecipient(name:"Priscila", email: "priscila@presstoday.com"))

        }else if(Environment.current == Environment.PRODUCTION) {
            recpts.add(new MandrillRecipient(name:"gm@mach-1.it", email: "gm@mach-1.it"))

        }else if (Environment.current==Environment.TEST) {
            recpts.add(new MandrillRecipient(name: "Silvia Carino", email: "s.carino@mach-1.it"))
            recpts.add(new MandrillRecipient(name: "Cristina Sivelli", email: "c.sivelli@mach-1.it"))
            recpts.add(new MandrillRecipient(name: "Team Mach1", email: "priscila@presstoday.com"))
        }
        contents.add([name:"", content:""])
        def mandrillMessage = new MandrillMessage(
                text:"this is a text message",
                subject:"Richiesta esclusione Polizza ${polizza.noPolizza} - ${polizza.targa.toUpperCase()}",
                from_email:"assistenzaclienti@mach-1.it",
                to:recpts,
                attachments : attachs,
                global_merge_vars:vars
        )
        def mailinviata = mandrillService.sendTemplate(mandrillMessage, "richiesta-esclusione", contents )
        def inviatoDealer = mailinviata.count { r -> r.success == false } == 0
        def response1 = mailinviata.collect { response1 ->
            [
                    status: response1?.status,
                    message: response1?.message,
                    emailCliente: response1?.email,
                    id: response1?.id,
                    rejectReason: response1?.rejectReason,
                    successCliente: response1?.success
            ].collect { k, v -> "$k: $v" }.join(", ")
        }.join("\n")/*.collect { k, v -> "$k: $v" }.join("\n")*/
        return response1
    }
    def invioMailLibretto( dealerId) {
        def date=new Date();
        def streamLibretti = new ByteArrayOutputStream()
        def zipLibretti = new ZipOutputStream(streamLibretti)
        def recpts = []
        def attachs=[]
        def contents = []
        def logg
        def dealer=Dealer.get(dealerId)
        def targhe="Elenco targhe:<br><br>"
        //prendo i libretti
        def libretti=Documenti.createCriteria().list(){
            eq "inviatoMail", false
            eq "tipo",TipoDocumento.LIBRETTO
            eq "dealer", dealer

        }
        if(libretti){
            int i=0
            libretti.each{libretto->
                targhe+="${libretto.targa}<br>"
                zipLibretti.putNextEntry(new ZipEntry("${dealer.username}_${i}_"+libretto.fileName))
                zipLibretti.write(libretto.fileContent)
                i++
                libretto.inviatoMail=true
                if(!libretto.save(flush:true)){
                    logg = new Log(parametri: "l'aggiornamento dello stato del documento libretto e' fallito", operazione: "aggiornamento status Libretto", pagina: "Salva e chiudi")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                }
            }
            zipLibretti.close()
            streamLibretti.close()
            def credenziali= "${dealer.ragioneSociale} <br> <br>"

            def vars = [new MergeVar(name: "CREDENZIALI", content: "${credenziali}"),new MergeVar(name: "TARGHE", content: "${targhe}")]
            if (Environment.current==Environment.DEVELOPMENT){
                recpts.add(new MandrillRecipient(name:"Priscila", email: "priscila@presstoday.com"))

            }else if(Environment.current == Environment.PRODUCTION) {
                recpts.add(new MandrillRecipient(name:"gm@mach-1.it", email: "gm@mach-1.it"))

            }else if (Environment.current==Environment.TEST) {
                recpts.add(new MandrillRecipient(name: "Silvia Carino", email: "s.carino@mach-1.it"))
                recpts.add(new MandrillRecipient(name: "Cristina Sivelli", email: "c.sivelli@mach-1.it"))
                recpts.add(new MandrillRecipient(name: "Team Mach1", email: "priscila@presstoday.com"))
            }
            def fileEncode = new sun.misc.BASE64Encoder().encode(streamLibretti.toByteArray())
            attachs.add(new MandrillAttachment(type: "application/octet-stream", name: "Libretti_${dealer.username}_${date.format("yyyyMMdd")}.zip", content: fileEncode))
            contents.add([name:"", content:""])
            def mandrillMessage = new MandrillMessage(
                    text:"this is a text message",
                    subject:"Libretti Polizze Demo dealer: ${dealer.ragioneSociale} del ${date.format("dd-MMM-yyyy")}",
                    from_email:"assistenzaclienti@mach-1.it",
                    to:recpts,
                    attachments : attachs,
                    global_merge_vars:vars
            )
            def mailinviata = mandrillService.sendTemplate(mandrillMessage, "libretti-polizze-demo", contents )
            def inviatoDealer = mailinviata.count { r -> r.success == false } == 0
            def response1 = mailinviata.collect { response1 ->
                [
                        status: response1?.status,
                        message: response1?.message,
                        emailCliente: response1?.email,
                        id: response1?.id,
                        rejectReason: response1?.rejectReason,
                        successCliente: response1?.success
                ].collect { k, v -> "$k: $v" }.join(", ")
            }.join("\n")/*.collect { k, v -> "$k: $v" }.join("\n")*/
            logg =new Log(parametri: "risultato invio mail libretti ${response1} dealer: ${dealer.id}", operazione: "invio mail libretti", pagina: "Salva e chiudi")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            return response1
        }else{
            return "non ci sono libretti da inviare"
        }

    }


    private class EmailCategory {
        static void to(HtmlEmail email, to = "") {
            def addresses = []
            if(to instanceof String) addresses = to.split(",").collect { address -> address.trim() }
            else if(to instanceof List) addresses = to
            else if(to instanceof String[]) addresses = to as List
            addresses.each { address -> email.addTo(address) }
        }
        static void cc(HtmlEmail email, cc = "") {
            def addresses = []
            if(cc instanceof String) addresses = cc.split(",").collect { address -> address.trim() }
            else if(cc instanceof List) addresses = cc
            else if(cc instanceof String[]) addresses = cc as List
            addresses.each { address -> email.addTo(address) }
        }
        static void attach(HtmlEmail email, file, fileName, type = "text/plain", description = "") {
            def data = null
            if(file instanceof byte[] || file instanceof InputStream) data = file
            else if(file instanceof File) data = file.bytes
            if(data) {
                def source = new ByteArrayDS(data, type)
                email.attach(source, fileName, description)
            }
        }
        static void subject(HtmlEmail email, subject = "") { email.subject = subject }
        static void msg(HtmlEmail email, msg = "") { email.msg = msg }
    }

    def sendMail(Closure closure) throws EmailException {
        def emailConfig = grailsApplication.config.mail
        def email = new HtmlEmail()
        use(EmailCategory) {
            email.hostName = emailConfig.host
            email.smtpPort = emailConfig.port
            email.from = emailConfig.from
            closure.resolveStrategy = Closure.DELEGATE_ONLY
            closure.delegate = email
            closure.call()
            return email.send()
        }
    }
    private HttpSession getSession() { WebUtils.retrieveGrailsWebRequest().getSession() }
}
