package gmac

import grails.test.mixin.TestFor
import spock.lang.Specification
import tracciati.TracciatiService

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(TracciatiService)
class TracciatiServiceSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test something"() {
        expect: "fix me"
        true == false
    }
}
