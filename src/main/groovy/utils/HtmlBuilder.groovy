package utils

import groovy.xml.MarkupBuilder

class HtmlBuilder extends MarkupBuilder {

    HtmlBuilder(writer) {
        super(writer)
        doubleQuotes = true
        omitNullAttributes = true
    }

}
