package utils

import groovy.transform.SelfType
import grails.artefact.TagLibrary
import org.grails.taglib.TemplateVariableBinding

@SelfType(TagLibrary)
trait TagLibUtils {

    private static final RANDOM_ID = "random-generated-id"

    abstract TemplateVariableBinding getPageScope()

    Map attrsForPrefix(String prefix, attrs, Map defaults = [:]) {
        def found = attrs.findAll { k, v -> k.startsWith("$prefix-") }
        found.each { k, v -> attrs.remove(k) }
        def prefixAttrs = found.collectEntries { k, v -> [k.replace("$prefix-", ""), v] }
        if(defaults) {
            defaults.each { k, v ->
                if(prefixAttrs[k]) prefixAttrs[k] += " ${v}"
                else prefixAttrs[k] = v
            }
        }
        return prefixAttrs
    }

    List<String> getHtmlClass(Map attrs) { attrs.class?.tokenize(" ")?.unique() ?: [] }

    Map addHtmlClass(Map attrs, String addClass) {
        attrs.class = (getHtmlClass(attrs) + addClass.tokenize(" ").unique()).unique().join(" ")
        return attrs
    }

    Map removeHtmlClass(Map attrs, String removeClass) {
        attrs.class = (getHtmlClass(attrs) - removeClass.tokenize(" ").unique()).unique().join(" ")
        return attrs
    }

    Map swapHtmlClass(Map attrs, Map swapClass) {
        def htmlClass = getHtmlClass(attrs).join(" ")
        swapClass.each { from, to -> htmlClass = htmlClass.replaceAll(from, to) }
        attrs.class = htmlClass
        return attrs
    }

    def randomId(String prefix = "id") {
        if(pageScope[RANDOM_ID] == null) pageScope[RANDOM_ID] = []
        def randomId = "${prefix}-" + String.random(10)
        while(randomId in pageScope[RANDOM_ID]) randomId = "${prefix}-" + String.random(10)
        pageScope[RANDOM_ID] << randomId
        return randomId
    }

}