package utils

trait PagingUtils {

    Set<Integer> getPages(Number max, Number totale) {
        if(totale <= max) return []
        else return (1..Math.ceil(totale / max)).collect()
    }

}