package security

import gmac.utenti.Utente
import org.slf4j.*
import org.springframework.security.core.userdetails.*
import grails.transaction.Transactional

class GmacUserDetailsService implements UserDetailsService {

    static final Logger log = LoggerFactory.getLogger(GmacUserDetailsService)

    @Transactional(readOnly = true, noRollbackFor = [IllegalArgumentException, UsernameNotFoundException])
    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        username=username.trim().replaceAll("\\s+","").replaceAll("-","")
        username=username.replaceAll("^0*","")
        def utente = Utente.findWhere(username: username, deleted: false)
        log.info "Authenticate user ${username}: ${utente}"
        if (!utente) {
            log.warn "User not found: $username"
            throw new UsernameNotFoundException('User not found')
        }
        return new GmacUser(utente)
    }

}
