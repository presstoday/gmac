package security

import groovy.util.logging.Slf4j
import javax.servlet.*
import javax.servlet.http.*

@Slf4j
class InterceptFilter implements Filter {

    void init(FilterConfig filterConfig) throws ServletException {}

    void destroy() {}

    void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        def url = ((HttpServletRequest)request).requestURI
        log.debug "Intercepted url: ${url}"
        if(url == "/GMAC") {
            log.debug "Url match /GMAC, performing redirect to /GMAC/"
            ((HttpServletResponse)response).sendRedirect("/GMAC/")
        } else chain.doFilter(request, response)
    }
}