package security

import gmac.utenti.Utente
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.authority.AuthorityUtils

class GmacUser extends User {

    private final def id
    private final String stringRepresentation

    GmacUser(Utente utente) {
        super(utente.username, utente.password, utente.enabled, true, !utente.passwordScaduta, true, AuthorityUtils.commaSeparatedStringToAuthorityList(utente.ruoli.collect { ruolo -> ruolo.name() }.join(",")))
        this.id = utente.id
        this.stringRepresentation = utente.toString()
    }

    def getId() { return id }

    String toString() { return stringRepresentation }

}
